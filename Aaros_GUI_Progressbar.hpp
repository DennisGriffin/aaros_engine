// File: Aaros_GUI_Progressbar.hpp
//
// Presents a progress bar onscreen.

#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include "Aaros_GUI_Widget.hpp"
#include "Aaros_Utils_Math.hpp"
#include "Aaros_Utils_Misc.hpp"

namespace Aaros
{
    namespace Constants
    {

        const sf::Vector2f  G_PROGRESSBAR_SIZE              = { 192.0f, 24.0f };
        const sf::Color     G_PROGRESSBAR_BACK_BORDER_COLOR = { 33, 33, 33 };
        const sf::Color     G_PROGRESSBAR_BACK_FILL_COLOR   = { 255, 255, 255 };
        const sf::Color     G_PROGRESSBAR_FILL_COLOR        = { 0, 0, 33 };

    }

    namespace GUI
    {

        class Progressbar : public Widget
        {
        private /* Members */:
            sf::RectangleShape  m_backRect;
            sf::RectangleShape  m_fillRect;
            sf::Vector2f        m_size;
            int                 m_value;
            int                 m_maximum;

        private /* Inherited Methods */:
            void                updateWidgetInternals () override;

        public /* Constructor and Destructor */:
            Progressbar (Entity* ap_entity, const std::string& a_name);
            ~Progressbar ();

        public /* Inherited Methods */:
            void                loadFromXML (const pugi::xml_node &a_node) override;
            void                render (sf::RenderWindow &a_window) override;

        public /* Outline Getter-Setters */:
            void                setValue (const int a_value);
            void                setMaximum (const int a_maximum);

        public /* Inline Getter-Setters */:
            inline float        getWidth () const { return m_size.x; }
            inline float        getHeight () const { return m_size.y; }
            inline int          getValue () const { return m_value; }
            inline int          getMaximum () const { return m_maximum; }
            inline float        getPercent () const { return (m_value / m_maximum) * 100.0f; }
            inline bool         isEmpty () const { return (m_value == 0); }
            inline bool         isFull () const { return (m_value == m_maximum); }

            inline void setSize(float a_width, float a_height) { m_size = { a_width, a_height }; updateWidgetInternals(); }
            inline void setBackBorderColor(int a_r, int a_g, int a_b) { m_backRect.setOutlineColor(Utils::createColor(a_r, a_g, a_b)); }
            inline void setBackFillColor(int a_r, int a_g, int a_b) { m_backRect.setFillColor(Utils::createColor(a_r, a_g, a_b)); }
            inline void setFillColor(int a_r, int a_g, int a_b) { m_fillRect.setFillColor(Utils::createColor(a_r, a_g, a_b)); }
        };

    }
}
