// File: Aaros_Component_Widget.cpp

#include "Aaros_Component_Widget.hpp"
#include "Aaros_Entity.hpp"
#include "Aaros_GUI_Textarea.hpp"
#include "Aaros_GUI_Button.hpp"
#include "Aaros_GUI_Scrollbar.hpp"
#include "Aaros_GUI_Progressbar.hpp"
#include "Aaros_GUI_Listbox.hpp"
#include "Aaros_GUI_Checkbox.hpp"

namespace Aaros
{

    GUI::Widget::Iterator WidgetComponent::findWidgetByName(const std::string &a_name)
    {
        return std::find_if (m_widgets.begin(), m_widgets.end(),
                             [&a_name] (const GUI::Widget::Ptr& a_widget)
        {
            return a_widget->getName() == a_name;
        });
    }

    void WidgetComponent::sortWidgets()
    {
        std::sort(m_widgets.begin(), m_widgets.end(),
                  [] (const GUI::Widget::Ptr& a_one, const GUI::Widget::Ptr& a_two)
        {
            return a_one->getFocusIndex() < a_two->getFocusIndex();
        });
    }

    WidgetComponent::WidgetComponent(Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_focusIndex            { 0 },
        m_startFocus            { false }
    {

    }

    WidgetComponent::~WidgetComponent()
    {
        m_widgets.clear();
        m_focusIndex = 0;
        m_startFocus = false;

        mp_entity = nullptr;
        m_entityName.clear();
        m_entityID = 0;
        m_enabled = false;
        m_ancestorDisabled = false;
    }

    void WidgetComponent::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        // Iterate through each child node named "Widget".
        for (const xml_node l_node : a_node.children("Widget"))
        {
            // Get the type and name of widget.
            std::string l_type = l_node.attribute("Type").as_string();
            std::string l_name = l_node.attribute("Name").as_string();

            // Get the widget's focus index.
            unsigned int l_order = l_node.attribute("Index").as_uint();

            // Validate the name.
            if (l_name.empty()) { continue; }

            // Check the type.
            if (l_type == "Textarea")           { addWidget<GUI::Textarea>(l_name, l_order).loadFromXML(l_node); }
            else if (l_type == "Button")        { addWidget<GUI::Button>(l_name, l_order).loadFromXML(l_node); }
            else if (l_type == "Scrollbar")     { addWidget<GUI::Scrollbar>(l_name, l_order).loadFromXML(l_node); }
            else if (l_type == "Listbox")       { addWidget<GUI::Listbox>(l_name, l_order).loadFromXML(l_node); }
            else if (l_type == "Checkbox")      { addWidget<GUI::Checkbox>(l_name, l_order).loadFromXML(l_node); }
            else if (l_type == "Progressbar")   { addWidget<GUI::Progressbar>(l_name, l_order).loadFromXML(l_node); }
        }
    }

    void WidgetComponent::update(const sf::Time &a_deltaTime)
    {
        sortWidgets();

        unsigned int l_widgetCount = m_widgets.size();

        for (unsigned int i = 0; i < l_widgetCount; ++i)
        {
            auto& l_ptr = m_widgets.at(i);

            if (l_ptr->isEnabled() == false || l_ptr->isVisible() == false)
                continue;

            l_ptr->update(a_deltaTime);
        }
    }

    void WidgetComponent::render(sf::RenderWindow &a_window)
    {
        unsigned int l_widgetCount = m_widgets.size();

        for (unsigned int i = 0; i < l_widgetCount; ++i)
        {
            auto& l_ptr = m_widgets.at(i);

            if (l_ptr->isVisible() == false)
                continue;

            l_ptr->render(a_window);
        }
    }

    void WidgetComponent::focusPrevWidget()
    {
        if (m_widgets.empty()) { return; }

        unsigned int l_focusIndex = m_focusIndex;
        bool l_focusableFound = false, l_startedFocus = false;

        m_widgets.at(m_focusIndex)->deactivate();

        while (l_focusableFound == false)
        {
            if (m_startFocus == false)
            {
                m_startFocus = true;
                l_startedFocus = true;
            }
            else
            {
                if (m_focusIndex == 0)
                {
                    m_focusIndex = m_widgets.size();
                }
                m_focusIndex--;
            }

            if (m_widgets.at(m_focusIndex)->isFocusable() == true)
            {
                m_widgets.at(m_focusIndex)->activate();
                l_focusableFound = true;
            }

            if (m_focusIndex == l_focusIndex && l_startedFocus == false)
            {
                break;
            }
        }
    }

    void WidgetComponent::focusNextWidget()
    {
        if (m_widgets.empty()) { return; }

        unsigned int l_focusIndex = m_focusIndex;
        bool l_focusableFound = false, l_startedFocus = false;

        m_widgets.at(m_focusIndex)->deactivate();

        while (l_focusableFound == false)
        {
            if (m_startFocus == false)
            {
                m_startFocus = true;
                l_startedFocus = true;
            }
            else
            {
                m_focusIndex++;
                if (m_focusIndex >= m_widgets.size())
                {
                    m_focusIndex = 0;
                }
            }

            if (m_widgets.at(m_focusIndex)->isFocusable() == true)
            {
                m_widgets.at(m_focusIndex)->activate();
                l_focusableFound = true;
            }

            if (m_focusIndex == l_focusIndex && l_startedFocus == false)
            {
                break;
            }
        }
    }

    void WidgetComponent::resetFocus()
    {
        m_focusIndex = 0;
        m_startFocus = false;
        for (auto l_iter = m_widgets.begin(); l_iter != m_widgets.end(); ++l_iter)
        {
            (*l_iter)->deactivate();
        }
    }

}
