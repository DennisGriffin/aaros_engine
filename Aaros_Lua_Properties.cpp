// File: Aaros_Lua_Properties.cpp

#include "Aaros_Lua_Properties.hpp"
#include "Aaros_PropertyManager.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeProperty (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<Property> l_userdata
            {
                "Property", l_ctors,

                "getString",    &Property::get,
                "getInteger",   &Property::getAs<int>,
                "getNumber",    &Property::getAs<float>,
                "getBoolean",   &Property::getAs<bool>,
                "setString",    &Property::set,
                "setInteger",   &Property::setAs<int>,
                "setNumber",    &Property::setAs<float>,
                "setBoolean",   &Property::setAs<bool>,
                "isSavable",    &Property::isSavable,
                "setSavable",   &Property::setSavable
            };
            a_state.set_userdata(l_userdata);
        }

        void exposePropertyManager (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<PropertyManager> l_userdata
            {
                "PropertyManager", l_ctors,

                "loadFromFile",         &PropertyManager::loadFromFile,
                "saveToFile",           &PropertyManager::saveToFile,
                "savePropertyToFile",   &PropertyManager::savePropertyToFile,
                "appendPropertyToFile", &PropertyManager::appendPropertyToFile,
                "append",               &PropertyManager::append,
                "get",                  &PropertyManager::get,
                "remove",               &PropertyManager::remove
            };
            a_state.set_userdata(l_userdata)
                    .get<sol::table>("PropertyManager")
                    .set_function("getInstance", PropertyManager::getInstance);
        }

    }
}
