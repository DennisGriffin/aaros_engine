// File: Aaros_Component_Script.cpp

#include "Aaros_Component_Script.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    ScriptModule::Iterator ScriptComponent::findModuleByFilename(const std::string &a_file)
    {
        return std::find_if(m_modules.begin(), m_modules.end(),
                            [&a_file] (const ScriptModule& a_module)
        {
            return a_file == a_module.getXmlFilename();
        });
    }

    ScriptComponent::ScriptComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID, sol::state *ap_lua) :
        Component               { ap_entity, a_entityName, a_entityID },
        mp_lua                  { ap_lua }
    {

    }

    ScriptComponent::~ScriptComponent ()
    {
        m_modules.clear();
        mp_lua                  = nullptr;

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void ScriptComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        for (const xml_node& l_moduleNode : a_node.children("Module"))
        {
            std::string l_xmlFile = l_moduleNode.attribute("Source").as_string();
            bool l_enabled = l_moduleNode.attribute("Enabled").as_bool(true);

            if (l_xmlFile.empty() == false)
            {
                addModule(l_xmlFile, l_enabled).loadFromXML(l_moduleNode);
            }
        }
    }

    ScriptModule &ScriptComponent::addModule(const std::string &a_file, const bool a_enabled)
    {
        auto l_findByFile = findModuleByFilename(a_file);

        if (l_findByFile != m_modules.end()) { return *l_findByFile; }

        m_modules.emplace_back(a_file, a_enabled);
        return m_modules.back();
    }

    ScriptModule &ScriptComponent::getModule(const std::string &a_file)
    {
        auto l_find = findModuleByFilename(a_file);

        if (l_find == m_modules.end())
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorModuleFile", false).set(a_file);
            PM.append("errorEntityName", false).set(m_entityName);
            PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

            throw std::runtime_error {
                "[ScriptComponent::getModule] Script module file '" + a_file + "' not loaded in entity '" + m_entityName + "'!"
            };
        }

        return *l_find;
    }

}
