// File: Aaros_Mouse.cpp

#include "Aaros_Mouse.hpp"

namespace Aaros
{

    void Mouse::updateWorldPosition(sf::RenderWindow &a_window, const sf::View &a_view)
    {
        m_coords = a_window.mapPixelToCoords(m_position, a_view);
    }

    void Mouse::pollEvent(sf::Event &a_event)
    {
        if (a_event.type == sf::Event::MouseButtonPressed)
        {
            int l_code = a_event.mouseButton.button;
            m_pressed[l_code] = true;
            m_down[l_code] = true;

            m_position = { a_event.mouseButton.x, a_event.mouseButton.y };
        }
        else if (a_event.type == sf::Event::MouseButtonReleased)
        {
            int l_code = a_event.mouseButton.button;
            m_pressed[l_code] = false;
            m_down[l_code] = false;

            m_position = { a_event.mouseButton.x, a_event.mouseButton.y };
        }
        else if (a_event.type == sf::Event::MouseMoved)
        {
            m_position = { a_event.mouseMove.x, a_event.mouseMove.y };
        }
        else if (a_event.type == sf::Event::MouseWheelScrolled)
        {
            m_delta = a_event.mouseWheelScroll.delta;
            m_position = {
                a_event.mouseWheelScroll.x,
                a_event.mouseWheelScroll.y
            };
        }
    }

    bool Mouse::isButtonPressed(const int a_index)
    {
        auto l_find = m_pressed.find(a_index);
        if (l_find != m_pressed.end())
        {
            bool l_return = l_find->second;
            l_find->second = false;
            return l_return;
        }

        return false;
    }

    bool Mouse::isButtonDown(const int a_index)
    {
        auto l_find = m_down.find(a_index);
        if (l_find != m_down.end())
        {
            return l_find->second;
        }

        return false;
    }

    sf::Vector2f Mouse::getPosition() const
    {
        return {
            static_cast<float>(m_position.x),
            static_cast<float>(m_position.y)
        };
    }

    sf::Vector2f Mouse::getWorldPosition() const
    {
        return m_coords;
    }

    int Mouse::getWheelDelta()
    {
        int l_delta = m_delta;
        m_delta = 0;
        return l_delta;
    }

    void Mouse::resetMouseButtonPresses()
    {
        for (auto& l_presses : m_pressed)
        {
            l_presses.second = false;
        }
    }

}
