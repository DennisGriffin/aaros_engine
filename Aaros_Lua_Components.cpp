// File: Aaros_Lua_Components.cpp

#include "Aaros_Lua_Components.hpp"
#include "Aaros_Component_Transform.hpp"
#include "Aaros_Component_Rigidbody.hpp"
#include "Aaros_Component_Collider.hpp"
#include "Aaros_Component_Script.hpp"
#include "Aaros_Component_Timer.hpp"
#include "Aaros_Component_Widget.hpp"
#include "Aaros_Component_Text.hpp"
#include "Aaros_Component_Sprite.hpp"
#include "Aaros_Component_Tilemap.hpp"
#include "Aaros_Component_Emitter.hpp"
#include "Aaros_Component_Animation.hpp"
#include "Aaros_Component_Sound.hpp"
#include "Aaros_Component_Widget.hpp"
#include "Aaros_GUI_Textarea.hpp"
#include "Aaros_GUI_Button.hpp"
#include "Aaros_GUI_Scrollbar.hpp"
#include "Aaros_GUI_Progressbar.hpp"
#include "Aaros_GUI_Listbox.hpp"
#include "Aaros_GUI_Checkbox.hpp"

#define EXPOSE_BASE_COMPONENT_METHODS(a_component) \
    "isEnabled", &a_component::isEnabled, \
    "setEnabled", &a_component::setEnabled

namespace Aaros
{
    namespace Lua
    {

        static void exposeTransformComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<TransformComponent> l_userdata
            {
                "TransformComponent", l_ctors,

                "translate",            &TransformComponent::translate,
                "getPositionX",         &TransformComponent::getPositionX,
                "getPositionY",         &TransformComponent::getPositionY,
                "getCenterX",           &TransformComponent::getCenterX,
                "getCenterY",           &TransformComponent::getCenterY,
                "getPixelPositionX",    &TransformComponent::getPixelPositionX,
                "getPixelPositionY",    &TransformComponent::getPixelPositionY,
                "getPixelCenterX",      &TransformComponent::getPixelCenterX,
                "getPixelCenterY",      &TransformComponent::getPixelCenterY,
                "setPosition",          &TransformComponent::setPosition,
                "setCenter",            &TransformComponent::setCenter
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeRigidbodyComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<RigidbodyComponent> l_userdata
            {
                "RigidbodyComponent", l_ctors,

                "applyForce",           &RigidbodyComponent::applyForce,
                "getVelocityX",         &RigidbodyComponent::getVelocityX,
                "getVelocityY",         &RigidbodyComponent::getVelocityY,
                "getPixelVelocityX",    &RigidbodyComponent::getPixelVelocityX,
                "getPixelVelocityY",    &RigidbodyComponent::getPixelVelocityY,
                "getForceX",            &RigidbodyComponent::getForceX,
                "getForceY",            &RigidbodyComponent::getForceY,
                "getMass",              &RigidbodyComponent::getMass,
                "getInverseMass",       &RigidbodyComponent::getInverseMass,
                "getDrag",              &RigidbodyComponent::getDrag,
                "getGravityScale",      &RigidbodyComponent::getGravityScale,
                "isKinematic",          &RigidbodyComponent::isKinematic,

                "setVelocity",          &RigidbodyComponent::setVelocity,
                "setMass",              &RigidbodyComponent::setMass,
                "makeStatic",           &RigidbodyComponent::makeStatic,
                "setDrag",              &RigidbodyComponent::setDrag,
                "setGravityScale",      &RigidbodyComponent::setGravityScale,
                "setKinematic",         &RigidbodyComponent::setKinematic,

                EXPOSE_BASE_COMPONENT_METHODS(RigidbodyComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeColliderComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<ColliderComponent> l_userdata
            {
                "ColliderComponent", l_ctors,

                "getMinimumX",          &ColliderComponent::getMinimumX,
                "getMinimumY",          &ColliderComponent::getMinimumY,
                "getMaximumX",          &ColliderComponent::getMaximumX,
                "getMaximumY",          &ColliderComponent::getMaximumY,
                "getCenterX",           &ColliderComponent::getCenterX,
                "getCenterY",           &ColliderComponent::getCenterY,
                "getWidth",             &ColliderComponent::getWidth,
                "getHeight",            &ColliderComponent::getHeight,
                "getHalfwidth",         &ColliderComponent::getHalfwidth,
                "getHalfheight",        &ColliderComponent::getHalfheight,
                "getRestitution",       &ColliderComponent::getRestitution,

                "setPosition",          &ColliderComponent::setPosition,
                "setSize",              &ColliderComponent::setSize,
                "setRestitution",       &ColliderComponent::setRestitution,

                EXPOSE_BASE_COMPONENT_METHODS(ColliderComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeScriptComponent (sol::state& a_state)
        {
            sol::constructors<> l_comCtors, l_modCtors;

            sol::userdata<ScriptModule> l_modUserdata
            {
                "ScriptModule",     l_modCtors,

                "isFunctionEnabled",    &ScriptModule::isFunctionEnabled,
                "getString",            &ScriptModule::getString,
                "getInteger",           &ScriptModule::getInteger,
                "getNumber",            &ScriptModule::getNumber,
                "getBoolean",           &ScriptModule::getBoolean,
                "setFunction",          &ScriptModule::setFunction,
                "setString",            &ScriptModule::setString,
                "setInteger",           &ScriptModule::setInteger,
                "setNumber",            &ScriptModule::setNumber,
                "setBoolean",           &ScriptModule::setBoolean,
                "importFromGlobal",     &ScriptModule::importFromGlobal,
                "exportToGlobal",       &ScriptModule::exportToGlobal,
                "getXmlFilename",       &ScriptModule::getXmlFilename,
                "getScriptFilename",    &ScriptModule::getScriptFilename,
                "isEnabled",            &ScriptModule::isEnabled,
                "setEnabled",           &ScriptModule::setEnabled
            };

            sol::userdata<ScriptComponent> l_comUserdata
            {
                "ScriptComponent",  l_comCtors,

                "addModule",        &ScriptComponent::addModule,
                "getModule",        &ScriptComponent::getModule,

                EXPOSE_BASE_COMPONENT_METHODS(ScriptComponent)
            };

            a_state.set_userdata(l_modUserdata);
            a_state.set_userdata(l_comUserdata);
        }

        static void exposeTimerComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<TimerComponent> l_userdata
            {
                "TimerComponent", l_ctors,

                "setTimeEventInterval",     &TimerComponent::setTimeEventInterval,
                "toggleTimeEvent",          &TimerComponent::toggleTimeEvent,
                "resetTimer",               &TimerComponent::resetTimer,
                "resetAllTimers",           &TimerComponent::resetAllTimers,

                EXPOSE_BASE_COMPONENT_METHODS(TimerComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeTextComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<TextComponent> l_userdata
            {
                "TextComponent", l_ctors,

                "setFont",          &TextComponent::setFont,
                "getText",          &TextComponent::getText,
                "getFontSize",      &TextComponent::getFontSize,
                "isCentered",       &TextComponent::isCentered,
                "setText",          &TextComponent::setText,
                "setFontSize",      &TextComponent::setFontSize,
                "setColor",         &TextComponent::setColor,
                "setCentered",      &TextComponent::setCentered,

                EXPOSE_BASE_COMPONENT_METHODS(TextComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeSpriteComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<SpriteComponent> l_userdata
            {
                "SpriteComponent", l_ctors,

                "getShader",        &SpriteComponent::getShader,

                EXPOSE_BASE_COMPONENT_METHODS(SpriteComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeTilemapComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<TilemapComponent> l_userdata
            {
                "TilemapComponent", l_ctors,

                "setTile",          &TilemapComponent::setTile,
                "removeTile",       &TilemapComponent::removeTile,
                "getShader",        &TilemapComponent::getShader,

                EXPOSE_BASE_COMPONENT_METHODS(TilemapComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeWidgetComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<WidgetComponent> l_userdata
            {
                "WidgetComponent", l_ctors,

                "removeWidget",         &WidgetComponent::removeWidget,
                "focusPrevWidget",      &WidgetComponent::focusPrevWidget,
                "focusNextWidget",      &WidgetComponent::focusNextWidget,
                "resetFocus",           &WidgetComponent::resetFocus,

                "getTextarea",          &WidgetComponent::getWidget<GUI::Textarea>,
                "getButton",            &WidgetComponent::getWidget<GUI::Button>,
                "getScrollbar",         &WidgetComponent::getWidget<GUI::Scrollbar>,
                "getProgressbar",       &WidgetComponent::getWidget<GUI::Progressbar>,
                "getListbox",           &WidgetComponent::getWidget<GUI::Listbox>,
                "getCheckbox",          &WidgetComponent::getWidget<GUI::Checkbox>,

                "addTextarea",          &WidgetComponent::addWidget<GUI::Textarea>,
                "addButton",            &WidgetComponent::addWidget<GUI::Button>,
                "addScrollbar",         &WidgetComponent::addWidget<GUI::Scrollbar>,
                "addProgressbar",       &WidgetComponent::addWidget<GUI::Progressbar>,
                "addListbox",           &WidgetComponent::addWidget<GUI::Listbox>,
                "addCheckbox",          &WidgetComponent::addWidget<GUI::Checkbox>,

                EXPOSE_BASE_COMPONENT_METHODS(WidgetComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeEmitterComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<EmitterComponent> l_userdata
            {
                "EmitterComponent", l_ctors,

                "setLowerVelocity",     &EmitterComponent::setLowerboundVelocity,
                "setUpperVelocity",     &EmitterComponent::setUpperboundVelocity,
                "getParticleCount",     &EmitterComponent::getParticleCount,
                "getParticleLimit",     &EmitterComponent::getParticleLimit,
                "getLowerVelocityX",    &EmitterComponent::getLowerVelocityX,
                "getLowerVelocityY",    &EmitterComponent::getLowerVelocityY,
                "getUpperVelocityX",    &EmitterComponent::getUpperVelocityX,
                "getUpperVelocityY",    &EmitterComponent::getUpperVelocityY,
                "getParticleLifetime",  &EmitterComponent::getParticleLifetime,
                "getParticleSpawntime", &EmitterComponent::getParticleSpawntime,

                "setParticleLimit",     &EmitterComponent::setParticleLimit,
                "setParticleLifetime",  &EmitterComponent::setParticleLifetime,
                "setParticleSpawntime", &EmitterComponent::setParticleSpawntime,
                "setParticleColor",     &EmitterComponent::setParticleColor,

                "getShader",            &EmitterComponent::getShader,

                EXPOSE_BASE_COMPONENT_METHODS(EmitterComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeAnimationComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<AnimationComponent> l_userdata
            {
                "AnimationComponent", l_ctors,

                "setCurrentAnimation",  &AnimationComponent::setCurrentAnimation,
                "pushFrame",            &AnimationComponent::pushFrame,
                "popFrame",             &AnimationComponent::popFrame,
                "getCurrentAnimation",  &AnimationComponent::getCurrentAnimation,
                "getCurrentFrame",      &AnimationComponent::getCurrentFrame,
                "getFrameInterval",     &AnimationComponent::getFrameInterval,
                "getFrameWidth",        &AnimationComponent::getFrameWidth,
                "getFrameHeight",       &AnimationComponent::getFrameHeight,
                "setFrameInterval",     &AnimationComponent::setFrameInterval,
                "setFrameSize",         &AnimationComponent::setFrameSize,

                EXPOSE_BASE_COMPONENT_METHODS(AnimationComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeSoundComponent (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<SoundComponent> l_userdata
            {
                "SoundComponent", l_ctors,

                "loadSound",            &SoundComponent::loadSound,
                "playSound",            &SoundComponent::playSound,
                "pauseSound",           &SoundComponent::pauseSound,
                "stopSound",            &SoundComponent::stopSound,
                "setMinimumPitch",      &SoundComponent::setMinimumPitch,
                "setMaximumPitch",      &SoundComponent::setMaximumPitch,
                "getVolume",            &SoundComponent::getVolume,
                "getMinimumDistance",   &SoundComponent::getMinimumDistance,
                "getAttenuation",       &SoundComponent::getAttenuation,
                "getMinimumPitch",      &SoundComponent::getMinimumPitch,
                "getMaximumPitch",      &SoundComponent::getMaximumPitch,
                "isLooping",            &SoundComponent::isLooping,
                "setVolume",            &SoundComponent::setVolume,
                "setMinimumDistance",   &SoundComponent::setMinimumDistance,
                "setAttenuation",       &SoundComponent::setAttenuation,
                "setLooping",           &SoundComponent::setLooping,

                EXPOSE_BASE_COMPONENT_METHODS(SoundComponent)
            };
            a_state.set_userdata(l_userdata);
        }

        void exposeComponents (sol::state& a_state)
        {
            exposeTransformComponent(a_state);
            exposeRigidbodyComponent(a_state);
            exposeColliderComponent(a_state);
            exposeScriptComponent(a_state);
            exposeTimerComponent(a_state);
            exposeTextComponent(a_state);
            exposeSpriteComponent(a_state);
            exposeTilemapComponent(a_state);
            exposeEmitterComponent(a_state);
            exposeWidgetComponent(a_state);
            exposeAnimationComponent(a_state);
            exposeSoundComponent(a_state);
        }

    }
}
