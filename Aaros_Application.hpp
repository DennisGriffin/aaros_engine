// File: Aaros_Application.hpp
//
// The heart of the Aaros Engine, this class manages the game window.

#pragma once

#include <SFML/Config.hpp>
#if SFML_VERSION_MAJOR < 2 || SFML_VERSION_MINOR < 3
#   error The Aaros Engine requires SFML 2.3+. Download it at 'www.sfml-dev.org/download.php'.
#endif

#include <lua.hpp>
#if LUA_VERSION_NUM < 502
#   error The Aaros Engine requires Lua 5.2+. Download it at 'www.lua.org/download.html'.
#endif

#include <string>
#include <stdexcept>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>
#include "Aaros_AssetManager.hpp"
#include "Aaros_Keyboard.hpp"
#include "Aaros_Mouse.hpp"
#include "Aaros_Scene.hpp"
#include "Aaros_Utils_Random.hpp"

namespace Aaros
{

    namespace Constants
    {
        // Window Attributes.
        const unsigned int  G_WINDOW_WIDTH          = 800;
        const unsigned int  G_WINDOW_HEIGHT         = 600;
        const bool          G_WINDOW_FULLSCREEN     = false;
        const std::string   G_WINDOW_TITLE          = "Project Aaros Engine";

        // Game Time Attributes.
        const sf::Time      G_FIXED_TIMESTEP        = sf::seconds(1.0f / 60.0f);
        const sf::Time      G_DELTA_CAP             = sf::seconds(0.2f);
        const sf::Time      G_FRAMETIME             = sf::seconds(1.0f);

        // Initialization Attributes.
        const std::string   G_INIT_FILE             = "Assets/System/Init.xml";
    }

    /**
     * This class is in charge of maintaining the game window and running
     * the main game loop. It is essentially the heart of the Aaros Engine.
     */
    class Application
    {
    private /* Members */:
        Scene::Ptr              m_scene;            /** The game scene. */
        sf::RenderWindow        m_window;           /** The game window. */
        sf::Vector2u            m_windowSize;       /** The size of the game window. */
        bool                    m_windowFullscreen; /** The window's fullscreen flag. */
        std::string             m_windowTitle;      /** The game window's titlebar text. */
        sf::Text                m_reportFPS;        /** The visual FPS indicator. */
        sf::Clock               m_appClock;         /** A clock to help the application with timekeeping. */
        sf::Time                m_deltaTime;        /** Keeps track of when we need to do the next fixed update. */
        sf::Time                m_frameTime;        /** Keeps track of when we need to do the next FPS update. */
        int                     m_frameCount;       /** The amount of frames drawn since the previous FPS update. */

    private /* Methods */:
        /**
         * This initialization method loads an XML file containing properties that pertain
         * to the engine's initialization.
         */
        void                    loadInitFile ();

        /**
         * This initialization method creates the game window.
         */
        void                    createWindow ();

        /**
         * This initialization method loads the starting scene.
         */
        void                    loadStartScene ();

        /**
         * This method polls the game window, as well as input devices such as the
         * keyboard and mouse, for events raised by the user.
         *
         * This is called first in the game loop, and again whenever a fixed update is
         * requested.
         */
        void                    processEvents ();

        /**
         * This method performs physics calculations and other updates that rely upon
         * a fixed timestep. Every 1/60 of a second, the engine will request a fixed update.
         *
         * When requested, this is called after event-handling and before the standard update.
         *
         * @param   a_timestep          A constant timestep to be used as a reference.
         */
        void                    fixedUpdate (const sf::Time& a_timestep);

        /**
         * This method performs general updates of the game engine and the underlying game.
         * This is called after event-handling and the fixed update, if requested.
         *
         * @param   a_deltaTime         The amount of time elapsed since the previous update.
         */
        void                    update (const sf::Time& a_deltaTime);

        /**
         * This method updates the current framerate. Every second a new FPS count will be reported
         * to the user.
         *
         * This is called after the standard update.
         *
         * @param   a_elapsedTime       The amount of time elapsed since the previous update.
         */
        void                    updateFPS (const sf::Time& a_elapsedTime);

        /**
         * Called last in the game loop, this method draws stuff to the game window.
         */
        void                    render ();

    public /* Constructor and Destructor */:
        /**
         * The constructor initializes the game window.
         */
        Application ();
        ~Application ();

    public /* Methods */:
        /**
         * This method starts the game loop.
         */
        void                    run ();

        /**
         * This method quits the game loop.
         */
        void                    quit ();

    public /* Outline Getter-Setters */:
        void                    setWindowSize (const unsigned int a_width,
                                               const unsigned int a_height);
        void                    setWindowFullscreen (const bool a_fullscreen);
    };

}
