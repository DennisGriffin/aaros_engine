// File: Aaros_Utils_ScriptModule.hpp
//
// Contains a helper class for storing Lua script functions.

#pragma once

#include <string>
#include <map>
#include <stdexcept>
#include <sol.hpp>
#include "Aaros_PropertyManager.hpp"

namespace Aaros
{

    class ScriptModule : Utils::XmlLoadable
    {
    public /* Typedefs */:
        using Container         = std::vector<ScriptModule>;
        using Iterator          = Container::iterator;
        using FunctionMap       = std::map<std::string, bool>;
        using PropertyMap       = std::map<std::string, std::string>;

    private /* Members */:
        FunctionMap             m_functions;            /** The Lua script functions inside this module. */
        PropertyMap             m_properties;           /** The string properties maintained inside this module. */
        std::string             m_scriptFile;           /** The Lua script file that contains the module's functions. */
        bool                    m_enabled;              /** Should the engine call the functions inside this module? */

    public /* Constructor and Destructor */:
        ScriptModule (const std::string& a_xmlFile,
                      const bool a_enabled = true);
        ~ScriptModule ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        bool                    isFunctionEnabled (const std::string& a_name);
        std::string             getString (const std::string& a_name, const std::string& a_default = "");
        int                     getInteger (const std::string& a_name, const int a_default = 0);
        float                   getNumber (const std::string& a_name, const float a_default = 0.0f);
        bool                    getBoolean (const std::string& a_name, const bool a_default = false);
        void                    setFunction (const std::string& a_name, const bool a_enabled);
        void                    setString (const std::string& a_name, const std::string& a_value);
        void                    setInteger (const std::string& a_name, const int a_value);
        void                    setNumber (const std::string& a_name, const float a_value);
        void                    setBoolean (const std::string& a_name, const bool a_value);
        void                    importFromGlobal (const std::string& a_name);
        void                    exportToGlobal (const std::string& a_name);

    public /* Getter-Setters */:
        inline std::string      getXmlFilename () const { return m_filename; }
        inline std::string      getScriptFilename () const { return m_scriptFile; }
        inline bool             isEnabled () const { return m_enabled; }

        inline void setEnabled (const bool a_enabled) { m_enabled = a_enabled; }
    };

}
