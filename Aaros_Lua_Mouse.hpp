// File: Aaros_Lua_Mouse.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeMouse (sol::state& a_state);

    }
}
