// File: Aaros_Utils_Singleton.hpp
//
// Provides a helper class that gurantees that inheriting
// classes can only have one instance loaded at a time.

#pragma once

#include <SFML/System/NonCopyable.hpp>

namespace Aaros
{
    namespace Utils
    {

        /**
         * This helper class provides inheriting classes with facility that ensures
         * that only one instance of that class is loaded at any given time. A static method
         * lies inside that users can call to access this "singleton" instance of the inheriting class.
         *
         * In order for this class to work properly, in addition to inheriting from this class, the
         * class also needs to include this class as a friend, like so:
         *
         * @code
         *
         * class TestClass : public Utils::Singleton<TestClass>
         * {
         * private:
         *     friend class Utils::Singleton<TestClass>;
         * };
         *
         * @endcode
         */
        template <typename T>
        class Singleton : private sf::NonCopyable
        {
        protected /* Constructor */:
            Singleton<T>() {}

        public /* Static Methods */:
            /**
             * This method returns the singleton instance of the inheriting
             * class, instantiating it if necessary.
             */
            inline static T& getInstance ()
            {
                static T s_instance;
                return s_instance;
            }
        };

    }
}
