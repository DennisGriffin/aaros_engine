// File: Aaros_System_Physics.cpp

#include "Aaros_System_Physics.hpp"

namespace Aaros
{

    bool PhysicsSystem::isEntityValid (Entity *ap_entity)
    {
        if (ap_entity->isEnabled() == false) { return false; }

        PhysicsObject l_object { ap_entity };
        bool l_hasRigidbody = ap_entity->hasComponent<RigidbodyComponent>();
        bool l_hasCollider = ap_entity->hasComponent<ColliderComponent>();

        if (l_hasRigidbody)
        {
            RigidbodyComponent& l_rigidbody = ap_entity->getComponent<RigidbodyComponent>();
            if (l_rigidbody.isEnabled())
                l_object.mp_rigidbody = &l_rigidbody;
        }

        if (l_hasCollider)
        {
            ColliderComponent& l_collider = ap_entity->getComponent<ColliderComponent>();
            if (l_collider.isEnabled())
            {
                l_collider.resetCollisionStatus();
                l_collider.updateOrigin();
                l_collider.checkClicked();
                l_object.mp_collider = &l_collider;
            }
        }

        if (l_object.isValid() == false) { return false; }

        m_objects.push_back(l_object);
        return true;
    }

    void PhysicsSystem::integrateForce(PhysicsObject &a_object, const float a_time)
    {
        RigidbodyComponent* lp_rigidbody = a_object.mp_rigidbody;
        if (lp_rigidbody != nullptr)
        {
            // Do not apply any force if the body has infinite mass, or if the
            // body is flagged as kinematic.
            if (lp_rigidbody->getInverseMass() == 0.0f || lp_rigidbody->isKinematic())
                return;

            lp_rigidbody->applyVelocityVector(
                ((lp_rigidbody->getForce() * lp_rigidbody->getInverseMass())
                        + (m_gravity * lp_rigidbody->getGravityScale()))
                        * (a_time / 2.0f)
            );
        }
    }

    void PhysicsSystem::integrateVelocity(PhysicsObject &a_object, const float a_time)
    {
        TransformComponent* lp_transform = a_object.mp_transform;
        RigidbodyComponent* lp_rigidbody = a_object.mp_rigidbody;
        if (lp_rigidbody != nullptr)
        {
            if (lp_rigidbody->getInverseMass() == 0.0f)
                return;

            lp_transform->translateByVector(lp_rigidbody->getVelocity() * a_time);

            // Do not apply drag force if the body is kinematic.
            if (lp_rigidbody->isKinematic() == false)
                lp_rigidbody->applyDrag(a_time);

            integrateForce(a_object, a_time);
        }
    }

    void PhysicsSystem::clearForce(PhysicsObject &a_object)
    {
        RigidbodyComponent* lp_rigidbody = a_object.mp_rigidbody;
        if (lp_rigidbody != nullptr)
        {
            lp_rigidbody->resetForce();
        }
    }

    bool PhysicsSystem::checkCollision(PhysicsObject &a_one, PhysicsObject &a_two, const bool a_store)
    {
        // Get the collider components of the two physics objects.
        ColliderComponent* lp_colliderOne = a_one.mp_collider;
        ColliderComponent* lp_colliderTwo = a_two.mp_collider;

        // Return false if one or both of the colliders are null.
        if (lp_colliderOne == nullptr || lp_colliderTwo == nullptr)
            return false;

        // Get the minimum width and height of the two colliders..
        sf::Vector2f l_minSize = { 0.0f, 0.0f };
        l_minSize.x = std::min(lp_colliderOne->getWidth(), lp_colliderTwo->getWidth());
        l_minSize.y = std::min(lp_colliderOne->getHeight(), lp_colliderTwo->getHeight());

        // Get the distance between the colliders' center points.
        sf::Vector2f l_distance = lp_colliderTwo->getCenter() - lp_colliderOne->getCenter();

        // Use that distance and the colliders' half-extents to calculate an overlap.
        sf::Vector2f l_overlap =
                (lp_colliderOne->getHalfsize() + lp_colliderTwo->getHalfsize()) - Math::getVectorAbs(l_distance);

        // Apply the Separating Axis Theorem upon the overlap vector.
        // If both axes of the overlap is greater than zero, then there is a collision.
        if (l_overlap.x > 0.0f && l_overlap.y > 0.0f)
        {
            // Create a physics contact and begin storing information on the collision.
            PhysicsContact l_contact { &a_one, &a_two };
            bool l_significant = false;
            bool l_notTrigger = false;

            // Find out how much we are overlapping along the axes of least and greatest penetration.
            // Use the minimum penetration depth, along with the distnace between the colliders in order
            // to generate the contact normal.
            //
            // The contact normal determines which faces of colliding AABBs are colliding.
            if (l_overlap.x < l_overlap.y)
            {
                if (l_distance.x < 0.0f)    { l_contact.m_normal.x = -1.0f; }
                else                        { l_contact.m_normal.x = 1.0f; }

                l_contact.m_minPenetration = l_overlap.x;
                l_contact.m_maxPenetration = l_overlap.y;
                l_significant = (l_contact.m_maxPenetration > l_minSize.y / 4.0f);
            }
            else
            {
                if (l_distance.y < 0.0f)    { l_contact.m_normal.y = -1.0f; }
                else                        { l_contact.m_normal.y = 1.0f; }

                l_contact.m_minPenetration = l_overlap.y;
                l_contact.m_maxPenetration = l_overlap.x;
                l_significant = (l_contact.m_maxPenetration > l_minSize.x / 4.0f);
            }

            // If desired, and the collision is significant enough, store the contact for resolution.
            if (a_store == true && l_significant == true)
            {
                lp_colliderOne->collisionCallback(lp_colliderTwo, l_contact.m_normal);
                lp_colliderTwo->collisionCallback(lp_colliderOne, l_contact.m_normal);
                l_notTrigger = (lp_colliderOne->isTrigger() == false && lp_colliderTwo->isTrigger() == false);

                if (l_notTrigger == true) { m_contacts.push_back(l_contact); }

                // Collision. Return true.
                return true;
            }
        }

        lp_colliderOne->noCollisionCallback(lp_colliderTwo);
        lp_colliderTwo->noCollisionCallback(lp_colliderOne);

        // No collision. Return false.
        return false;
    }

    void PhysicsSystem::resolveCollision(PhysicsContact &a_contact)
    {
        // Get the rigidbody components of the colliding physics objects.
        RigidbodyComponent* lp_rigidbodyOne = a_contact.mp_one->mp_rigidbody;
        RigidbodyComponent* lp_rigidbodyTwo = a_contact.mp_two->mp_rigidbody;

        // Set up variables for the rigidbodies' velocity, inverse mass, and kinematic flags.
        // Also, set up a flag for indicating the validity of the rigidbody pointers.
        sf::Vector2f l_velocityOne = { 0.0f, 0.0f }, l_velocityTwo = { 0.0f, 0.0f };
        float l_inverseMassOne = 0.0f, l_inverseMassTwo = 0.0f;
        bool l_kinematicOne = false, l_kinematicTwo = false;
        bool l_validOne = false, l_validTwo = false;

        // Get the rigidbodies' velocity, inverse mass, and kinematic flags.
        //
        // A physics object with a nullptr rigidbody is assumed to be a static physics object,
        // with no sense of velocity and infinite mass (indicated by an inverse mass of zero).
        //
        // If a physics object's rigidbody component has its kinematic flag set to true, then
        // it is considered a kinematic physics object. Kinematic physics objects do not respond to
        // forces, but can have their positions manipulated by velocity. Kinematic physics objects also
        // have infinite mass.
        //
        // Kinematic physics objects do not respond to collisions themselves, but can collide and
        // apply forces upon other dynamic physics objects (physics objects with valid rigidbodies
        // with their kinematic flag set to false). Kinematic physics objects will not collide at all
        // with other kinematic physics objects or static physics objects.
        if (lp_rigidbodyOne != nullptr)
        {
            l_validOne = true;
            l_kinematicOne = lp_rigidbodyOne->isKinematic();
            l_velocityOne = lp_rigidbodyOne->getVelocity();

            if (!l_kinematicOne) { l_inverseMassOne = lp_rigidbodyOne->getInverseMass(); }
        }

        if (lp_rigidbodyTwo != nullptr)
        {
            l_validTwo = true;
            l_kinematicTwo = lp_rigidbodyTwo->isKinematic();
            l_velocityTwo = lp_rigidbodyTwo->getVelocity();

            if (!l_kinematicTwo) { l_inverseMassTwo = lp_rigidbodyTwo->getInverseMass(); }
        }

        // If the inverse mass of both rigidbodies add up to zero, then one of two things
        // are happening here:
        //
        // - One or both objects are dynamic objects with infinite mass. Resolve by setting their velocities to zero.
        // - Both objects are either static or kinematic. Do not resolve.
        if (l_inverseMassOne + l_inverseMassTwo == 0.0f)
        {
            if (l_validOne && !l_kinematicOne) { lp_rigidbodyOne->setVelocity(0.0f, 0.0f); }
            if (l_validTwo && !l_kinematicTwo) { lp_rigidbodyTwo->setVelocity(0.0f, 0.0f); }

            // Early out in this case.
            return;
        }

        // Calculate the relative velocity between the rigidbodies.
        // Also calculate the velocity along the contact normal.
        sf::Vector2f l_relativeVelocity = l_velocityTwo - l_velocityOne;
        float l_velocityAlongNormal = Math::getVectorDot(l_relativeVelocity, a_contact.m_normal);

        // If the velocity along the normal is greater than zero, then there is no need to resolve this
        // collision, as the rigidbodies' velocities will do that for us. Early out in this case.
        if (l_velocityAlongNormal > 0.0f) { return; }

        // Use the numbers we just got in order to calculate the impulse scalar.
        float l_impulseScalar = -(1.0f + a_contact.m_minRestitution) * l_velocityAlongNormal;
        l_impulseScalar /= (l_inverseMassOne + l_inverseMassTwo);

        // Calculate the impulse vector, which will be applied upon the rigidbodies' velocity in order
        // to resolve the collision.
        sf::Vector2f l_impulse = l_impulseScalar * a_contact.m_normal;

        // Only apply the impulse on non-kinematic rigidbodies.
        if (l_validOne && !l_kinematicOne) { lp_rigidbodyOne->applyImpulseVector(-l_impulse); }
        if (l_validTwo && !l_kinematicTwo) { lp_rigidbodyTwo->applyImpulseVector(l_impulse); }
    }

    void PhysicsSystem::correctPosition(PhysicsContact &a_contact)
    {
        // Get the collider, rigidbody and transform components of the colliding physics objects.
        ColliderComponent* lp_colliderOne = a_contact.mp_one->mp_collider;
        ColliderComponent* lp_colliderTwo = a_contact.mp_two->mp_collider;
        RigidbodyComponent* lp_rigidbodyOne = a_contact.mp_one->mp_rigidbody;
        RigidbodyComponent* lp_rigidbodyTwo = a_contact.mp_two->mp_rigidbody;
        TransformComponent* lp_transformOne = a_contact.mp_one->mp_transform;
        TransformComponent* lp_transformTwo = a_contact.mp_two->mp_transform;

        // Get the validity of the rigidbodies, as well as their inverse mass and kinematic flags.
        float l_inverseMassOne = 0.0f, l_inverseMassTwo = 0.0f;
        bool l_kinematicOne = false, l_kinematicTwo = false;

        if (lp_rigidbodyOne != nullptr)
        {
            l_kinematicOne = lp_rigidbodyOne->isKinematic();

            if (!l_kinematicOne) { l_inverseMassOne = lp_rigidbodyOne->getInverseMass(); }
        }

        if (lp_rigidbodyTwo != nullptr)
        {
            l_kinematicTwo = lp_rigidbodyTwo->isKinematic();

            if (!l_kinematicTwo) { l_inverseMassTwo = lp_rigidbodyTwo->getInverseMass(); }
        }

        // If both objects have infinite mass, then don't bother with position correction.
        if (l_inverseMassOne + l_inverseMassTwo == 0.0f) { return; }

        // The correction percent indicates by how much we would like our transforms' positions
        // to be corrected. Example: Set to 0.5f for a correction of 50% of the penetration depth.
        //
        // The correction slop is an arbitrary penetration threshold below which position correction
        // will not be performed. Example: Set to 0.05f, and position correction will not be done if
        // the penetration depth is below 0.05 meters.
        float l_percent = 0.5f;
        float l_slop = 0.03f;

        // Calculate the magnitude of correction needed.
        sf::Vector2f l_correction =
                (std::max(a_contact.m_minPenetration - l_slop, 0.0f) / (l_inverseMassOne + l_inverseMassTwo)) *
                l_percent * a_contact.m_normal;

        // Apply correction. Objects with infinite mass will not be corrected.
        lp_transformOne->translateByVector(-(l_correction * l_inverseMassOne));
        lp_transformTwo->translateByVector(l_correction * l_inverseMassTwo);

        // Update the origin points of the colliders.
        lp_colliderOne->updateOrigin();
        lp_colliderTwo->updateOrigin();

        // Get the distance between the colliders' center points.
        sf::Vector2f l_distance = lp_colliderTwo->getCenter() - lp_colliderOne->getCenter();

        // Use that distance and the colliders' half-extents to calculate an overlap.
        sf::Vector2f l_overlap =
                (lp_colliderOne->getHalfsize() + lp_colliderTwo->getHalfsize()) - Math::getVectorAbs(l_distance);

        // Update the minimum and maximum penetration depths of this contact.
        a_contact.m_minPenetration = std::min(l_overlap.x, l_overlap.y);
        a_contact.m_maxPenetration = std::max(l_overlap.x, l_overlap.y);
    }

    void PhysicsSystem::fixedUpdate (const Entity::Container &a_entities, const sf::Time &a_timestep, const unsigned int a_entityCount)
    {
        // Register physics objects.
        m_objects.clear();
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);
            isEntityValid(l_ptr.get());
        }

        // Detect collisions.
        m_contacts.clear();
        for (auto l_iterOne = m_objects.begin(); l_iterOne != m_objects.end(); ++l_iterOne)
        {
            for (auto l_iterTwo = std::next(l_iterOne); l_iterTwo != m_objects.end(); ++l_iterTwo)
            {
                checkCollision(*l_iterOne, *l_iterTwo);
            }
        }

        // Integrate force.
        for (PhysicsObject& l_object : m_objects)
        {
            integrateForce(l_object, a_timestep.asSeconds());
        }

        // Resolve collisions.
        for (int i = 0; i < m_velocityIters; ++i)
        {
            for (PhysicsContact& l_contact : m_contacts)
            {
                resolveCollision(l_contact);
            }
        }

        // Integrate velocity.
        for (PhysicsObject& l_object : m_objects)
        {
            integrateVelocity(l_object, a_timestep.asSeconds());
        }

        // Correct positions.
        for (int i = 0; i < m_positionIters; ++i)
        {
            for (PhysicsContact& l_contact : m_contacts)
            {
                correctPosition(l_contact);
            }
        }

        // Reset force.
        for (PhysicsObject& l_object : m_objects)
        {
            clearForce(l_object);
        }
    }

}
