// File: Aaros_Component_Rigidbody.hpp
//
// Allows the owning entity to be affected by physics.

#pragma once

#include <SFML/System/Clock.hpp>
#include "Aaros_Component.hpp"
#include "Aaros_Utils_Math.hpp"

namespace Aaros
{

    /**
     * This entity component allows the owning entity to be affected by physics.
     */
    class RigidbodyComponent : public Component
    {
    private /* Members */:
        sf::Vector2f            m_velocity;             /** How fast is the rigidbody moving? */
        sf::Vector2f            m_force;                /** The sum of all forces being applied upon the rigidbody. */
        float                   m_mass;                 /** How heavy is the rigidbody? How hard is it to move? */
        float                   m_inverseMass;          /** The inverse mass of the rigidbody. */
        float                   m_drag;                 /** How quickly shall the rigidbody come to rest after a collision or force? */
        float                   m_gravityScale;         /** On a scale between 0 and 1, how much is the rigidbody affected by gravity? */
        bool                    m_kinematic;            /** Should the rigidbody itself be affected by the physics engine? */

    private /* Setter Callbacks */:
        /** Called when the rigidbody's mass is changed. */
        void                    onMassChanged ();

    public /* Constructor and Destructor */:
        RigidbodyComponent (Entity* ap_entity,
                            const std::string& a_entityName,
                            const unsigned int a_entityID);
        ~RigidbodyComponent ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        /**
         * Applies a force upon the rigidbody. Applied forces affect the rigidbody's velocity,
         * which in turn affects the transform's position.
         *
         * @param   a_x         The force being applied along the X axis.
         * @param   a_y         The force being applied along the Y axis.
         */
        void                    applyForce (const float a_x, const float a_y);

    public /* Internal Methods */:
        /**
         * Applies a force upon the rigidbody. This is the internal version of the 'applyForce' method,
         * called from within the physics system.
         *
         * @param   a_force     A vector representing the force being applied.
         */
        void                    applyForceVector (const sf::Vector2f& a_force);

        /**
         * Applies an impulse upon the rigidbody, directly affecting its velocity, usually as a
         * result of a collision with another rigidbody. This is called internally from the physics system.
         *
         * @param   a_impulse   A vector representing the impulse being applied.
         */
        void                    applyImpulseVector (const sf::Vector2f& a_impulse);

        /**
         * Explicitly applies the given velocity vector upon the rigidbody.
         * This is called internally from the physics system.
         *
         * @param   a_vector    The velocity vector to be applied upon the rigidbody.
         */
        void                    applyVelocityVector (const sf::Vector2f& a_vector);

        /**
         * Applies a drag force to the rigidbody, causing it to slow down on account of atomshperic friction.
         * This is called internally from the physics system.
         *
         * @param   a_time      The timestep fed by the physics system.
         */
        void                    applyDrag (const float a_time);

    public /* Getter-Setters */:
        inline sf::Vector2f     getVelocity () const { return m_velocity; }
        inline sf::Vector2f     getForce () const { return m_force; }
        inline float            getVelocityX () const { return m_velocity.x; }
        inline float            getVelocityY () const { return m_velocity.y; }
        inline float            getPixelVelocityX () const { return m_velocity.x * Constants::G_METERS_TO_PIXELS_F; }
        inline float            getPixelVelocityY () const { return m_velocity.y * Constants::G_METERS_TO_PIXELS_F; }
        inline float            getForceX () const { return m_force.x; }
        inline float            getForceY () const { return m_force.y; }
        inline float            getMass () const { return m_mass; }
        inline float            getInverseMass () const { return m_inverseMass; }
        inline float            getDrag () const { return m_drag; }
        inline float            getGravityScale () const { return m_gravityScale; }
        inline bool             isKinematic () const { return m_kinematic; }

        inline void setVelocity (const float a_x, const float a_y) { m_velocity = { a_x, a_y }; }
        inline void setMass (const float a_mass) { m_mass = std::abs(a_mass); onMassChanged(); }
        inline void makeStatic () { m_mass = 0.0f; onMassChanged(); }
        inline void setDrag (const float a_drag) { m_drag = std::abs(a_drag); }
        inline void setGravityScale (const float a_scale) { m_gravityScale = Math::clampNumber(a_scale, 0.0f, 1.0f); }
        inline void setKinematic (const bool a_kinematic) { m_kinematic = a_kinematic; }

    public /* Internal Methods */:
        inline void resetForce () { m_force = { 0.0f, 0.0f }; }

    };

}
