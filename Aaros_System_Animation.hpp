// File: Aaros_System_Animation.hpp
//
// This system animates entities that have sprite and animation components.

#pragma once

#include "Aaros_System.hpp"

namespace Aaros
{

    class AnimationSystem : public System
    {
    private /* Methods */:
        bool                    isEntityValid (Entity *ap_entity) override;

    public /* Methods */:
        void                    update (const Entity::Container &a_entities,
                                        const sf::Time &a_deltaTime,
                                        const unsigned int a_entityCount) override;
    };

}
