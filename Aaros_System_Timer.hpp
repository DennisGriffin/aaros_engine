// File: Aaros_System_Timer.hpp
//
// Runs operations on entities with timer components.

#pragma once

#include "Aaros_System.hpp"

namespace Aaros
{

    class TimerSystem : public System
    {
    private /* Methods */:
        bool                isEntityValid (Entity *ap_entity) override;

    public /* Methods */:
        void                update (const Entity::Container &a_entities,
                                    const sf::Time &a_deltaTime,
                                    const unsigned int a_entityCount) override;
        void                updateCameraRect (const sf::View& a_view) override;
    };

}
