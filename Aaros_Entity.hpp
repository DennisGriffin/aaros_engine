// File: Aaros_Entity.hpp
//
// Represents a game object in our engine.

#pragma once

#include <sol.hpp>
#include "Aaros_PropertyManager.hpp"
#include "Aaros_Component_Transform.hpp"
#include "Aaros_Component_Rigidbody.hpp"
#include "Aaros_Component_Collider.hpp"
#include "Aaros_Component_Animation.hpp"
#include "Aaros_Component_Text.hpp"
#include "Aaros_Component_Sprite.hpp"
#include "Aaros_Component_Tilemap.hpp"
#include "Aaros_Component_Emitter.hpp"
#include "Aaros_Component_Script.hpp"
#include "Aaros_Component_Timer.hpp"
#include "Aaros_Component_Widget.hpp"
#include "Aaros_Component_Sound.hpp"

namespace Aaros
{

    /** Forward-declare the Scene class. */
    class Scene;

    /**
     * This class represents a game object in the Aaros Engine. By itself, it only
     * consists of a name and an ID number. However, its functionality can be expanded upon
     * by attaching entity components.
     */
    class Entity : public Utils::XmlLoadable
    {
    public /* Typedefs */:
        using Ptr               = std::unique_ptr<Entity>;
        using Container         = std::vector<Ptr>;
        using Iter              = Container::iterator;

    private /* Static Members */:
        static unsigned int     s_uid;                  /** The next available unique ID (UID) number. */

    private /* Members */:
        Component::Typemap      m_components;           /** The map of the entity's loaded components. */
        TransformComponent*     mp_transform;           /** A pointer to the entity's transform component. */
        Scene*                  mp_scene;               /** A pointer to the scene that loaded this entity. */
        sol::state*             mp_lua;                 /** A pointer to the scene's Lua state. */
        std::string             m_name;                 /** The name of the entity itself. */
        std::string             m_path;                 /** The hierarchal path to this entity. Blank for top-level "root" entities. */
        std::string             m_tag;                  /** Lists the entity under a certain category. */
        unsigned int            m_uid;                  /** The entity's ID number. */
        int                     m_order;                /** When should the systems operate upon this entity's components? */
        bool                    m_alive;                /** Should the entity and its children be unloaded at the end of the frame? */
        bool                    m_ancestorDead;         /** Is one of the entities further up this entity's hierarchy marked for unloading? */
        bool                    m_enabled;              /** Should the systems bother with this entity's components, or those of its children? */
        bool                    m_ancestorDisabled;     /** Is one of the entities further up this entity's hierarchy disabled? */
        bool                    m_persistent;           /** Should the entity and its children remain loaded when a new scene is loaded? */
        bool                    m_ancestorPersistent;   /** Is one of the entities further up this entity's hierarchy marked as persistent? */

    private /* Setter Callbacks */:
        /**
         * This callback method is called when the entity's run order is changed. The run order
         * of an entity determines when, during a given frame, its components are operated upon by
         * the entity systems managed by the scene. Entities with higher run orders are processed later
         * in the frame than entities with lower run orders.
         *
         * An entity's run order is not affected by its position in an entity hierarchy.
         */
        void                    onOrderChanged ();

        /**
         * Called when the entity's alive flag is changed, this method will travel down
         * this entity's hierarchy and set the alive flag of all of this entity's children,
         * their children and so on to the state of this entity's alive flag.
         *
         * The alive flag of a parent entity takes precedence over the alive flags of its children.
         */
        void                    onAliveChanged ();

        /**
         * Called when the entity's enabled flag is changed, this method instructs the entity's
         * attached transform component to set its enabled flag to false, upon which the ancestorDisabled
         * flag of all of the transform's descendents (as well as their owning entities) are modified
         * accordingly.
         */
        void                    onEnabledChanged ();

        /**
         * This callback is invoked when the entity's persistence flag is changed. Persistent entities
         * and their children are not unloaded whenever a new scene is loaded. This is useful for keeping
         * certain entities (like a heads-up display, for instance) on screen in games with multiple rooms.
         *
         * The persistent flag of a parent entity takes precedence over the persistent flags of its children.
         */
        void                    onPersistentChanged ();

    public /* Constructor and Destructor */:
        Entity (Scene* ap_scene,
                sol::state* ap_lua,
                const std::string& a_name,
                const std::string& a_tag = "");
        ~Entity ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Non-Templated Methods */:
        /**
         * Attempts to retrieve an entity that is loaded in this scene.
         *
         * @param   a_path              The name of the entity, with a path if it is part of a hierarchy.
         *
         * @return  The entity with the given name.
         */
        Entity&                 getEntityInScene (const std::string& a_path);

        /**
         * Checks to see if this entity has a child attached with the given name.
         *
         * @param   a_name              The name of the child entity in question.
         *
         * @return  True if the given entity is a child of this entity, false if not.
         */
        bool                    hasChildEntity (const std::string& a_name);

        /**
         * Attempts to retrieve a child entity with the given name.
         *
         * @param   a_name              The name of the child entity.
         *
         * @return  The child entity with the given name.
         */
        Entity&                 getChildEntity (const std::string& a_name);

        /**
         * Attempts to disassociate the child entity with the given name from this entity.
         *
         * @param   a_name              The name of the child entity.
         */
        void                    detachChildEntity (const std::string& a_name);

        /**
         * Attempts to disassociate all child entities from this entity.
         */
        void                    detachAllChildren ();

        /**
         * Sets this entity's parent to the one with the given path.
         *
         * @param   a_path              The path to the entity given.
         */
        void                    setParentEntity (const std::string& a_path);

    public /* Templated Methods */:
        /**
         * Checks to see if a component of the given type is attached to the entity.
         *
         * @return  True if the component is attached, false if not.
         */
        template <typename T>
        bool                    hasComponent ();

        /**
         * Creates a new component and attaches it to the game object. If a component
         * of the given type is already attached, then that component is returned instead.
         *
         * @param   apk_args                The arguments that the component may require for construction, if any.
         *
         * @return  The newly-created (or already-existing) component.
         */
        template <typename T, typename... R>
        T&                      addComponent (R&&... apk_args);

        /**
         * Attempts to retrieve the attached component of the given type.
         *
         * @return  The component of the given type.
         */
        template <typename T>
        T&                      getComponent ();

        /**
         * Attempts to remove the component of the given type.
         */
        template <typename T>
        void                    removeComponent ();

        /**
         * OK, we're going to deviate a bit from ECS standards and grant the entity and its
         * components access to the Script component, if it exists, and allow them to call
         * Lua scripts through this method, with the specified arguments.
         *
         * Of course, all Lua script functions to be called by the entity and components must
         * first be registered with the entity's Script component.
         *
         * @param   a_functionName          The name of the script function.
         * @param   apk_functionArgs        The arguments that the function accepts.
         */
        template <typename... Args>
        void callLuaFunction (const std::string& a_functionName, Args&&... apk_functionArgs);

    public /* Internal Methods */:
        void                    detachOnKill ();

    public /* Outline Getter-Setters */:
        std::string             getFullName () const;

    public /* Inline Getter-Setters */:
        inline Scene*           getScene () const { return mp_scene; }
        inline std::string      getSelfName () const { return m_name; }
        inline std::string      getTag () const { return m_tag; }
        inline unsigned int     getID () const { return m_uid; }
        inline int              getOrder () const { return m_order; }
        inline bool             isAlive () const { return m_alive && !m_ancestorDead; }
        inline bool             isEnabled () const { return m_enabled && !m_ancestorDisabled; }
        inline bool             isPersistent () const { return m_persistent || m_ancestorPersistent; }
        inline bool             isRoot () const { return mp_transform->isRoot(); }

        inline void setTag (const std::string& a_tag) { m_tag = a_tag; }
        inline void setOrder (const int a_order) { m_order = a_order; onOrderChanged(); }
        inline void setAlive (const bool a_alive) { m_alive = a_alive; onAliveChanged(); }
        inline void setEnabled (const bool a_enabled) { m_enabled = a_enabled; onEnabledChanged(); }
        inline void setPersistent (const bool a_persistent) { m_persistent = a_persistent; onPersistentChanged(); }

    public /* Internal Getter-Setters */:
        inline TransformComponent* getTransform () const { return mp_transform; }
        inline bool             isSelfAlive () const { return m_alive; }
        inline bool             isAncestorDead () const { return m_ancestorDead; }
        inline bool             isSelfEnabled () const { return m_enabled; }
        inline bool             isAncestorDisabled () const { return m_ancestorDisabled; }
        inline bool             isSelfPersistent () const { return m_persistent; }
        inline bool             isAncestorPersistent () const { return m_ancestorPersistent; }
        inline std::string      getHierarchyPath () const { return m_path; }

        inline void setAncestorDead (const bool a_dead) { m_ancestorDead = a_dead; }
        inline void setAncestorDisabled (const bool a_disabled) { m_ancestorDisabled = a_disabled; }
        inline void setAncestorPersistent (const bool a_persistent) { m_ancestorPersistent = a_persistent; }
        inline void setHierarchyPath (const std::string& a_path) { m_path = a_path; }

    };

    template <typename T>
    inline bool Entity::hasComponent ()
    {
        // Check to see if the component is attached.
        std::type_index l_type = { typeid(T) };
        return m_components.find(l_type) != m_components.end();
    }

    template <typename T, typename... R>
    inline T &Entity::addComponent(R&&... apk_args)
    {
        // Index the type that was given.
        std::type_index l_type = { typeid(T) };

        // Check to see if a component of the given type is already attached.
        auto l_find = m_components.find(l_type);
        if (l_find != m_components.end())
        {
            // If so, then just return that component.
            T* l_downcast = dynamic_cast<T*>(l_find->second.get());
            return *l_downcast;
        }

        // Create the component, using the parameter pack we were given.
        m_components[l_type] = std::make_unique<T>(this, m_name, m_uid, std::forward<R>(apk_args)...);

        // Downcast, de-reference, and return the pointer.
        T* lp_downcast = dynamic_cast<T*>(m_components[l_type].get());
        return *lp_downcast;
    }

    template <typename T>
    inline T& Entity::getComponent ()
    {
        // Index the given type. Get its name, too, as this method can throw.
        std::type_index l_type = { typeid(T) };
        std::string l_typestr = l_type.name();

        // Find the component. If it is not attached, then throw.
        auto l_find = m_components.find(l_type);
        if (l_find == m_components.end())
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorEntityName", false).set(m_name);
            PM.append("errorEntityID", false).setAs<unsigned int>(m_uid);
            PM.append("errorComponentType", false).set(l_typestr);

            throw std::runtime_error { "[Entity::getComponent] Attempted to fetch a non-existing component from entity '" + m_name + "'!" };
        }

        // Downcast, de-reference, and return the pointer.
        T* lp_downcast = dynamic_cast<T*>(l_find->second.get());
        return *lp_downcast;
    }

    template <typename T>
    inline void Entity::removeComponent ()
    {
        // Do not remove components that are considered vital.
        if (TT_Is_Component_Vital<T>::Value == true) { return; }

        // Find the component. If it exists, then remove it.
        auto l_find = m_components.find({ typeid(T) });
        if (l_find != m_components.end())
        {
            l_find = m_components.erase(l_find);
        }
    }

    template <typename... Args>
    inline void Entity::callLuaFunction (const std::string& a_functionName, Args&&... apk_functionArgs)
    {
        if (isEnabled() && hasComponent<ScriptComponent>())
        {
            getComponent<ScriptComponent>().callFunction(a_functionName, std::forward<Args>(apk_functionArgs)...);
        }
    }

}
