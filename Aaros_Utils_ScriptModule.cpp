// File: Aaros_Utils_ScriptModule.cpp

#include "Aaros_Utils_ScriptModule.hpp"

namespace Aaros
{

    ScriptModule::ScriptModule(const std::string &a_xmlFile, const bool a_enabled) :
        m_scriptFile            { "" },
        m_enabled               { a_enabled }
    {
        loadFromFile(a_xmlFile, "Module");
    }

    ScriptModule::~ScriptModule()
    {
        m_functions.clear();
        m_properties.clear();
        m_scriptFile.clear();
        m_enabled = false;
    }

    void ScriptModule::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_attribute l_scriptAttr = a_node.attribute("Script");
        xml_attribute l_enabledAttr = a_node.attribute("Enabled");

        if (!l_scriptAttr.empty()) { m_scriptFile = l_scriptAttr.as_string(); }
        if (!l_enabledAttr.empty()) { m_enabled = l_enabledAttr.as_bool(true); }

        for (const xml_node& l_propNode : a_node.children("Property"))
        {
            std::string l_propKey = l_propNode.attribute("Key").as_string();
            std::string l_propValue = l_propNode.attribute("Value").as_string();

            if (!l_propKey.empty()) { setString(l_propKey, l_propValue); }
        }

        for (const xml_node& l_funcNode : a_node.children("Function"))
        {
            std::string l_funcName = l_funcNode.attribute("Name").as_string();
            bool l_enabled = l_funcNode.attribute("Enabled").as_bool(true);

            if (!l_funcName.empty()) { setFunction(l_funcName, l_enabled); }
        }
    }

    bool ScriptModule::isFunctionEnabled(const std::string &a_name)
    {
        auto l_find = m_functions.find(a_name);
        if (l_find != m_functions.end())
        {
            return l_find->second;
        }

        return false;
    }

    std::string ScriptModule::getString(const std::string &a_name, const std::string &a_default)
    {
        auto l_find = m_properties.find(a_name);
        if (l_find != m_properties.end())
        {
            return l_find->second;
        }

        return a_default;
    }

    int ScriptModule::getInteger(const std::string &a_name, const int a_default)
    {
        int l_return = 0;

        try { l_return = std::stoi(getString(a_name)); }
        catch (...) { l_return = a_default; }

        return l_return;
    }

    float ScriptModule::getNumber(const std::string &a_name, const float a_default)
    {
        float l_return = 0.0f;

        try { l_return = std::stof(getString(a_name)); }
        catch (...) { l_return = a_default; }

        return l_return;
    }

    bool ScriptModule::getBoolean(const std::string &a_name, const bool a_default)
    {
        std::string l_value = getString(a_name);
        if (l_value == "true") { return true; }
        else if (l_value == "false") { return false; }

        return a_default;
    }

    void ScriptModule::setFunction(const std::string &a_name, const bool a_enabled)
    {
        m_functions[a_name] = a_enabled;
    }

    void ScriptModule::setString(const std::string &a_name, const std::string &a_value)
    {
        m_properties[a_name] = a_value;
    }

    void ScriptModule::setInteger(const std::string &a_name, const int a_value)
    {
        m_properties[a_name] = std::to_string(a_value);
    }

    void ScriptModule::setNumber(const std::string &a_name, const float a_value)
    {
        m_properties[a_name] = std::to_string(a_value);
    }

    void ScriptModule::setBoolean(const std::string &a_name, const bool a_value)
    {
        if (a_value == true) { m_properties[a_name] = "true"; }
        else { m_properties[a_name] = "false"; }
    }

    void ScriptModule::importFromGlobal(const std::string &a_name)
    {
        PropertyManager& PM = PropertyManager::getInstance();

        m_properties[a_name] = PM.get(a_name).get();
    }

    void ScriptModule::exportToGlobal(const std::string &a_name)
    {
        PropertyManager& PM = PropertyManager::getInstance();

        PM.append(a_name, false).set(getString(a_name));
    }

}
