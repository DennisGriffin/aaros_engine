// File: Aaros_GUI_Scrollbar.cpp

#include "Aaros_GUI_Scrollbar.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace GUI
    {

        void Scrollbar::updateWidgetInternals()
        {
            m_body.setPosition(m_position);
            float l_minPosition = 0.0f, l_maxPosition = 0.0f, l_space = 0.0f, l_pos = 0.0f;

            if (m_orientation == ScrollbarOrientation::Vertical)
            {
                m_body.setSize({ m_shortSize, m_longSize });
                m_plusButton.setSize({ m_shortSize, Constants::G_SCROLLBAR_SHORT_SIZE });
                m_minusButton.setSize({ m_shortSize, Constants::G_SCROLLBAR_SHORT_SIZE });
                m_positionButton.setSize({ m_shortSize, Constants::G_SCROLLBAR_SHORT_SIZE });

                m_minusButton.setPosition(m_position);
                m_plusButton.setPosition(m_position.x, m_position.y + (m_longSize - Constants::G_SCROLLBAR_SHORT_SIZE));

                l_minPosition = m_position.y + Constants::G_SCROLLBAR_SHORT_SIZE + 4.0f;
                l_maxPosition = m_position.y + (m_longSize - (Constants::G_SCROLLBAR_SHORT_SIZE * 2.0f) - 4.0f);
                l_space = l_maxPosition - l_minPosition;
                l_pos = (static_cast<float>(m_value) / static_cast<float>(m_maximum)) * l_space;

                m_positionButton.setPosition(m_position.x, l_minPosition + l_pos);
            }
            else
            {
                m_body.setSize({ m_longSize, m_shortSize });
                m_plusButton.setSize({ Constants::G_SCROLLBAR_SHORT_SIZE, m_shortSize });
                m_minusButton.setSize({ Constants::G_SCROLLBAR_SHORT_SIZE, m_shortSize });
                m_positionButton.setSize({ Constants::G_SCROLLBAR_SHORT_SIZE, m_shortSize });

                m_minusButton.setPosition(m_position);
                m_plusButton.setPosition(m_position.x + (m_longSize - Constants::G_SCROLLBAR_SHORT_SIZE), m_position.y);

                l_minPosition = m_position.x + Constants::G_SCROLLBAR_SHORT_SIZE + 4.0f;
                l_maxPosition = m_position.x + (m_longSize - (Constants::G_SCROLLBAR_SHORT_SIZE * 2.0f) - 4.0f);
                l_space = l_maxPosition - l_minPosition;
                l_pos = (static_cast<float>(m_value) / static_cast<float>(m_maximum)) * l_space;

                m_positionButton.setPosition(l_minPosition + l_pos, m_position.y);
            }

            if (m_focused == true)
            {
                m_body.setFillColor({ 200, 200, 200 });
            }
            else
            {
                m_body.setFillColor(Constants::G_SCROLLBAR_BACK_COLOR);
            }

            updateButtons(m_plusButton, 0);
            updateButtons(m_minusButton, 1);
            updateButtons(m_positionButton, 2);
        }

        void Scrollbar::updateButtons(sf::RectangleShape &a_button, const int a_id)
        {
            Mouse& M = Mouse::getInstance();
            bool l_activated = false;

            if (a_button.getGlobalBounds().contains(M.getPosition()) == true && a_id != 2)
            {
                a_button.setFillColor(m_hoverColor);

                l_activated = M.isButtonPressed(sf::Mouse::Left);

                if (l_activated == true)
                {
                    if (a_id == 0 && m_value != m_maximum)
                    {
                        m_value++;
                    }
                    else if (a_id == 1 && m_value != 0)
                    {
                        m_value--;
                    }
                }
            }
            else
            {
                a_button.setFillColor(m_buttonColor);
            }
        }

        Scrollbar::Scrollbar(Entity *ap_entity, const std::string &a_name) :
            Widget              { ap_entity, a_name },
            m_backColor         { Constants::G_SCROLLBAR_BACK_COLOR },
            m_buttonColor       { Constants::G_SCROLLBAR_BUTTON_COLOR },
            m_hoverColor        { Constants::G_SCROLLBAR_HOVER_COLOR },
            m_orientation       { ScrollbarOrientation::Vertical },
            m_longSize          { Constants::G_SCROLLBAR_LONG_SIZE },
            m_shortSize         { Constants::G_SCROLLBAR_SHORT_SIZE },
            m_value             { 0 },
            m_maximum           { 10 }
        {
            m_body.setOutlineThickness(Constants::G_SCROLLBAR_THICKNESS);
            m_plusButton.setOutlineThickness(Constants::G_SCROLLBAR_THICKNESS);
            m_minusButton.setOutlineThickness(Constants::G_SCROLLBAR_THICKNESS);
            m_positionButton.setOutlineThickness(Constants::G_SCROLLBAR_THICKNESS);

            m_body.setOutlineColor(sf::Color::Black);
            m_plusButton.setOutlineColor(sf::Color::Black);
            m_minusButton.setOutlineColor(sf::Color::Black);
            m_positionButton.setOutlineColor(sf::Color::Black);

            m_focusable = true;

            updateWidgetInternals();
        }

        Scrollbar::~Scrollbar()
        {
            m_backColor = { 0, 0, 0 };
            m_buttonColor = { 0, 0, 0 };
            m_hoverColor = { 0, 0, 0 };
            m_longSize = 0.0f;
            m_shortSize = 0.0f;
            m_value = 0;
            m_maximum = 0;

            m_enabled = false;
            m_visible = false;
            m_focused = false;
            m_focusable = false;
            m_name.clear();
            m_position = { 0.0f, 0.0f };
            mp_entity = nullptr;
        }

        void Scrollbar::loadFromXML(const pugi::xml_node &a_node)
        {
            using namespace pugi;

            xml_node l_positionNode = a_node.child("Position");
            xml_node l_sizeNode = a_node.child("Size");
            xml_node l_colorNode = a_node.child("Color");
            xml_node l_scrollNode = a_node.child("Scrollbar");

            float l_x = l_positionNode.attribute("X").as_float();
            float l_y = l_positionNode.attribute("Y").as_float();

            float l_short = l_sizeNode.attribute("Short").as_float(Constants::G_SCROLLBAR_SHORT_SIZE);
            float l_long = l_sizeNode.attribute("Long").as_float(Constants::G_SCROLLBAR_LONG_SIZE);

            int l_bodyR = l_colorNode.child("Body").attribute("Red").as_int(Constants::G_SCROLLBAR_BACK_COLOR.r);
            int l_bodyG = l_colorNode.child("Body").attribute("Green").as_int(Constants::G_SCROLLBAR_BACK_COLOR.g);
            int l_bodyB = l_colorNode.child("Body").attribute("Blue").as_int(Constants::G_SCROLLBAR_BACK_COLOR.b);
            int l_buttonR = l_colorNode.child("Button").attribute("Red").as_int(Constants::G_SCROLLBAR_BUTTON_COLOR.r);
            int l_buttonG = l_colorNode.child("Button").attribute("Green").as_int(Constants::G_SCROLLBAR_BUTTON_COLOR.g);
            int l_buttonB = l_colorNode.child("Button").attribute("Blue").as_int(Constants::G_SCROLLBAR_BUTTON_COLOR.b);
            int l_hoverR = l_colorNode.child("Hover").attribute("Red").as_int(Constants::G_SCROLLBAR_HOVER_COLOR.r);
            int l_hoverG = l_colorNode.child("Hover").attribute("Green").as_int(Constants::G_SCROLLBAR_HOVER_COLOR.g);
            int l_hoverB = l_colorNode.child("Hover").attribute("Blue").as_int(Constants::G_SCROLLBAR_HOVER_COLOR.b);

            unsigned int l_value = l_scrollNode.attribute("Value").as_uint();
            unsigned int l_maximum = l_scrollNode.attribute("Maximum").as_uint(10);
            std::string l_orientation = l_scrollNode.attribute("Orientation").as_string("Vertical");

            m_position = { l_x, l_y };
            m_shortSize = l_short;
            m_longSize = l_long;
            m_backColor = Utils::createColor(l_bodyR, l_bodyG, l_bodyB);
            m_buttonColor = Utils::createColor(l_buttonR, l_buttonG, l_buttonB);
            m_hoverColor = Utils::createColor(l_hoverR, l_hoverG, l_hoverB);
            setMaximum(l_maximum);
            setValue(l_value);

            if (l_orientation == "Horizontal")  { m_orientation = ScrollbarOrientation::Horizontal; }
            else                                { m_orientation = ScrollbarOrientation::Vertical; }

            updateWidgetInternals();
        }

        void Scrollbar::update(const sf::Time &a_deltaTime)
        {
            (void) a_deltaTime;

            if (m_focused == true)
            {
                Mouse& M = Mouse::getInstance();
                float l_delta = M.getWheelDelta();

                if (l_delta < 0 && m_value != m_maximum)
                {
                    m_value++;
                }
                else if (l_delta > 0 && m_value != 0)
                {
                    m_value--;
                }
            }

            updateWidgetInternals();
        }

        void Scrollbar::render(sf::RenderWindow &a_window)
        {
            a_window.draw(m_body);
            a_window.draw(m_plusButton);
            a_window.draw(m_minusButton);
            a_window.draw(m_positionButton);
        }

        void Scrollbar::setValue(const unsigned int a_value)
        {
            if (a_value > m_maximum) { m_value = m_maximum; }

            m_value = a_value;
        }

        void Scrollbar::setMaximum(const unsigned int a_value)
        {
            if (a_value < 1) { m_maximum = 1; }

            m_maximum = a_value;
            m_value = 0;
        }

    }
}
