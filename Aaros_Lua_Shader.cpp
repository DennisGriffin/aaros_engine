// File: Aaros_Lua_Shader.cpp

#include "Aaros_Lua_Shader.hpp"
#include "Aaros_Shader.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeShader (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<Shader> l_userdata
            {
                "Shader", l_ctors,

                "setFloat",             &Shader::setFloat,
                "setVector2",           &Shader::setVec2,
                "setVector3",           &Shader::setVec3,
                "setVector4",           &Shader::setVec4,
                "setCurrentTexture",    &Shader::setCurrentTexture,

                "getVertexFilename",    &Shader::getVertexShaderFile,
                "getFragmentFilename",  &Shader::getFragmentShaderFile,
                "isEnabled",            &Shader::isEnabled,
                "setEnabled",           &Shader::setEnabled
            };
            a_state.set_userdata(l_userdata);
        }

    }
}
