// File: Aaros_System_Widget.hpp
//
// Performs operations on entities with widget host components.

#pragma once

#include "Aaros_System.hpp"

namespace Aaros
{

    class WidgetSystem : public System
    {
    private /* Methods */:
        bool                    isEntityValid (Entity *ap_entity) override;

    public /* Methods */:
        void                    update (const Entity::Container &a_entities,
                                        const sf::Time &a_deltaTime,
                                        const unsigned int a_entityCount) override;
        void                    render (const Entity::Container &a_entities,
                                        sf::RenderWindow &a_window,
                                        const unsigned int a_entityCount) override;
    };

}
