// File: Aaros_Scene.hpp
//
// Represents the collection of entities that make up a level.

#pragma once

#include <SFML/Audio/Listener.hpp>
#include "Aaros_System_Animation.hpp"
#include "Aaros_System_Physics.hpp"
#include "Aaros_System_Render.hpp"
#include "Aaros_System_Script.hpp"
#include "Aaros_System_Timer.hpp"
#include "Aaros_System_Widget.hpp"
#include "Aaros_System_Sound.hpp"

namespace Aaros
{

    /** Forward-declare the Application class. */
    class Application;

    /**
     * If the Application class is the heart of the Aaros Engine, then the
     * Scene class has to be its brain. This class is in charge of the following:
     *
     * - Maintaining a list of all currently loaded entities.
     * - Managing the Systems, which operate upon said entities' components.
     * - Maintaining the engine's Lua state.
     */
    class Scene : public Utils::XmlLoadable
    {
    public /* Typedefs */:
        using Ptr               = std::unique_ptr<Scene>;

    private /* Members */:
        Application*            mp_application;         /** A pointer to the application class. */

        Entity::Container       m_entities;             /** The container of loaded entities. */
        unsigned int            m_entityCount;          /** The current entity count. */
        sol::state              m_lua;                  /** The engine's Lua state. */
        bool                    m_paused;               /** Is the game paused? */
        sf::View                m_camera;               /** The game camera. */
        Entity*                 mp_center;              /** The entity upon which the camera will center. */

        AnimationSystem         m_animationSystem;      /** The animation system. */
        PhysicsSystem           m_physicsSystem;        /** The physics system. */
        RenderSystem            m_renderSystem;         /** The rendering system. */
        ScriptSystem            m_scriptSystem;         /** The scripting system. */
        TimerSystem             m_timerSystem;          /** The timer system. */
        WidgetSystem            m_widgetSystem;         /** The GUI widget system. */
        SoundSystem             m_soundSystem;          /** The sound system. */

        bool                    m_newSceneRequested;    /** Was a new scene requested by the user? */
        std::string             m_requestedSceneFile;   /** The requested scene file. */

    private /* Search Methods */:
        /** Seeks out a loaded game object by the name given. */
        Entity::Iter            findEntityByName (const std::string& a_name);

    private /* Methods */:
        /**
         * Initializes the Lua state.
         */
        void                    initializeLua ();

        /**
         * Initializes the scene camera.
         */
        void                    initializeCamera ();

        /**
         * Loads a new scene if one is requested.
         */
        void                    loadRequestedScene ();

        /**
         * Unloads all entities marked as "dead".
         */
        void                    cullEntities ();

        /**
         * Called every time a new entity is added to the scene, this method sorts
         * all loaded entities by their run order.
         */
        void                    sortEntities ();

        /**
         * Updates the game camera.
         */
        void                    updateCamera ();

    public /* Constructor and Destructor */:
        Scene (Application* ap_application);
        ~Scene ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        void                    loadNew (const std::string& a_filename,
                                         const bool a_removePersistents = false);
        void                    loadAdditive (const std::string& a_filename);

        /**
         * Creates a new game object and adds it to the scene. Keep in mind that
         * more than one entity with the same name can exist within the scene.
         *
         * @param   a_name              The name of the new entity.
         *
         * @return  The newly-created entity.
         */
        Entity&                 addEntity (const std::string& a_name);

        /**
         * Attempts to retrieve the first loaded entity with the given name.
         *
         * @param   a_name              The name of the entity.
         *
         * @return  The first loaded root game object with the given name.
         */
        Entity&                 getEntity (const std::string& a_name);

        /**
         * Unloads all entities from the scene, except for persistents.
         *
         * @param   a_allOfThem         All entities, or only non-persistents?
         */
        void                    removeAllEntities (const bool a_allOfThem = false);

        /**
         * Centers the game camera on the given point. If the camera is focusing on an entity,
         * calling this method will remove that focus.
         *
         * @param   a_x                 The camera's new center point on the X axis.
         * @param   a_y                 The camera's new center point on the Y axis.
         */
        void                    centerCameraOnPoint (const float a_x, const float a_y);

        /**
         * Centers the game camera upon the center point of the transform belonging to the entity
         * with the given name.
         *
         * @param   a_entityName        The name of the entity to center upon.
         */
        void                    centerCameraOnEntity (const std::string& a_entityName);

    public /* Systems Operation Methods */:
        /** Runs standard update systems on all loaded entities. */
        void                    update (const sf::Time& a_deltaTime);

        /** Runs fixed update (physics) systems on all loaded entities. */
        void                    fixedUpdate (const sf::Time& a_timestep);

        /** Runs rendering systems on entities that draw something onscreen. */
        void                    render (sf::RenderWindow& a_window);

    public /* Outline Method Wrappers */:
        void                    setWindowSize (const unsigned int a_width,
                                               const unsigned int a_height);
        void                    setWindowFullscreen (const bool a_fullscreen);
        void                    quit ();

    public /* Getter-Setter Wrappers */:
        inline float            getGravityX () const { return m_physicsSystem.getGravity().x; }
        inline float            getGravityY () const { return m_physicsSystem.getGravity().y; }
        inline int              getVelocityIterCount () const { return m_physicsSystem.getVelocityIterCount(); }
        inline int              getPositionIterCount () const { return m_physicsSystem.getPositionIterCount(); }

        inline void setGravity (const float a_x, const float a_y) { m_physicsSystem.setGravity(a_x, a_y); }
        inline void setVelocityIterCount (const int a_iters) { m_physicsSystem.setVelocityIterCount(a_iters); }
        inline void setPositionIterCount (const int a_iters) { m_physicsSystem.setPositionIterCount(a_iters); }

    public /* Internal Getter-Setters */:
        inline sf::View&        getCameraView () { return m_camera; }

    };

}
