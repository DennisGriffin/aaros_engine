// File: Aaros_Lua_Keyboard.cpp

#include <functional>
#include "Aaros_Lua_Keyboard.hpp"
#include "Aaros_Keyboard.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeKeyboard (sol::state& a_state)
        {
            sol::table l_keys = a_state.create_table();
            auto l_expose = [&l_keys] (const sf::Keyboard::Key a_key, const std::string& a_index)
            {
                l_keys.set(a_index, static_cast<int>(a_key));
            };

            // Letter Keys
            l_expose(sf::Keyboard::A,       "A");
            l_expose(sf::Keyboard::B,       "B");
            l_expose(sf::Keyboard::C,       "C");
            l_expose(sf::Keyboard::D,       "D");
            l_expose(sf::Keyboard::E,       "E");
            l_expose(sf::Keyboard::F,       "F");
            l_expose(sf::Keyboard::G,       "G");
            l_expose(sf::Keyboard::H,       "H");
            l_expose(sf::Keyboard::I,       "I");
            l_expose(sf::Keyboard::J,       "J");
            l_expose(sf::Keyboard::K,       "K");
            l_expose(sf::Keyboard::L,       "L");
            l_expose(sf::Keyboard::M,       "M");
            l_expose(sf::Keyboard::N,       "N");
            l_expose(sf::Keyboard::O,       "O");
            l_expose(sf::Keyboard::P,       "P");
            l_expose(sf::Keyboard::Q,       "Q");
            l_expose(sf::Keyboard::R,       "R");
            l_expose(sf::Keyboard::S,       "S");
            l_expose(sf::Keyboard::T,       "T");
            l_expose(sf::Keyboard::U,       "U");
            l_expose(sf::Keyboard::V,       "V");
            l_expose(sf::Keyboard::W,       "W");
            l_expose(sf::Keyboard::X,       "X");
            l_expose(sf::Keyboard::Y,       "Y");
            l_expose(sf::Keyboard::Z,       "Z");

            // Number Keys
            l_expose(sf::Keyboard::Divide,      "Divide");
            l_expose(sf::Keyboard::Multiply,    "Multiply");
            l_expose(sf::Keyboard::Subtract,    "Subtract");
            l_expose(sf::Keyboard::Add,         "Add");
            l_expose(sf::Keyboard::Num0,    "Num0");
            l_expose(sf::Keyboard::Num1,    "Num1");
            l_expose(sf::Keyboard::Num2,    "Num2");
            l_expose(sf::Keyboard::Num3,    "Num3");
            l_expose(sf::Keyboard::Num4,    "Num4");
            l_expose(sf::Keyboard::Num5,    "Num5");
            l_expose(sf::Keyboard::Num6,    "Num6");
            l_expose(sf::Keyboard::Num7,    "Num7");
            l_expose(sf::Keyboard::Num8,    "Num8");
            l_expose(sf::Keyboard::Num9,    "Num9");

            // Function Keys
            l_expose(sf::Keyboard::F1,      "F1");
            l_expose(sf::Keyboard::F2,      "F2");
            l_expose(sf::Keyboard::F3,      "F3");
            l_expose(sf::Keyboard::F4,      "F4");
            l_expose(sf::Keyboard::F5,      "F5");
            l_expose(sf::Keyboard::F6,      "F6");
            l_expose(sf::Keyboard::F7,      "F7");
            l_expose(sf::Keyboard::F8,      "F8");
            l_expose(sf::Keyboard::F9,      "F9");
            l_expose(sf::Keyboard::F10,     "F10");
            l_expose(sf::Keyboard::F11,     "F11");
            l_expose(sf::Keyboard::F12,     "F12");

            // Arrow Keys
            l_expose(sf::Keyboard::Up,      "Up");
            l_expose(sf::Keyboard::Down,    "Down");
            l_expose(sf::Keyboard::Left,    "Left");
            l_expose(sf::Keyboard::Right,   "Right");

            // Symbol Keys
            l_expose(sf::Keyboard::Space,       "Spacebar");
            l_expose(sf::Keyboard::Dash,        "Dash");
            l_expose(sf::Keyboard::Equal,       "Equal");
            l_expose(sf::Keyboard::LBracket,    "LeftBracket");
            l_expose(sf::Keyboard::RBracket,    "RightBracket");
            l_expose(sf::Keyboard::BackSlash,   "Backslash");
            l_expose(sf::Keyboard::SemiColon,   "Semicolon");
            l_expose(sf::Keyboard::Quote,       "Quote");
            l_expose(sf::Keyboard::Comma,       "Comma");
            l_expose(sf::Keyboard::Period,      "Period");
            l_expose(sf::Keyboard::Slash,       "Frontslash");

            a_state["Key"] = l_keys;

            sol::constructors<> l_ctors;
            sol::userdata<Keyboard> l_userdata
            {
                "Keyboard", l_ctors,

                "isKeyPressed",     &Keyboard::isKeyPressed,
                "isKeyDown",        &Keyboard::isKeyDown
            };
            a_state.set_userdata(l_userdata)
                    .get<sol::table>("Keyboard")
                    .set_function("getInstance", Keyboard::getInstance);
        }

    }
}

