// File: Aaros_Entity.cpp

#include "Aaros_Entity.hpp"
#include "Aaros_Scene.hpp"

namespace Aaros
{

    unsigned int Entity::s_uid = 0;

    void Entity::onOrderChanged()
    {
        callLuaFunction("onRunOrderChanged", m_order);
    }

    void Entity::onAliveChanged()
    {
        mp_transform->setSelfAlive(m_alive);

        callLuaFunction("onAliveChanged", m_alive);
    }

    void Entity::onEnabledChanged()
    {
        mp_transform->setEnabled(m_enabled);

        callLuaFunction("onEnabledChanged", m_enabled);
    }

    void Entity::onPersistentChanged()
    {
        mp_transform->setSelfPersistent(m_persistent);

        callLuaFunction("onPersistentChanged", m_persistent);
    }

    Entity::Entity(Scene *ap_scene, sol::state *ap_lua, const std::string &a_name, const std::string &a_tag) :
        mp_transform            { nullptr },
        mp_scene                { ap_scene },
        mp_lua                  { ap_lua },
        m_name                  { a_name },
        m_path                  { "" },
        m_tag                   { a_tag },
        m_uid                   { Entity::s_uid++ },
        m_order                 { 0 },
        m_alive                 { true },
        m_ancestorDead          { false },
        m_enabled               { true },
        m_ancestorDisabled      { false },
        m_persistent            { false },
        m_ancestorPersistent    { false }
    {
        std::cout << "[Entity::Constructor] Creating Entity '" << m_name << "' (ID: " << m_uid << ")...";

        mp_transform = &addComponent<TransformComponent>();

        std::cout << " done." << std::endl;
    }

    Entity::~Entity()
    {
        m_name.clear();
        m_path.clear();
        m_tag.clear();

        mp_transform            = nullptr;
        mp_scene                = nullptr;
        mp_lua                  = nullptr;
        m_uid                   = 0;
        m_order                 = 0;
        m_alive                 = false;
        m_ancestorDead          = false;
        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_persistent            = false;
        m_ancestorPersistent    = false;

        m_components.clear();
    }

    void Entity::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        // First, check to see if the user provided an external source file to
        // load this game object. If so, then attempt to load that file.
        std::string l_source = a_node.attribute("Source").as_string();
        if (!l_source.empty()) { loadFromFile(l_source); }

        // Next, check to see if the user provided a tag string for the entity.
        //
        // A tag string groups the entity into a certain category of entities that share
        // that same string.
        xml_attribute l_tagAttrib = a_node.attribute("Tag");
        if (!l_tagAttrib.empty())
        {
            m_tag = l_tagAttrib.as_string();
        }

        // Next, check to see if the user specified a running order for the entity.
        // The entity's run order determines when its components are processed by the
        // entity systems.
        xml_attribute l_orderAttrib = a_node.attribute("Order");
        if (!l_orderAttrib.empty()) { m_order = l_orderAttrib.as_int(); }

        // Next, check to see if the user has specified this entity as a persistent.
        // Persistent entities and their children are not unloaded when a new scene is requested.
        // An entity's persistence flag is overridden by that of its parent.
        xml_attribute l_persistAttrib = a_node.attribute("Persistent");
        if (!l_persistAttrib.empty()) { m_persistent = l_persistAttrib.as_bool(); }

        // Next, load the entity's components.
        // Iterate through each child node named "Component".
        for (const xml_node& l_node : a_node.children("Component"))
        {
            // Get the type of component as a string.
            std::string l_type = l_node.attribute("Type").as_string();

            // Load the component requested.
            if      (l_type == "Transform")         { mp_transform->loadFromXML(l_node); }
            else if (l_type == "Rigidbody")         { addComponent<RigidbodyComponent>().loadFromXML(l_node); }
            else if (l_type == "Collider")          { addComponent<ColliderComponent>().loadFromXML(l_node); }
            else if (l_type == "Animation")         { addComponent<AnimationComponent>().loadFromXML(l_node); }
            else if (l_type == "Text")              { addComponent<TextComponent>().loadFromXML(l_node); }
            else if (l_type == "Sprite")            { addComponent<SpriteComponent>().loadFromXML(l_node); }
            else if (l_type == "Tilemap")           { addComponent<TilemapComponent>().loadFromXML(l_node); }
            else if (l_type == "Emitter")           { addComponent<EmitterComponent>().loadFromXML(l_node); }
            else if (l_type == "Script")            { addComponent<ScriptComponent>(mp_lua).loadFromXML(l_node); }
            else if (l_type == "Timer")             { addComponent<TimerComponent>().loadFromXML(l_node); }
            else if (l_type == "Widget")            { addComponent<WidgetComponent>().loadFromXML(l_node); }
            else if (l_type == "Sound")             { addComponent<SoundComponent>().loadFromXML(l_node); }
        }

        // Next, load the entity's children.
        // Iterate through each child node named "Entity".
        for (const xml_node& l_node : a_node.children("Entity"))
        {
            // Get its name.
            std::string l_name = l_node.attribute("Name").as_string();

            // If a name was provided, load it and attach it.
            if (!l_name.empty())
            {
                Entity& l_child = mp_scene->addEntity(l_name);

                l_child.setParentEntity(getFullName());
                l_child.loadFromXML(l_node);
            }
        }

        if (l_source.empty()) { callLuaFunction("onInitialize"); }
    }

    Entity &Entity::getEntityInScene(const std::string &a_path)
    {
        return mp_scene->getEntity(a_path);
    }

    bool Entity::hasChildEntity(const std::string &a_name)
    {
        TransformComponent* lp_transform = mp_transform->getChild(a_name);

        return (lp_transform != nullptr);
    }

    Entity &Entity::getChildEntity(const std::string &a_name)
    {
        TransformComponent* lp_transform = mp_transform->getChild(a_name);

        if (lp_transform == nullptr)
        {
            PropertyManager& PM = PropertyManager::getInstance();
            std::string l_fullName = getFullName();

            PM.append("errorParentName", false).set(l_fullName);
            PM.append("errorChildName", false).set(a_name);
            PM.append("errorParentID", false).setAs<unsigned int>(m_uid);

            throw std::runtime_error { "[Entity::getChildEntity] '" + a_name + "': Child entity not found in entity '" + l_fullName + "'!" };
        }

        return *lp_transform->getOwningEntity();
    }

    void Entity::detachChildEntity(const std::string &a_name)
    {
        mp_transform->detachChild(a_name);
    }

    void Entity::detachAllChildren()
    {
        mp_transform->detachAllChildren();
    }

    void Entity::setParentEntity(const std::string &a_path)
    {
        mp_transform->setParent(a_path);
    }

    void Entity::detachOnKill()
    {
        mp_transform->detachAllChildren(true);
    }

    std::string Entity::getFullName() const
    {
        // If this entity is not the root entity of a hierarchy, then it will have a string
        // containing the path down that hierarchy to this entity.
        //
        // The path to a certain entity in a hierarchy is a period-separated string containing
        // the names of each entity in the hierarchy, for instance:
        //
        //  "rootEntity.childEntity.grandChildEntity"
        //  |     Entity path      |  Entity name   |
        if (!m_path.empty())
        {
            std::string l_return = m_path + "." + m_name;
            return l_return;
        }

        // If the entity is the root entity of a hierarchy, its 'path' string will be blank,
        // so just return the entity's self name.
        return m_name;
    }

}
