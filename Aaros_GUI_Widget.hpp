// File: Aaros_GUI_Widget.hpp
//
// This is the base class for our graphical user interface objects.

#pragma once

#include <vector>
#include <algorithm>
#include <memory>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>
#include "Aaros_AssetManager.hpp"
#include "Aaros_Keyboard.hpp"
#include "Aaros_Mouse.hpp"
#include "Aaros_Utils_GUI.hpp"
#include "Aaros_Utils_XmlLoadable.hpp"
#include "Aaros_Utils_String.hpp"
#include "Aaros_Utils_Math.hpp"
#include "Aaros_Utils_Misc.hpp"

namespace Aaros
{

    /** Forward-declare the Entity class. */
    class Entity;

    namespace GUI
    {

        /**
         * This is the base class for our engine's graphical user interface objects.
         * GUI objects will be hosted inside of a component that can be attached to an
         * entity.
         */
        class Widget : public Utils::XmlLoadable
        {
        public /* Typedefs */:
            using Ptr           = std::unique_ptr<Widget>;
            using Container     = std::vector<Ptr>;
            using Iterator      = Container::iterator;

        protected /* Members */:
            Entity*             mp_entity;              /** The entity that owns this widget host component. */
            std::string         m_name;                 /** The name of the widget. */
            sf::Vector2f        m_position;             /** The widget's position, relative to the top-left of the window. */
            bool                m_enabled;              /** Is the widget enabled? */
            bool                m_visible;              /** Is the widget visible? */
            bool                m_focusable;            /** Can this widget be focused upon? */
            bool                m_focused;              /** Is the widget in focus? */
            int                 m_focusIndex;           /** The focus index of the widget. */

        protected /* Methods */:
            /** This method updates the internals of the widget. */
            virtual void        updateWidgetInternals () {}

        protected /* Constructor */:
            Widget (Entity* ap_entity, const std::string& a_name) :
                mp_entity       { ap_entity },
                m_name          { a_name },
                m_position      { 0.0f, 0.0f },
                m_enabled       { true },
                m_visible       { true },
                m_focusable     { false },
                m_focused       { false },
                m_focusIndex    { 0 }
            {}

        public /* Virtual Destructor */:
            virtual ~Widget () {}

        public /* Methods */:
            /** This method watches the user's actions and updates the widget as appropriate. */
            virtual void        update (const sf::Time& a_deltaTime) { (void) a_deltaTime; }

            /** This method renders the widget. */
            virtual void        render (sf::RenderWindow& a_window) = 0;

        public /* Local Getter-Setters */:
            inline std::string  getName () const { return m_name; }
            inline float        getPositionX () const { return m_position.x; }
            inline float        getPositionY () const { return m_position.y; }
            inline bool         isEnabled () const { return m_enabled && m_visible; }
            inline bool         isVisible () const { return m_visible; }
            inline bool         isFocusable () const { return m_focusable && m_enabled && m_visible; }
            inline bool         isFocused () const { return m_focused; }
            inline int          getFocusIndex () const { if (!isFocusable()) { return -1; } return m_focusIndex; }

            inline void setPosition (const float a_x, const float a_y) { m_position = { a_x, a_y }; updateWidgetInternals(); }
            inline void setEnabled (const bool a_enabled) { m_enabled = a_enabled; deactivate(); }
            inline void setVisible (const bool a_visible) { m_visible = a_visible; deactivate(); }
            inline void setFocusIndex (const int a_index) { if (m_focusable == true && a_index > 0) { m_focusIndex = a_index; } }

        public /* Internal Getter-Setters */:
            inline sf::Vector2f getPosition () const { return m_position; }

            inline void         activate () { m_focused = true; }
            inline void         deactivate () { m_focused = false; }

        };

    }
}
