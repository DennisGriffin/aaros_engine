// File: Aaros_Lua_Mouse.cpp

#include <functional>
#include "Aaros_Lua_Mouse.hpp"
#include "Aaros_Mouse.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeMouse (sol::state& a_state)
        {
            sol::table l_buttons = a_state.create_table();
            auto l_expose = [&l_buttons] (const sf::Mouse::Button a_button, const std::string& a_index)
            {
                l_buttons.set(a_index, static_cast<int>(a_button));
            };

            l_expose(sf::Mouse::Left,       "Left");
            l_expose(sf::Mouse::Right,      "Right");
            l_expose(sf::Mouse::Middle,     "Middle");
            l_expose(sf::Mouse::XButton1,   "ExtraOne");
            l_expose(sf::Mouse::XButton2,   "ExtraTwo");

            a_state["MouseButton"] = l_buttons;

            sol::constructors<> l_ctors;
            sol::userdata<Mouse> l_userdata
            {
                "Mouse", l_ctors,

                "isButtonPressed",      &Mouse::isButtonPressed,
                "isButtonDown",         &Mouse::isButtonDown,
                "getPositionX",         &Mouse::getPositionX,
                "getPositionY",         &Mouse::getPositionY,
                "getCoordX",            &Mouse::getCoordX,
                "getCoordY",            &Mouse::getCoordY,
                "getMeterPositionX",    &Mouse::getMeterPositionX,
                "getMeterPositionY",    &Mouse::getMeterPositionY,
                "getMeterCoordX",       &Mouse::getMeterCoordX,
                "getMeterCoordY",       &Mouse::getMeterCoordY,
                "getWheelDelta",        &Mouse::getWheelDelta
            };
            a_state.set_userdata(l_userdata)
                    .get<sol::table>("Mouse")
                    .set_function("getInstance", Mouse::getInstance);
        }

    }
}

