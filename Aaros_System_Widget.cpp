// File: Aaros_System_Widget.cpp

#include "Aaros_System_Widget.hpp"

namespace Aaros
{

    bool WidgetSystem::isEntityValid (Entity *ap_entity)
    {
        if (ap_entity->isEnabled() == false) { return false; }

        bool l_hasWidget = ap_entity->hasComponent<WidgetComponent>();
        if (l_hasWidget == true)
        {
            if (ap_entity->getComponent<WidgetComponent>().isEnabled() == true)
                return true;
        }

        return false;
    }

    void WidgetSystem::update (const Entity::Container &a_entities, const sf::Time &a_deltaTime, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            WidgetComponent& l_widget = l_ptr->getComponent<WidgetComponent>();
            l_widget.update(a_deltaTime);
        }
    }

    void WidgetSystem::render (const Entity::Container &a_entities, sf::RenderWindow &a_window, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            WidgetComponent& l_widget = l_ptr->getComponent<WidgetComponent>();
            l_widget.render(a_window);
        }
    }

}
