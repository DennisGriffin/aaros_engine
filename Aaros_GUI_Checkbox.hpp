// File: Aaros_GUI_Checkbox.hpp
//
// Presents a toggleable checkbox onscreen.

#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include "Aaros_GUI_Widget.hpp"

namespace Aaros
{
    namespace Constants
    {

        const sf::Vector2f  G_CHECKBOX_SIZE                     = { 16.0f, 16.0f };
        const sf::Color     G_CHECKBOX_FILL_COLOR               = { 255, 255, 255 };
        const sf::Color     G_CHECKBOX_SELECTED_FILL_COLOR      = { 128, 128, 128 };
        const sf::Color     G_CHECKBOX_OUTLINE_COLOR            = { 33, 33, 33 };
        const sf::Color     G_CHECKBOX_SELECTED_OUTLINE_COLOR   = { 128, 128, 128 };
        const float         G_CHECKBOX_OUTLINE_SIZE             = 2.0f;

    }

    namespace GUI
    {

        class Checkbox : public Widget
        {
        private /* Members */:
            sf::RectangleShape  m_rectObject;
            sf::Color           m_rectFill;
            sf::Color           m_rectFillFocus;
            sf::Color           m_rectOutline;
            sf::Color           m_rectOutlineFocus;
            bool                m_selected;

        private /* Methods */:
            void                updateWidgetInternals () override;

        public /* Constructor and Destructor */:
            Checkbox (Entity* ap_entity, const std::string& a_name);
            ~Checkbox ();

        public /* Methods */:
            void                loadFromXML (const pugi::xml_node &a_node) override;
            void                update (const sf::Time &a_deltaTime) override;
            void                render (sf::RenderWindow &a_window) override;

        public /* Getter-Setters */:
            inline bool         isSelected () const { return m_selected; }

            inline void setFillColor (const int a_r, const int a_g, const int a_b) { m_rectFill = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusFillColor (const int a_r, const int a_g, const int a_b) { m_rectFillFocus = Utils::createColor(a_r, a_g, a_b); }
            inline void setOutlineColor (const int a_r, const int a_g, const int a_b) { m_rectOutline = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusOutlineColor (const int a_r, const int a_g, const int a_b) { m_rectOutlineFocus = Utils::createColor(a_r, a_g, a_b); }
        };

    }
}
