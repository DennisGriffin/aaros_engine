// File: Aaros_Component_Widget.hpp
//
// This entity component allows the owning entity to host GUI objects on screen.

#pragma once

#include <typeinfo>
#include <type_traits>
#include <stdexcept>
#include <SFML/Graphics/RenderWindow.hpp>
#include "Aaros_PropertyManager.hpp"
#include "Aaros_GUI_Widget.hpp"
#include "Aaros_Component.hpp"

namespace Aaros
{

    class WidgetComponent : public Component
    {
    private /* Members */:
        GUI::Widget::Container  m_widgets;              /** The list of loaded GUI widgets. */
        unsigned int            m_focusIndex;           /** The index of the currently focused widget. */
        bool                    m_startFocus;           /** Have we started focusing on widgets, yet? */

    private /* Search and Sort Methods */:
        GUI::Widget::Iterator   findWidgetByName (const std::string& a_name);
        void                    sortWidgets ();

    public /* Constructor and Destructor */:
        WidgetComponent (Entity* ap_entity,
                         const std::string& a_entityName,
                         const unsigned int a_entityID);
        ~WidgetComponent ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Templated Methods */:
        template <typename T>
        T&                      addWidget (const std::string& a_name, const unsigned int a_order = 0);

        template <typename T>
        T&                      getWidget (const std::string& a_name, const bool a_resetFocus = false);

        void                    removeWidget (const std::string& a_name);

    public /* Local Methods */:
        void                    update (const sf::Time& a_deltaTime);
        void                    render (sf::RenderWindow& a_window);
        void                    focusPrevWidget ();
        void                    focusNextWidget ();
        void                    resetFocus ();

    };

    template <typename T>
    inline T& WidgetComponent::addWidget (const std::string& a_name, const unsigned int a_order)
    {
        static_assert(std::is_base_of<GUI::Widget, T>::value,
                      "[WidgetComponent::addWidget] Type provided must inherit from GUI::Widget.");

        if (a_name.empty())
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorWidgetType", false).set(typeid(T).name());
            PM.append("errorEntityName", false).set(m_entityName);
            PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

            throw std::invalid_argument {
                "[WidgetComponent::addWidget] No name specified for widget in entity '" + m_entityName + "'!"
            };
        }

        auto l_find = findWidgetByName(a_name);

        if (l_find != m_widgets.end())
        {
            if (typeid(**l_find) == typeid(T))
            {
                T* lp_downcast = dynamic_cast<T*>((*l_find).get());
                return *lp_downcast;
            }
            else
            {
                PropertyManager& PM = PropertyManager::getInstance();

                PM.append("errorWidgetName", false).set(a_name);
                PM.append("errorWidgetTypeRequested", false).set(typeid(T).name());
                PM.append("errorWidgetTypeFound", false).set(typeid(**l_find).name());
                PM.append("errorEntityName", false).set(m_entityName);
                PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

                throw std::runtime_error {
                    "[WidgetComponent::addWidget] A widget type mismatch occured in entity '" + m_entityName + "'!"
                };
            }
        }

        T* lp_widget = new T(mp_entity, a_name);
        lp_widget->setFocusIndex(a_order);

        std::unique_ptr<T> l_smart { lp_widget };
        m_widgets.push_back(std::move(l_smart));

        resetFocus();

        return *lp_widget;
    }

    template <typename T>
    inline T& WidgetComponent::getWidget (const std::string& a_name, const bool a_resetFocus)
    {
        static_assert(std::is_base_of<GUI::Widget, T>::value,
                      "[WidgetComponent::getWidget] Type provided must inherit from GUI::Widget.");

        if (a_name.empty())
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorWidgetType", false).set(typeid(T).name());
            PM.append("errorEntityName", false).set(m_entityName);
            PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

            throw std::invalid_argument {
                "[WidgetComponent::getWidget] No name specified for widget in entity '" + m_entityName + "'!"
            };
        }

        auto l_find = findWidgetByName(a_name);

        if (l_find == m_widgets.end())
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorWidgetName", false).set(a_name);
            PM.append("errorWidgetType", false).set(typeid(T).name());
            PM.append("errorEntityName", false).set(m_entityName);
            PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

            throw std::runtime_error {
                "[WidgetComponent::getWidget] Widget '" + a_name + "' not found in entity '" + m_entityName + "'!"
            };
        }

        if (typeid(**l_find) != typeid(T))
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorWidgetName", false).set(a_name);
            PM.append("errorWidgetTypeRequested", false).set(typeid(T).name());
            PM.append("errorWidgetTypeFound", false).set(typeid(**l_find).name());
            PM.append("errorEntityName", false).set(m_entityName);
            PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

            throw std::runtime_error {
                "[WidgetComponent::getWidget] A widget type mismatch occured in entity '" + m_entityName + "'!"
            };
        }

        if (a_resetFocus == true) { resetFocus(); }

        T* lp_downcast = dynamic_cast<T*>((*l_find).get());
        return *lp_downcast;
    }

    inline void WidgetComponent::removeWidget(const std::string &a_name)
    {
        auto l_find = findWidgetByName(a_name);

        if (l_find != m_widgets.end())
        {
            l_find = m_widgets.erase(l_find);
            resetFocus();
        }
    }

}
