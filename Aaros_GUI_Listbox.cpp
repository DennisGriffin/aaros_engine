// File: Aaros_GUI_Listbox.cpp

#include "Aaros_GUI_Listbox.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace GUI
    {

        void Listbox::updateButtons (const sf::Time &a_deltaTime)
        {
            // Update the scrollbar.
            if (m_scrollbar.isVisible())
            {
                m_scrollbar.update(a_deltaTime);
                m_firstChoiceIndex = m_scrollbar.getValue();
            }
            else
            {
                m_firstChoiceIndex = 0;
            }

            // Update the listbox buttons.
            for (unsigned int l_count = 0; l_count < m_buttonCount; ++l_count)
            {
                Button& l_button = m_buttons.at(l_count);
                unsigned int l_choiceIndex = m_firstChoiceIndex + l_count;

                if (l_choiceIndex < m_choices.size())
                {
                    ListboxChoice& l_choice = m_choices.at(l_choiceIndex);

                    l_button.setVisible(true);
                    l_button.setText(l_choice.m_name);

                    l_button.update(a_deltaTime);

                    if (l_choiceIndex == m_selectedChoiceIndex)
                    {
                        l_button.setFillColor(m_buttonFillFocus.r, m_buttonFillFocus.g, m_buttonFillFocus.b);
                        l_button.setTextColor(m_buttonTextFocus.r, m_buttonTextFocus.g, m_buttonTextFocus.b);
                    }
                    else
                    {
                        l_button.setFillColor(m_buttonFillColor.r, m_buttonFillColor.g, m_buttonFillColor.b);
                        l_button.setTextColor(m_buttonTextColor.r, m_buttonTextColor.g, m_buttonTextColor.b);
                    }

                    if (l_button.isSelected())
                    {
                        m_selectedChoiceIndex = l_choiceIndex;
                        m_selectedChoiceID = l_choice.m_id;

                        mp_entity->callLuaFunction(m_name + "_onChoiceSelected", m_selectedChoiceIndex, m_selectedChoiceID);
                    }
                }
                else
                {
                    l_button.setVisible(false);
                    l_button.setText("");
                }
            }
        }

        void Listbox::onButtonWidthChanged ()
        {
            // Button widgets in a listbox have a minimum width restriction.
            // Check to see if the width given complies.
            if (m_buttonWidth < Constants::G_LISTBOX_BUTTON_WIDTH) { m_buttonWidth = Constants::G_LISTBOX_BUTTON_WIDTH; }

            // Based on the width given, position the scrollbar.
            m_scrollbar.setPosition(m_position.x + m_buttonWidth + 4.0f, m_position.y);

            // Configure the size of the buttons.
            for (auto& l_button : m_buttons)
            {
                l_button.setSize(m_buttonWidth, Constants::G_LISTBOX_BUTTON_HEIGHT);
            }
        }

        void Listbox::onButtonCountChanged ()
        {
            // The minimum number of buttons I wish the listbox to display is 3.
            if (m_buttonCount < Constants::G_LISTBOX_BUTTON_COUNT) { m_buttonCount = Constants::G_LISTBOX_BUTTON_COUNT; }

            // Based on the number of buttons shown, and given a little spacing, develop
            // a size for the scrollbar.
            float l_scrollbarHeight =
                    static_cast<float>(m_buttonCount) * (Constants::G_LISTBOX_BUTTON_HEIGHT);

            m_scrollbar.setLongSize(l_scrollbarHeight + (static_cast<float>(m_buttonCount - 1) * 4.0f));
            m_scrollbar.setOrientation(ScrollbarOrientation::Vertical);

            // Resize the button container.
            m_buttons.resize(m_buttonCount, { nullptr, "" });

            // Position the buttons.
            for (unsigned int l_count = 0; l_count < m_buttonCount; ++l_count)
            {
                Button& l_button = m_buttons.at(l_count);

                l_button.setPosition(m_position.x, m_position.y + ((Constants::G_LISTBOX_BUTTON_HEIGHT + 4.0f) * static_cast<float>(l_count)));
                l_button.setSize(m_buttonWidth, Constants::G_LISTBOX_BUTTON_HEIGHT);
                l_button.setFont(m_buttonTextFont);
                l_button.setFontSize(Constants::G_LISTBOX_TEXT_FONT_SIZE);
                l_button.setFillColor(m_buttonFillColor.r, m_buttonFillColor.g, m_buttonFillColor.b);
                l_button.setFocusFillColor(m_buttonFillFocus.r, m_buttonFillFocus.g, m_buttonFillFocus.b);
                l_button.setOutlineColor(m_buttonOutlineColor.r, m_buttonOutlineColor.g, m_buttonOutlineColor.b);
                l_button.setFocusOutlineColor(m_buttonOutlineColor.r, m_buttonOutlineColor.g, m_buttonOutlineColor.b);
                l_button.setTextColor(m_buttonTextColor.r, m_buttonTextColor.g, m_buttonTextColor.b);
                l_button.setFocusTextColor(m_buttonTextFocus.r, m_buttonTextFocus.g, m_buttonTextFocus.b);
            }
        }

        void Listbox::onChoiceCountChanged()
        {
            if (m_choices.size() > m_buttonCount)
            {
                m_scrollbar.setVisible(true);
                m_scrollbar.setMaximum(m_choices.size() - m_buttonCount);
            }
            else
            {
                m_scrollbar.setVisible(false);
            }

            m_selectedChoiceID.clear();
            m_selectedChoiceIndex = 0;
        }

        Listbox::Listbox (Entity *ap_entity, const std::string &a_name) :
            Widget                  { ap_entity, a_name },
            m_buttonCount           { Constants::G_LISTBOX_BUTTON_COUNT },
            m_buttonWidth           { Constants::G_LISTBOX_BUTTON_WIDTH },
            m_buttonFillColor       { Constants::G_LISTBOX_BUTTON_COLOR },
            m_buttonFillFocus       { Constants::G_LISTBOX_BUTTON_SELECTED_COLOR },
            m_buttonOutlineColor    { Constants::G_LISTBOX_BUTTON_OUTLINE_COLOR },
            m_buttonTextColor       { Constants::G_LISTBOX_TEXT_COLOR },
            m_buttonTextFocus       { Constants::G_LISTBOX_TEXT_SELECTED_COLOR },
            m_buttonTextFont        { "" },
            m_firstChoiceIndex      { 0 },
            m_selectedChoiceIndex   { 0 },
            m_selectedChoiceID      { "" }
        {
            onButtonCountChanged();
            onButtonWidthChanged();
            onChoiceCountChanged();

            m_focusable = false;
        }

        Listbox::~Listbox ()
        {
            m_buttonCount = 0;
            m_buttonWidth = 0.0f;
            m_buttons.clear();
            m_firstChoiceIndex = 0;
            m_selectedChoiceIndex = 0;
            m_selectedChoiceID.clear();
            m_choices.clear();

            m_enabled = false;
            m_visible = false;
            m_focused = false;
            m_focusable = false;
            m_name.clear();
            m_position = { 0.0f, 0.0f };
            mp_entity = nullptr;
        }

        void Listbox::loadFromXML (const pugi::xml_node &a_node)
        {
            using namespace pugi;

            xml_node l_positionNode = a_node.child("Position");
            xml_node l_sizeNode = a_node.child("Size");
            xml_node l_fontNode = a_node.child("Font");
            xml_node l_colorNode = a_node.child("Color");
            xml_node l_choiceNode = a_node.child("Choices");

            float l_x = l_positionNode.attribute("X").as_float();
            float l_y = l_positionNode.attribute("Y").as_float();

            float l_width = l_sizeNode.attribute("ButtonWidth").as_float(Constants::G_LISTBOX_BUTTON_WIDTH);
            unsigned int l_count = l_sizeNode.attribute("Buttons").as_uint(Constants::G_LISTBOX_BUTTON_COUNT);

            std::string l_fontFile = l_fontNode.attribute("File").as_string();

            int l_fillRed = l_colorNode.child("Fill").attribute("Red").as_int(Constants::G_LISTBOX_BUTTON_COLOR.r);
            int l_fillGreen = l_colorNode.child("Fill").attribute("Green").as_int(Constants::G_LISTBOX_BUTTON_COLOR.g);
            int l_fillBlue = l_colorNode.child("Fill").attribute("Blue").as_int(Constants::G_LISTBOX_BUTTON_COLOR.b);
            int l_outlineRed = l_colorNode.child("Outline").attribute("Red").as_int(Constants::G_LISTBOX_BUTTON_OUTLINE_COLOR.r);
            int l_outlineGreen = l_colorNode.child("Outline").attribute("Green").as_int(Constants::G_LISTBOX_BUTTON_OUTLINE_COLOR.g);
            int l_outlineBlue = l_colorNode.child("Outline").attribute("Blue").as_int(Constants::G_LISTBOX_BUTTON_OUTLINE_COLOR.b);
            int l_textRed = l_colorNode.child("Text").attribute("Red").as_int(Constants::G_LISTBOX_TEXT_COLOR.r);
            int l_textGreen = l_colorNode.child("Text").attribute("Green").as_int(Constants::G_LISTBOX_TEXT_COLOR.g);
            int l_textBlue = l_colorNode.child("Text").attribute("Blue").as_int(Constants::G_LISTBOX_TEXT_COLOR.b);
            int l_fillRedFocus = l_colorNode.child("FocusFill").attribute("Red").as_int(Constants::G_LISTBOX_BUTTON_SELECTED_COLOR.r);
            int l_fillGreenFocus = l_colorNode.child("FocusFill").attribute("Green").as_int(Constants::G_LISTBOX_BUTTON_SELECTED_COLOR.g);
            int l_fillBlueFocus = l_colorNode.child("FocusFill").attribute("Blue").as_int(Constants::G_LISTBOX_BUTTON_SELECTED_COLOR.b);
            int l_textRedFocus = l_colorNode.child("FocusText").attribute("Red").as_int(Constants::G_LISTBOX_TEXT_SELECTED_COLOR.r);
            int l_textGreenFocus = l_colorNode.child("FocusText").attribute("Green").as_int(Constants::G_LISTBOX_TEXT_SELECTED_COLOR.g);
            int l_textBlueFocus = l_colorNode.child("FocusText").attribute("Blue").as_int(Constants::G_LISTBOX_TEXT_SELECTED_COLOR.b);

            setPosition(l_x, l_y);
            setButtonWidth(l_width);
            setButtonCount(l_count);
            setButtonFontFile(l_fontFile);
            setFillColor(l_fillRed, l_fillGreen, l_fillBlue);
            setOutlineColor(l_outlineRed, l_outlineGreen, l_outlineBlue);
            setTextColor(l_textRed, l_textGreen, l_textBlue);
            setFocusFillColor(l_fillRedFocus, l_fillGreenFocus, l_fillBlueFocus);
            setFocusTextColor(l_textRedFocus, l_textGreenFocus, l_textBlueFocus);

            for (const xml_node& l_node : l_choiceNode.children("Choice"))
            {
                std::string l_key = l_node.attribute("Key").as_string();
                std::string l_value = l_node.attribute("Value").as_string();

                if (l_key.empty() == false && l_key.find(' ') == std::string::npos)
                {
                    addChoice(l_key, l_value);
                }
            }

            onButtonCountChanged();
            onButtonWidthChanged();
        }

        void Listbox::update (const sf::Time &a_deltaTime)
        {
            updateButtons(a_deltaTime);
        }

        void Listbox::render (sf::RenderWindow &a_window)
        {
            for (Button& l_button : m_buttons)
            {
                if (l_button.isVisible())
                {
                    l_button.render(a_window);
                }
            }

            if (m_scrollbar.isVisible())
            {
                m_scrollbar.render(a_window);
            }
        }

        void Listbox::addChoice(const std::string &a_id, const std::string &a_name)
        {
            m_choices.emplace_back(a_id, a_name);

            onChoiceCountChanged();
        }

        void Listbox::removeChoice(const unsigned int a_index)
        {
            if (a_index < m_choices.size())
            {
                m_choices.erase(m_choices.begin() + a_index);
                onChoiceCountChanged();
            }
        }

        void Listbox::popChoice()
        {
            if (m_choices.empty() == false)
            {
                m_choices.pop_back();
                onChoiceCountChanged();
            }
        }

        void Listbox::clearChoices()
        {
            m_choices.clear();
            onChoiceCountChanged();
        }

    }
}
