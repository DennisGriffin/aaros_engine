// File: Aaros_Component_Script.hpp
//
// Allows Lua to manipulate the behavior of the owning entity.

#pragma once

#include <sol.hpp>
#include "Aaros_Component.hpp"
#include "Aaros_Utils_ScriptModule.hpp"

namespace Aaros
{

    /**
     * This entity component allows the behavior of its owning entity and components,
     * as well as that of other entities and their components, to be manipulated by
     * Lua script functions.
     */
    class ScriptComponent : public Component
    {
    private /* Members */:
        ScriptModule::Container     m_modules;      /** The list of stored script modules. */
        sol::state*                 mp_lua;         /** A pointer to the application's Lua state */

    private /* Search Methods */:
        ScriptModule::Iterator      findModuleByFilename (const std::string& a_filename);

    public /* Constructor and Destructor */:
        ScriptComponent (Entity* ap_entity,
                         const std::string& a_entityName,
                         const unsigned int a_entityID,
                         sol::state* ap_lua);
        ~ScriptComponent ();

    public /* Inherited Methods */:
        void loadFromXML (const pugi::xml_node &a_node) override;

    public /* Local Methods */:
        /**
         * Adds a new scripting module to the component, then returns it.
         *
         * If a module with the same name already exists within the component, that module
         * is returned instead.
         *
         * @param   a_file          The module file.
         * @param   a_enabled       Should the scripting module be run?
         *
         * @return  The newly-created, or already-existing, module.
         */
        ScriptModule&           addModule (const std::string& a_file,
                                           const bool a_enabled = true);

        /**
         * Gets the scripting module with the given filename.
         *
         * @param   a_file          The name of the module file.
         *
         * @return  The scripting module requested.
         */
        ScriptModule&           getModule (const std::string& a_file);

    public /* Inlined Methods */:
        /**
         * Calls the registered Lua function with the given name and arguments in all loaded
         * scripting modules.
         *
         * @param   a_name          The name of the function.
         * @param   apk_arguments   The parameter pack containing the function's arguments.
         */
        template <typename... Args>
        inline void             callFunction (const std::string& a_name, Args&&... apk_arguments)
        {
            if (isEnabled() == false) { return; }

            for (ScriptModule& a_module : m_modules)
            {
                if (a_module.isEnabled() == false) { continue; }
                if (a_module.isFunctionEnabled(a_name) == false) { continue; }

                try
                {
                    mp_lua->open_file(a_module.getScriptFilename());
                    auto l_function = mp_lua->get<sol::function>(a_name);

                    l_function(mp_entity, a_module, std::forward<Args>(apk_arguments)...);
                }
                catch (std::exception& l_ex)
                {
                    PropertyManager& PM = PropertyManager::getInstance();

                    PM.append("errorModuleFilename", false).set(a_module.getXmlFilename());
                    PM.append("errorModuleScript", false).set(a_module.getScriptFilename());
                    PM.append("errorFunctionName", false).set(a_name);
                    PM.append("errorEntityName", false).set(m_entityName);
                    PM.append("errorEntityID", false).setAs<unsigned int>(m_entityID);

                    throw;
                }
            }
        }

    };

}
