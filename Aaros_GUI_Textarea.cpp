// File: Aaros_GUI_Textarea.cpp

#include "Aaros_GUI_Textarea.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace GUI
    {

        void Textarea::updateWidgetInternals()
        {
            FontManager& FM = FontManager::getInstance();
            float l_thickness = Constants::G_TEXTAREA_OUTLINE_THICKNESS;

            m_rectObject.setPosition(m_position);
            m_rectObject.setSize(m_rectSize);

            m_textObject.setCharacterSize(m_textFontSize);
            m_textObject.setPosition(m_position.x + l_thickness,
                                     m_position.y + l_thickness);
            m_textWrapSize.x = m_rectSize.x - (l_thickness * 2.0f);
            m_textWrapSize.y = m_rectSize.y - (l_thickness * 2.0f);

            m_textWrapped = wrapText(m_text, m_textWrapSize, FM.get(m_textFontID), m_textFontSize);

            if (isTypewriterActive() == true)
            {
                m_textObject.setString(m_typewriterText);
            }
            else
            {
                m_textObject.setString(m_textWrapped);
            }
        }

        void Textarea::updateTypewriterEffect(const sf::Time &a_deltaTime)
        {
            if (m_typewriterActive == false || m_typewriterFinished == true)
            {
                m_typewriterTime = sf::Time::Zero;
                return;
            }

            m_typewriterTime += a_deltaTime;

            while (m_typewriterTime >= m_typewriterInterval && m_typewriterFinished == false)
            {
                m_typewriterTime -= m_typewriterInterval;

                m_typewriterText += m_textWrapped.at(m_typewriterPosition);
                m_typewriterPosition++;

                if (m_typewriterPosition == m_textWrapped.size())
                {
                    m_typewriterFinished = true;
                    mp_entity->callLuaFunction(m_name + "_onTypewriterFinished");
                }
            }
        }

        void Textarea::resetTypewriterEffect()
        {
            m_typewriterText.clear();
            m_typewriterTime = sf::Time::Zero;
            m_typewriterPosition = 0;
            m_typewriterFinished = false;
        }

        Textarea::Textarea (Entity *ap_entity, const std::string &a_name) :
            Widget                  { ap_entity, a_name },
            m_text                  { "" },
            m_textWrapped           { "" },
            m_textFontID            { "globalFont" },
            m_textFontSize          { Constants::G_TEXTAREA_FONT_SIZE },
            m_textWrapSize          { 0.0f, 0.0f },
            m_rectVisible           { false },
            m_rectSize              { Constants::G_TEXTAREA_SIZE },
            m_typewriterActive      { false },
            m_typewriterFinished    { false },
            m_typewriterText        { "" },
            m_typewriterPosition    { 0 },
            m_typewriterTime        { sf::Time::Zero },
            m_typewriterInterval    { Constants::G_TEXTAREA_TYPEWRITER_INTERVAL }
        {
            m_rectObject.setOutlineThickness(Constants::G_TEXTAREA_OUTLINE_THICKNESS);
            m_rectObject.setOutlineColor(Constants::G_TEXTAREA_RECT_OUTLINE_COLOR);
            m_rectObject.setFillColor(Constants::G_TEXTAREA_RECT_FILL_COLOR);
            m_textObject.setColor(Constants::G_TEXTAREA_TEXT_COLOR);

            setFont(m_textFontID);

            m_focusable = false;

            updateWidgetInternals();
        }

        Textarea::~Textarea()
        {
            m_text.clear();
            m_textWrapped.clear();
            m_textFontID.clear();
            m_textFontSize = 0;
            m_textWrapSize = { 0.0f, 0.0f };
            m_rectVisible = false;
            m_rectSize = { 0.0f, 0.0f };
            m_typewriterActive = false;
            m_typewriterFinished = false;
            m_typewriterText.clear();
            m_typewriterPosition = 0;
            m_typewriterTime = sf::Time::Zero;
            m_typewriterInterval = sf::Time::Zero;

            m_enabled = false;
            m_visible = false;
            m_focused = false;
            m_focusable = false;
            m_name.clear();
            m_position = { 0.0f, 0.0f };
            mp_entity = nullptr;
        }

        void Textarea::loadFromXML(const pugi::xml_node &a_node)
        {
            using namespace pugi;

            xml_node l_positionNode = a_node.child("Position");
            xml_node l_sizeNode = a_node.child("Size");
            xml_node l_fontNode = a_node.child("Font");
            xml_node l_typewriterNode = a_node.child("Typewriter");
            xml_node l_textNode = a_node.child("Text");
            xml_node l_rectNode = a_node.child("Rect");
            xml_node l_colorNode = a_node.child("Color");

            float l_x = l_positionNode.attribute("X").as_float();
            float l_y = l_positionNode.attribute("Y").as_float();

            float l_w = l_sizeNode.attribute("Width").as_float(Constants::G_TEXTAREA_SIZE.x);
            float l_h = l_sizeNode.attribute("Height").as_float(Constants::G_TEXTAREA_SIZE.y);

            std::string l_fontID = l_fontNode.attribute("File").as_string("globalFont");
            unsigned int l_fontSize = l_fontNode.attribute("Size").as_uint(Constants::G_TEXTAREA_FONT_SIZE);

            bool l_typewriter = l_typewriterNode.attribute("Enabled").as_bool();
            float l_typewriterInterval = l_typewriterNode.attribute("Interval").as_float(Constants::G_TEXTAREA_TYPEWRITER_INTERVAL.asSeconds());

            std::string l_text = l_textNode.attribute("Value").as_string();

            bool l_rectVisible = l_rectNode.attribute("Visible").as_bool(false);

            int l_fillRed = l_colorNode.child("Fill").attribute("Red").as_int(Constants::G_TEXTAREA_RECT_FILL_COLOR.r);
            int l_fillGreen = l_colorNode.child("Fill").attribute("Green").as_int(Constants::G_TEXTAREA_RECT_FILL_COLOR.g);
            int l_fillBlue = l_colorNode.child("Fill").attribute("Blue").as_int(Constants::G_TEXTAREA_RECT_FILL_COLOR.b);
            int l_outlineRed = l_colorNode.child("Outline").attribute("Red").as_int(Constants::G_TEXTAREA_RECT_OUTLINE_COLOR.r);
            int l_outlineGreen = l_colorNode.child("Outline").attribute("Green").as_int(Constants::G_TEXTAREA_RECT_OUTLINE_COLOR.g);
            int l_outlineBlue = l_colorNode.child("Outline").attribute("Blue").as_int(Constants::G_TEXTAREA_RECT_OUTLINE_COLOR.b);
            int l_textRed = l_colorNode.child("Text").attribute("Red").as_int(Constants::G_TEXTAREA_TEXT_COLOR.r);
            int l_textGreen = l_colorNode.child("Text").attribute("Green").as_int(Constants::G_TEXTAREA_TEXT_COLOR.g);
            int l_textBlue = l_colorNode.child("Text").attribute("Blue").as_int(Constants::G_TEXTAREA_TEXT_COLOR.b);

            setPosition(l_x, l_y);
            setSize(l_w, l_h);
            setFont(l_fontID);
            setFontSize(l_fontSize);
            setTypewriterActive(l_typewriter);
            setTypewriterInterval(l_typewriterInterval);
            setText(l_text);
            setRectVisible(l_rectVisible);
            setFillColor(l_fillRed, l_fillGreen, l_fillBlue);
            setOutlineColor(l_outlineRed, l_outlineGreen, l_outlineBlue);
            setTextColor(l_textRed, l_textGreen, l_textBlue);

            updateWidgetInternals();
        }

        void Textarea::update(const sf::Time &a_deltaTime)
        {
            if (m_typewriterActive == true) { updateTypewriterEffect(a_deltaTime); }

            updateWidgetInternals();
        }

        void Textarea::render(sf::RenderWindow &a_window)
        {
            if (m_rectVisible == true) { a_window.draw(m_rectObject); }

            a_window.draw(m_textObject);
        }

    }
}
