// File: Aaros_Component_Transform.hpp
//
// Gives the owning entity a sense of position in the game world
// that can be derived from a parent.

#pragma once

#include <vector>
#include <algorithm>
#include <SFML/System/Vector2.hpp>
#include "Aaros_Component.hpp"
#include "Aaros_Utils_Math.hpp"
#include "Aaros_Utils_String.hpp"

namespace Aaros
{

    /**
     * This component gives the owning entity a sense of position in the game world, and is
     * also capable of managing a parent and child transforms, a feature which the ECS as a whole
     * will be able to take advantage of.
     *
     * This is a very important component to our ECS which will have a somewhat heavy coupling with
     * the entity class itself. As such, all entities in our engine will have this component attached upon
     * instantiation, and it cannot be removed by any normal means aside from unloading the entity altogether.
     */
    class TransformComponent : public Component
    {
    public /* Typedefs */:
        using Parent            = TransformComponent*;
        using Children          = std::vector<TransformComponent*>;
        using ChildIter         = Children::iterator;

    public /* Members */:
        /**
         * I am going to break from convention here and place this member variable in the
         * public scope. This will help me get this child-parent entity system working.
         */
        Parent                  mp_parent;

    private /* Members */:
        Children                m_children;             /** The transform's children. */
        bool                    m_entityAlive;          /** Is the owning entity 'alive'? */
        bool                    m_entityPersistent;     /** Is the owning entity 'persistent'? */
        bool                    m_ancestorDead;         /** Has an ancestor of the owning entity died? */
        bool                    m_ancestorPersistent;   /** Is an ancestor of the owning entity 'persistent'? */
        sf::Vector2f            m_originPoint;          /** A point of origin, based upon the parent's absolute position. */
        sf::Vector2f            m_relativePosition;     /** The position of the transform, relative to the origin point. */
        sf::Vector2f            m_absolutePosition;     /** The position of the transform in the game world. */
        sf::Vector2f            m_relativeCenter;       /** The transform's center point, relative to its absolute position. */
        sf::Vector2f            m_absoluteCenter;       /** The transform's center point in world space. */

    private /* Search Methods */:
        ChildIter               findChildByName (const std::string& a_name);

    private /* Setter Callbacks */:
        void                    onEnabledChanged () override;
        void                    onAncestorDisabledChanged () override;
        void                    onAliveChanged ();
        void                    onAncestorDeadChanged ();
        void                    onPersistentChanged ();
        void                    onAncestorPersistentChanged ();
        void                    onPositionChanged ();
        void                    onCenterChanged ();

    public /* Constructor and Destructor */:
        TransformComponent (Entity* ap_entity,
                            const std::string& a_entityName,
                            const unsigned int a_entityID);
        ~TransformComponent ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        bool                    hasChild (const std::string& a_name);
        TransformComponent*     getChild (const std::string& a_name);
        void                    detachChild (const std::string& a_name);
        void                    detachAllChildren (const bool a_killToo = false);
        void                    setParent (const std::string& a_path);
        void                    translate (const float a_x, const float a_y);
        void                    translateByVector (const sf::Vector2f& a_vector);

    public /* Internal Methods */:
        void                    attachChild (TransformComponent* ap_transform);
        void                    adjustHierarchyPath (const std::string &a_path);

    public /* Inline Getter-Setters */:
        inline bool             isRoot () const { return (mp_parent == nullptr); }
        inline sf::Vector2f     getPosition () const { return m_absolutePosition; }
        inline sf::Vector2f     getCenter () const { return m_absoluteCenter; }
        inline float            getPositionX () const { return m_absolutePosition.x; }
        inline float            getPositionY () const { return m_absolutePosition.y; }
        inline float            getCenterX () const { return m_absoluteCenter.x; }
        inline float            getCenterY () const { return m_absoluteCenter.y; }
        inline float            getPixelPositionX () const { return m_absolutePosition.x * Constants::G_METERS_TO_PIXELS_F; }
        inline float            getPixelPositionY () const { return m_absolutePosition.y * Constants::G_METERS_TO_PIXELS_F; }
        inline float            getPixelCenterX () const { return m_absoluteCenter.x * Constants::G_METERS_TO_PIXELS_F; }
        inline float            getPixelCenterY () const { return m_absoluteCenter.y * Constants::G_METERS_TO_PIXELS_F; }

        inline void setPosition (const float a_x, const float a_y) { m_relativePosition = { a_x, a_y }; onPositionChanged(); }
        inline void setCenter (const float a_x, const float a_y) { m_relativeCenter = { a_x, a_y }; onCenterChanged(); }

    public /* Internal Getter-Setters */:
        inline bool             isSelfAlive () const { return m_entityAlive; }
        inline bool             isSelfPersistent () const { return m_entityPersistent; }
        inline bool             isAncestorDead () const { return m_ancestorDead; }
        inline bool             isAncestorPersistent () const { return m_ancestorPersistent; }
        inline sf::Vector2f     getOriginPoint () const { return m_originPoint; }
        inline sf::Vector2f     getRelativePosition () const { return m_relativePosition; }
        inline sf::Vector2f     getPixelPosition () const { return m_absolutePosition * Constants::G_METERS_TO_PIXELS_F; }
        inline sf::Vector2f     getPixelCenter () const { return m_absoluteCenter * Constants::G_METERS_TO_PIXELS_F; }

        inline void setSelfAlive (const bool a_alive) { m_entityAlive = a_alive; onAliveChanged(); }
        inline void setSelfPersistent (const bool a_persistent) { m_entityPersistent = a_persistent; onPersistentChanged(); }
        inline void setAncestorDead (const bool a_dead) { m_ancestorDead = a_dead; onAncestorDeadChanged(); }
        inline void setAncestorPersistent (const bool a_persistent) { m_ancestorPersistent = a_persistent; onAncestorPersistentChanged(); }
        inline void setOriginPoint (const sf::Vector2f& a_point) { m_originPoint = a_point; onPositionChanged(); }
        inline void zeroOrigin () { m_originPoint = { 0.0f, 0.0f }; onPositionChanged(); }
        inline void setPositionVector (const sf::Vector2f& a_position) { m_relativePosition = a_position; onPositionChanged(); }

    };

    /**
     * The transform component is the most important component in our ECS. All
     * entities will have this component attached upon construction. As such, this
     * component is deemed vital.
     */
    template <>
    struct TT_Is_Component_Vital<TransformComponent>
    {
        static const bool Value = true;
    };

}
