// File: Aaros_RenderSystem.cpp

#include "Aaros_System_Render.hpp"

namespace Aaros
{

    bool RenderSystem::isEntityValid(Entity *ap_entity)
    {
        a_mode = RenderMode::None;

        if (ap_entity->isEnabled() == false) { return false; }

        bool l_hasText = ap_entity->hasComponent<TextComponent>();
        bool l_hasSprite = ap_entity->hasComponent<SpriteComponent>();
        bool l_hasTilemap = ap_entity->hasComponent<TilemapComponent>();
        bool l_hasEmitter = ap_entity->hasComponent<EmitterComponent>();

        if (l_hasText == true)
        {
            if (ap_entity->getComponent<TextComponent>().isEnabled())
                a_mode = RenderMode::Text;
        }
        else if (l_hasSprite == true)
        {
            if (ap_entity->getComponent<SpriteComponent>().isEnabled())
                a_mode = RenderMode::Sprite;
        }
        else if (l_hasTilemap == true)
        {
            if (ap_entity->getComponent<TilemapComponent>().isEnabled())
                a_mode = RenderMode::Tilemap;
        }
        else if (l_hasEmitter == true)
        {
            if (ap_entity->getComponent<EmitterComponent>().isEnabled())
                a_mode = RenderMode::Emitter;
        }

        if (a_mode == RenderMode::None) { return false; }

        return true;
    }

    void RenderSystem::update(const Entity::Container &a_entities, const sf::Time &a_deltaTime, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            TransformComponent& l_transform = l_ptr->getComponent<TransformComponent>();

            if (a_mode == RenderMode::Text)
            {
                TextComponent& l_text = l_ptr->getComponent<TextComponent>();
                sf::Text& l_obj = l_text.getTextObject();

                if (l_text.isCentered())
                {
                    l_obj.setPosition(l_transform.getPixelPositionX() - (l_obj.getGlobalBounds().width / 2.0f),
                                      l_transform.getPixelPositionY());
                }
                else
                {
                    l_obj.setPosition(l_transform.getPixelPosition());
                }
            }
            else if (a_mode == RenderMode::Emitter)
            {
                EmitterComponent& l_emitter = l_ptr->getComponent<EmitterComponent>();

                l_emitter.update(l_transform.getPosition(), a_deltaTime, m_cameraRect);
            }
        }
    }

    void RenderSystem::render(const Entity::Container &a_entities, sf::RenderWindow &a_window, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            TransformComponent& l_transform = *l_ptr->getTransform();

            if (a_mode == RenderMode::Sprite)
            {
                SpriteComponent& l_sprite = l_ptr->getComponent<SpriteComponent>();
                l_sprite.getSprite().setPosition(l_transform.getPixelPosition());

                if (l_sprite.getSprite().getGlobalBounds().intersects(m_cameraRect))
                {
                    sf::RenderStates l_states;
                    l_states.shader = l_sprite.getShader().getShaderPtr();
                    l_states.blendMode = sf::BlendAdd;

                    a_window.draw(l_sprite.getSprite(), l_states);
                }
            }
            else if (a_mode == RenderMode::Tilemap)
            {
                TilemapComponent& l_tilemap = l_ptr->getComponent<TilemapComponent>();

                l_tilemap.render(l_transform.getPosition(), a_window, m_cameraRect);
            }
            else if (a_mode == RenderMode::Emitter)
            {
                EmitterComponent& l_emitter = l_ptr->getComponent<EmitterComponent>();

                l_emitter.render(l_transform.getPosition(), a_window, m_cameraRect);
            }
            else if (a_mode == RenderMode::Text)
            {
                TextComponent& l_text = l_ptr->getComponent<TextComponent>();

                if (l_text.getTextObject().getGlobalBounds().intersects(m_cameraRect))
                {
                    a_window.draw(l_text.getTextObject());
                }
            }
        }
    }

}
