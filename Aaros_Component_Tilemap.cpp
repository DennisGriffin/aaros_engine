// File: Aaros_Component_Tilemap.cpp

#include "Aaros_Component_Tilemap.hpp"
#include "Aaros_Utils_Math.hpp"

namespace Aaros
{

    Tile::Iterator TilemapComponent::findTile(const int a_x, const int a_y)
    {
        return std::find_if(m_tiles.begin(), m_tiles.end(),
                            [&a_x, &a_y] (const Tile& a_tile)
        {
            return a_tile.m_position.x == a_x && a_tile.m_position.y == a_y;
        });
    }

    TilemapComponent::TilemapComponent(Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_shader                { &m_toRender }
    {

    }

    TilemapComponent::~TilemapComponent()
    {
        m_tiles.clear();

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void TilemapComponent::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_imageNode = a_node.child("Image");
        xml_node l_sizeNode = a_node.child("Size");

        std::string l_imageFile = l_imageNode.attribute("Filename").as_string();

        if (!l_imageFile.empty())
        {
            TextureManager& TM = TextureManager::getInstance();

            TM.load(l_imageFile);
            m_sprite.setTexture(TM.get(l_imageFile));
        }

        unsigned int l_width = l_sizeNode.attribute("Width").as_uint(4);
        unsigned int l_height = l_sizeNode.attribute("Height").as_uint(4);

        if (l_width < 4) { l_width = 4; }
        if (l_height < 4) { l_height = 4; }

        m_texture.create(l_width * static_cast<unsigned int>(Constants::G_DEFAULT_TILESIZE.x),
                         l_height * static_cast<unsigned int>(Constants::G_DEFAULT_TILESIZE.y));

        for (unsigned int w = 0; w < l_width; ++w)
            for (unsigned int h = 0; h < l_height; ++h)
                setTile(static_cast<int>(w), static_cast<int>(h), 0, 0);

        for (const xml_node& l_tileNode : a_node.children("Tile"))
        {
            float l_px = l_tileNode.attribute("PX").as_float();
            float l_py = l_tileNode.attribute("PY").as_float();
            int l_sx = l_tileNode.attribute("SX").as_int();
            int l_sy = l_tileNode.attribute("SY").as_int();

            setTile(l_px, l_py, l_sx, l_sy);
        }

        if (sf::Shader::isAvailable())
        {
            xml_node l_shaderNode = a_node.child("Shader");

            if (l_shaderNode.type() != node_null)
                m_shader.loadFromXML(l_shaderNode);
        }
    }

    void TilemapComponent::setTile(const int a_x, const int a_y, const int a_sx, const int a_sy)
    {
        if (static_cast<unsigned int>(a_x) >= m_texture.getSize().x ||
            static_cast<unsigned int>(a_y) >= m_texture.getSize().y)
        {
            return;
        }

        auto l_find = findTile(a_x, a_y);

        if (l_find != m_tiles.end())
        {
            l_find->m_sourceRect.left = a_x;
            l_find->m_sourceRect.top = a_y;
        }

        m_tiles.emplace_back(sf::Vector2i(a_x, a_y), sf::Vector2i(a_sx, a_sy));
    }

    void TilemapComponent::removeTile(const int a_x, const int a_y)
    {
        auto l_find = findTile(a_x, a_y);

        if (l_find != m_tiles.end())
        {
            l_find = m_tiles.erase(l_find);
        }
    }

    void TilemapComponent::render(const sf::Vector2f &a_transformPosition, sf::RenderWindow &a_window, const sf::FloatRect &a_cameraRect)
    {
        sf::Vector2f l_tilePos = { 0.0f, 0.0f };

        m_texture.clear();

        for (const Tile& l_tile : m_tiles)
        {
            l_tilePos.x = static_cast<float>(l_tile.m_position.x);
            l_tilePos.y = static_cast<float>(l_tile.m_position.y);

            m_sprite.setPosition(l_tilePos * Constants::G_METERS_TO_PIXELS_F);
            m_sprite.setTextureRect(l_tile.m_sourceRect);

            if (m_sprite.getGlobalBounds().intersects(a_cameraRect))
                m_texture.draw(m_sprite);
        }

        m_texture.display();

        m_toRender.setTexture(m_texture.getTexture());
        m_toRender.setPosition(a_transformPosition * Constants::G_METERS_TO_PIXELS_F);

        sf::RenderStates l_states;
        l_states.shader = m_shader.getShaderPtr();
        l_states.blendMode = sf::BlendAdd;

        a_window.draw(m_toRender, l_states);
    }

}
