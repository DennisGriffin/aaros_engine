// File: Aaros_Jukebox.hpp
//
// This class is responsible for playing sound and music files.

#pragma once

#include <cmath>
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Listener.hpp>
#include "Aaros_Utils_Singleton.hpp"

namespace Aaros
{

    class Jukebox : public Utils::Singleton<Jukebox>
    {
    private /* Friend Classes */:
        friend class Utils::Singleton<Jukebox>;

    private /* Members */:
        sf::Music               m_currentSong;
        bool                    m_musicLoaded = false;

    public /* Members */:
        void                    loadSong (const std::string& a_songFile);
        void                    playSong ();
        void                    pauseSong ();
        void                    stopSong ();

    public /* Getter-Setters */:
        inline bool             isSongLoaded () const { return m_musicLoaded; }
        inline bool             isLooping () const { return m_currentSong.getLoop(); }

        inline void setLooping (const bool a_looping) { m_currentSong.setLoop(a_looping); }

    };

}
