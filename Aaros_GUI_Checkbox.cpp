// File: Aaros_GUI_Checkbox.cpp

#include "Aaros_GUI_Checkbox.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace GUI
    {

        void Checkbox::updateWidgetInternals ()
        {
            m_rectObject.setPosition(m_position);
            m_rectObject.setSize(Constants::G_CHECKBOX_SIZE);
            m_rectObject.setOutlineThickness(Constants::G_CHECKBOX_OUTLINE_SIZE);

            if (m_selected == true)
            {
                m_rectObject.setFillColor(m_rectFillFocus);
            }
            else
            {
                m_rectObject.setFillColor(m_rectFill);
            }
        }

        Checkbox::Checkbox (Entity *ap_entity, const std::string &a_name) :
            Widget              { ap_entity, a_name },
            m_rectFill          { Constants::G_CHECKBOX_FILL_COLOR },
            m_rectFillFocus     { Constants::G_CHECKBOX_SELECTED_FILL_COLOR },
            m_rectOutline       { Constants::G_CHECKBOX_OUTLINE_COLOR },
            m_rectOutlineFocus  { Constants::G_CHECKBOX_SELECTED_OUTLINE_COLOR },
            m_selected          { false }
        {
            m_rectObject.setFillColor(m_rectFill);
            m_rectObject.setOutlineColor(m_rectOutline);
            m_focusable = true;

            updateWidgetInternals();
        }

        Checkbox::~Checkbox ()
        {
            m_selected = false;

            m_enabled = false;
            m_visible = false;
            m_focused = false;
            m_focusable = false;
            m_name.clear();
            m_position = { 0.0f, 0.0f };
            mp_entity = nullptr;
        }

        void Checkbox::loadFromXML (const pugi::xml_node &a_node)
        {
            using namespace pugi;

            xml_node l_positionNode = a_node.child("Position");
            xml_node l_checkboxNode = a_node.child("Checkbox");
            xml_node l_colorNode = a_node.child("Color");

            float l_x = l_positionNode.attribute("X").as_float();
            float l_y = l_positionNode.attribute("Y").as_float();

            bool l_selected = l_checkboxNode.attribute("Selected").as_bool();

            int l_fillRed = l_colorNode.child("Fill").attribute("Red").as_int(Constants::G_CHECKBOX_FILL_COLOR.r);
            int l_fillGreen = l_colorNode.child("Fill").attribute("Green").as_int(Constants::G_CHECKBOX_FILL_COLOR.g);
            int l_fillBlue = l_colorNode.child("Fill").attribute("Blue").as_int(Constants::G_CHECKBOX_FILL_COLOR.b);

            int l_fillFocusRed = l_colorNode.child("FocusFill").attribute("Red").as_int(Constants::G_CHECKBOX_SELECTED_FILL_COLOR.r);
            int l_fillFocusGreen = l_colorNode.child("FocusFill").attribute("Green").as_int(Constants::G_CHECKBOX_SELECTED_FILL_COLOR.g);
            int l_fillFocusBlue = l_colorNode.child("FocusFill").attribute("Blue").as_int(Constants::G_CHECKBOX_SELECTED_FILL_COLOR.b);

            int l_outlineRed = l_colorNode.child("Outline").attribute("Red").as_int(Constants::G_CHECKBOX_OUTLINE_COLOR.r);
            int l_outlineGreen = l_colorNode.child("Outline").attribute("Green").as_int(Constants::G_CHECKBOX_OUTLINE_COLOR.g);
            int l_outlineBlue = l_colorNode.child("Outline").attribute("Blue").as_int(Constants::G_CHECKBOX_OUTLINE_COLOR.b);

            int l_outlineFocusRed = l_colorNode.child("FocusOutline").attribute("Red").as_int(Constants::G_CHECKBOX_SELECTED_OUTLINE_COLOR.r);
            int l_outlineFocusGreen = l_colorNode.child("FocusOutline").attribute("Green").as_int(Constants::G_CHECKBOX_SELECTED_OUTLINE_COLOR.g);
            int l_outlineFocusBlue = l_colorNode.child("FocusOutline").attribute("Blue").as_int(Constants::G_CHECKBOX_SELECTED_OUTLINE_COLOR.b);

            m_position = { l_x, l_y };
            m_selected = l_selected;

            setFillColor(l_fillRed, l_fillGreen, l_fillBlue);
            setFocusFillColor(l_fillFocusRed, l_fillFocusGreen, l_fillFocusBlue);
            setOutlineColor(l_outlineRed, l_outlineGreen, l_outlineBlue);
            setFocusOutlineColor(l_outlineFocusRed, l_outlineFocusGreen, l_outlineFocusBlue);

            updateWidgetInternals();
        }

        void Checkbox::update (const sf::Time &a_deltaTime)
        {
            (void) a_deltaTime;

            Mouse& M = Mouse::getInstance();
            Keyboard& K = Keyboard::getInstance();
            bool l_hovering = m_rectObject.getGlobalBounds().contains(M.getPosition());
            bool l_selected = false;

            if (l_hovering || m_focused)
            {
                m_rectObject.setOutlineColor(m_rectOutlineFocus);

                if (l_hovering == true)
                {
                    l_selected = M.isButtonPressed(sf::Mouse::Left);
                }

                if (m_focused == true)
                {
                    l_selected = K.isKeyPressed(sf::Keyboard::Return);
                }

                if (l_selected == true)
                {
                    m_selected = !m_selected;
                    mp_entity->callLuaFunction(m_name + "_onCheckboxToggled", m_selected);
                }
            }
            else
            {
                m_rectObject.setOutlineColor(m_rectOutline);
            }

            updateWidgetInternals();
        }

        void Checkbox::render (sf::RenderWindow &a_window)
        {
            a_window.draw(m_rectObject);
        }

    }
}
