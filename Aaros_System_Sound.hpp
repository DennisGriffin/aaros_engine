// File: Aaros_System_Sound.hpp
//
// Updates the position of sound components.

#pragma once

#include "Aaros_System.hpp"

namespace Aaros
{

    class SoundSystem : public System
    {
    private /* Methods */:
        bool                    isEntityValid (Entity *ap_entity) override;

    public /* Methods */:
        void                    update (const Entity::Container &a_entities,
                                        const sf::Time &a_deltaTime,
                                        const unsigned int a_entityCount) override;
    };

}
