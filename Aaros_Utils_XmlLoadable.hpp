// File: Aaros_Utils_XmlLoadable.hpp
//
// Provides a helper class that allows class instances to be loaded
// from definitions found in XML files and nodes.

#pragma once

#include <iostream>
#include <string>
#include <pugixml.hpp>

namespace Aaros
{
    namespace Utils
    {

        /**
         * This helper class allows instances of inheriting classes to be loaded
         * from definitions found inside of an XML file or XML node.
         */
        class XmlLoadable
        {
        protected /* Members */:
            std::string         m_filename;

        public /* Outline Methods */:
            /**
             * This method accesses the given XML node in order to load definitions needed
             * by the inheriting class. Since different classes will require different definitions
             * and their own means of playing with them, this class will need to be overloaded.
             *
             * @param   a_node                  The XML node containing the relevant definitions.
             */
            virtual void        loadFromXML (const pugi::xml_node& a_node) = 0;

        public /* Inline Methods */:
            /**
             * This method loads an XML file containing the definitions required by the inheriting class.
             *
             * @param   a_filename              The XML file to be loaded.
             * @param   a_rootname              The name of the root node, if desired.
             *
             * @return  True if the file loads successfully and the root node is valid, false otherwise.
             */
            inline bool loadFromFile (const std::string& a_filename,
                                      const std::string& a_rootname = "")
            {
                using namespace pugi;

                // Load the XML file.
                xml_document l_document;
                xml_parse_result l_result = l_document.load_file(a_filename.c_str());

                // Verify that the file loaded successfully.
                if (l_result.status == status_ok)
                {
                    // Get the root node.
                    xml_node l_node;
                    if (a_rootname.empty()) { l_node = l_document.first_child(); }
                    else                    { l_node = l_document.child(a_rootname.c_str()); }

                    // Verify that the root node is valid.
                    if (l_node.type() != node_null)
                    {
                        // Load the root node.
                        m_filename = a_filename;
                        loadFromXML(l_node);
                    }
                    else
                    {
                        // Report the error before returning false.
                        if (a_rootname.empty())
                        {
                            std::cerr << "[XmlLoadable::loadFromFile] "
                                      << "'" << a_filename << "': Root node is invalid." << std::endl;
                        }
                        else
                        {
                            std::cerr << "[XmlLoadable::loadFromFile] "
                                      << "'" << a_filename << "': Root node '" << a_rootname << "' is missing or invalid." << std::endl;
                        }

                        return false;
                    }
                }
                else
                {
                    // Report the error to the user before returning false.
                    std::cerr << "[XmlLoadable::loadFromFile] "
                              << "'" << a_filename << "': " << l_result.description() << std::endl;

                    return false;
                }

                return true;
            }
        };

    }
}
