// File: Aaros_Keyboard.cpp

#include "Aaros_Keyboard.hpp"

namespace Aaros
{

    void Keyboard::pollEvent(sf::Event &a_event)
    {
        if (a_event.type == sf::Event::KeyPressed)
        {
            int l_code = a_event.key.code;
            m_pressed[l_code] = true;
            m_down[l_code] = true;
        }
        else if (a_event.type == sf::Event::KeyReleased)
        {
            int l_code = a_event.key.code;
            m_pressed[l_code] = false;
            m_down[l_code] = false;
        }
        else if (a_event.type == sf::Event::TextEntered)
        {
            m_unicode = static_cast<char>(a_event.text.unicode);
        }
    }

    bool Keyboard::isKeyPressed(const int a_index)
    {
        auto l_find = m_pressed.find(a_index);

        if (l_find != m_pressed.end())
        {
            bool l_return = l_find->second;
            l_find->second = false;
            return l_return;
        }

        return false;
    }

    bool Keyboard::isKeyDown(const int a_index)
    {
        auto l_find = m_down.find(a_index);

        if (l_find != m_down.end())
        {
            return l_find->second;
        }

        return false;
    }

    char Keyboard::getLastChar()
    {
        char l_unicode = m_unicode;
        m_unicode = 0;

        return l_unicode;
    }

    void Keyboard::resetKeyPress ()
    {
        for (auto& l_keypress : m_pressed)
        {
            l_keypress.second = false;
        }
    }

    void Keyboard::resetUnicode()
    {
        m_unicode = 0;
    }

}
