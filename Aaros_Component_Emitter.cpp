// File: Aaros_Component_Emitter.cpp

#include "Aaros_Component_Emitter.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    void EmitterComponent::onEnabledChanged ()
    {
        if (m_enabled == false)
        {
            m_particles.clear();
        }
    }

    void EmitterComponent::spawnParticle ()
    {
        if (m_particles.size() >= m_limit) { return; }

        Math::Random& R = Math::Random::getInstance();

        float l_vx = R.getNextFloat(m_velocityLower.x, m_velocityUpper.x);
        float l_vy = R.getNextFloat(m_velocityLower.y, m_velocityUpper.y);

        Particle l_particle { l_vx, l_vy, m_color };

        m_particles.push_back(l_particle);
    }

    EmitterComponent::EmitterComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_velocityLower         { Constants::G_PARTICLE_LOWERBOUND_VELOCITY },
        m_velocityUpper         { Constants::G_PARTICLE_UPPERBOUND_VELOCITY },
        m_lifetime              { Constants::G_PARTICLE_LIFETIME },
        m_spawntime             { Constants::G_PARTICLE_SPAWN_TIME },
        m_nextSpawn             { sf::Time::Zero },
        m_color                 { sf::Color::White },
        m_limit                 { Constants::G_PARTICLE_LIMIT },
        m_shader                { nullptr }
    {

    }

    EmitterComponent::~EmitterComponent ()
    {
        m_particles.clear();
        m_velocityLower = { 0.0f, 0.0f };
        m_velocityUpper = { 0.0f, 0.0f };
        m_lifetime = sf::Time::Zero;
        m_nextSpawn = sf::Time::Zero;
        m_limit = 0;

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void EmitterComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_velocityNode = a_node.child("Velocity");
        xml_node l_particleNode = a_node.child("Particles");

        float l_minvx = l_velocityNode.attribute("MinX").as_float(Constants::G_PARTICLE_LOWERBOUND_VELOCITY.x);
        float l_minvy = l_velocityNode.attribute("MinY").as_float(Constants::G_PARTICLE_LOWERBOUND_VELOCITY.y);
        float l_maxvx = l_velocityNode.attribute("MaxX").as_float(Constants::G_PARTICLE_UPPERBOUND_VELOCITY.x);
        float l_maxvy = l_velocityNode.attribute("MaxY").as_float(Constants::G_PARTICLE_UPPERBOUND_VELOCITY.y);

        unsigned int l_limit = l_particleNode.attribute("Limit").as_uint(Constants::G_PARTICLE_LIMIT);
        float l_lifetime = l_particleNode.attribute("Lifetime").as_float(Constants::G_PARTICLE_LIFETIME.asSeconds());
        float l_spawntime = l_particleNode.attribute("Spawntime").as_float(Constants::G_PARTICLE_SPAWN_TIME.asSeconds());
        int l_red = l_particleNode.attribute("Red").as_int(255);
        int l_green = l_particleNode.attribute("Green").as_int(255);
        int l_blue = l_particleNode.attribute("Blue").as_int(255);

        setLowerboundVelocity(l_minvx, l_minvy);
        setUpperboundVelocity(l_maxvx, l_maxvy);
        setParticleLimit(l_limit);
        setParticleLifetime(l_lifetime);
        setParticleSpawntime(l_spawntime);

        m_color = Utils::createColor(l_red, l_green, l_blue);

        if (sf::Shader::isAvailable())
        {
            xml_node l_shaderNode = a_node.child("Shader");

            if (l_shaderNode.type() != node_null)
                m_shader.loadFromXML(l_shaderNode);
        }
    }

    void EmitterComponent::update (const sf::Vector2f &a_transformPosition, const sf::Time &a_deltaTime, const sf::FloatRect &a_cameraRect)
    {
        for (auto l_iter = m_particles.begin(); l_iter != m_particles.end(); )
        {
            l_iter->m_lifetime += a_deltaTime;

            if (l_iter->m_lifetime >= m_lifetime)
            {
                l_iter = m_particles.erase(l_iter);
            }
            else
            {
                l_iter->m_position += (l_iter->m_velocity * a_deltaTime.asSeconds());
                l_iter++;
            }
        }

        if (a_cameraRect.contains(a_transformPosition * Constants::G_METERS_TO_PIXELS_F) == true)
        {
            m_nextSpawn += a_deltaTime;

            while (m_nextSpawn >= m_spawntime)
            {
                m_nextSpawn -= m_spawntime;

                spawnParticle();
            }
        }
    }

    void EmitterComponent::render (const sf::Vector2f &a_transformPosition, sf::RenderWindow &a_window, const sf::FloatRect &a_cameraRect)
    {
        if (a_cameraRect.contains(a_transformPosition * Constants::G_METERS_TO_PIXELS_F) == false)
        {
            return;
        }

        sf::RenderStates l_states;
        l_states.shader = m_shader.getShaderPtr();
        l_states.blendMode = sf::BlendAdd;

        for (auto& l_particle : m_particles)
        {
            l_particle.m_circle.setPosition((l_particle.m_position + a_transformPosition) * Constants::G_METERS_TO_PIXELS_F);

            a_window.draw(l_particle.m_circle, l_states);
        }
    }

    void EmitterComponent::setLowerboundVelocity(const float a_x, const float a_y)
    {
        if (a_x > m_velocityUpper.x) { m_velocityLower.x = m_velocityUpper.x; }
        else { m_velocityLower.x = a_x; }

        if (a_y > m_velocityUpper.y) { m_velocityLower.y = m_velocityUpper.y; }
        else { m_velocityLower.y = a_y; }
    }

    void EmitterComponent::setUpperboundVelocity(const float a_x, const float a_y)
    {
        if (a_x < m_velocityLower.x) { m_velocityUpper.x = m_velocityLower.x; }
        else { m_velocityUpper.x = a_x; }

        if (a_y < m_velocityLower.y) { m_velocityUpper.y = m_velocityLower.y; }
        else { m_velocityUpper.y = a_y; }
    }

}
