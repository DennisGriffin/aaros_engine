// File: Aaros_GUI_Progressbar.cpp

#include "Aaros_GUI_Progressbar.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace GUI
    {

        void Progressbar::updateWidgetInternals ()
        {
            // Local Variable Setup
            float l_fillWidth = 0.0f;

            // Calculate the width of the fill rectangle.
            if (m_maximum > 0)
            {
                // Cast the value and maximum ints into floats.
                float l_value       = static_cast<float>(m_value);
                float l_maximum     = static_cast<float>(m_maximum);

                // Divide the current value by the maximum value.
                // Multiply the quotient by the total width of the widget.
                l_fillWidth = ((l_value / l_maximum) * m_size.x);

                // If the fill width comes to below zero, set it to zero.
                if (l_fillWidth <= 0.0f) { l_fillWidth = 0.0f; }
            }

            // Configure the progress bar's rectangles.
            m_backRect.setPosition(m_position);
            m_backRect.setSize(m_size);
            m_backRect.setOutlineThickness(4);
            m_fillRect.setOutlineThickness(0);
            m_fillRect.setPosition(m_position);
            m_fillRect.setSize({ l_fillWidth, m_size.y });
        }

        Progressbar::Progressbar (Entity *ap_entity, const std::string &a_name) :
            Widget              { ap_entity, a_name },
            m_size              { Constants::G_PROGRESSBAR_SIZE },
            m_value             { 0 },
            m_maximum           { 100 }
        {
            m_backRect.setOutlineColor(Constants::G_PROGRESSBAR_BACK_BORDER_COLOR);
            m_backRect.setFillColor(Constants::G_PROGRESSBAR_BACK_FILL_COLOR);
            m_fillRect.setFillColor(Constants::G_PROGRESSBAR_FILL_COLOR);

            m_focusable = false;

            updateWidgetInternals();
        }

        Progressbar::~Progressbar ()
        {
            m_size = { 0.0f, 0.0f };
            m_value = 0;
            m_maximum = 0;

            m_enabled = false;
            m_visible = false;
            m_focused = false;
            m_focusable = false;
            m_name.clear();
            m_position = { 0.0f, 0.0f };
            mp_entity = nullptr;
        }

        void Progressbar::loadFromXML (const pugi::xml_node &a_node)
        {
            using namespace pugi;

            // Get the child nodes that define this widget.
            xml_node l_positionNode     = a_node.child("Position");
            xml_node l_sizeNode         = a_node.child("Size");
            xml_node l_progressNode     = a_node.child("Progress");
            xml_node l_colorNode        = a_node.child("Color");

            // The progressbar's position.
            float l_x                   = l_positionNode.attribute("X").as_float();
            float l_y                   = l_positionNode.attribute("Y").as_float();

            // The progressbar's size.
            float l_width               = l_sizeNode.attribute("Width").as_float(Constants::G_PROGRESSBAR_SIZE.x);
            float l_height              = l_sizeNode.attribute("Height").as_float(Constants::G_PROGRESSBAR_SIZE.y);

            // The progressbar's progress values.
            int l_value                 = l_progressNode.attribute("Value").as_int(0);
            int l_maximum               = l_progressNode.attribute("Maximum").as_int(100);

            // The progressbar's colors.
            int l_backBorderR           = l_colorNode.child("BackBorder").attribute("R").as_int(Constants::G_PROGRESSBAR_BACK_BORDER_COLOR.r);
            int l_backBorderG           = l_colorNode.child("BackBorder").attribute("G").as_int(Constants::G_PROGRESSBAR_BACK_BORDER_COLOR.g);
            int l_backBorderB           = l_colorNode.child("BackBorder").attribute("B").as_int(Constants::G_PROGRESSBAR_BACK_BORDER_COLOR.b);
            int l_backFillR             = l_colorNode.child("BackFill").attribute("R").as_int(Constants::G_PROGRESSBAR_BACK_FILL_COLOR.r);
            int l_backFillG             = l_colorNode.child("BackFill").attribute("G").as_int(Constants::G_PROGRESSBAR_BACK_FILL_COLOR.g);
            int l_backFillB             = l_colorNode.child("BackFill").attribute("B").as_int(Constants::G_PROGRESSBAR_BACK_FILL_COLOR.b);
            int l_barFillR              = l_colorNode.child("Bar").attribute("R").as_int(Constants::G_PROGRESSBAR_FILL_COLOR.r);
            int l_barFillG              = l_colorNode.child("Bar").attribute("G").as_int(Constants::G_PROGRESSBAR_FILL_COLOR.g);
            int l_barFillB              = l_colorNode.child("Bar").attribute("B").as_int(Constants::G_PROGRESSBAR_FILL_COLOR.b);

            // Assign the values.
            setPosition(l_x, l_y);
            setSize(l_width, l_height);
            setBackBorderColor(l_backBorderR, l_backBorderG, l_backBorderB);
            setBackFillColor(l_backFillR, l_backFillG, l_backFillB);
            setFillColor(l_barFillR, l_barFillG, l_barFillB);

            if (l_maximum < 1)  { m_maximum = 1; }
            else                { m_maximum = l_maximum; }

            if (l_value < 0)    { m_value = 0; }
            else                { m_value = Math::clampInteger(l_value, 0, l_maximum); }

            // Internally update the widget.
            updateWidgetInternals();
        }

        void Progressbar::render (sf::RenderWindow &a_window)
        {
            a_window.draw(m_backRect);
            a_window.draw(m_fillRect);
        }

        void Progressbar::setValue(const int a_value)
        {
            m_value = Math::clampInteger(a_value, 0, m_maximum);

            mp_entity->callLuaFunction(m_name + "_onProgressbarValueChanged", m_value, m_maximum);
            updateWidgetInternals();
        }

        void Progressbar::setMaximum(const int a_maximum)
        {
            if (a_maximum < 1)  { m_maximum = 1; }
            else                { m_maximum = a_maximum; }

            m_value = Math::clampNumber(m_value, 0, m_maximum);

            mp_entity->callLuaFunction(m_name + "_onProgressbarMaximumChanged", m_maximum);
            updateWidgetInternals();
        }

    }
}
