// File: Aaros_System.hpp
//
// The base class for our entity systems.

#pragma once

#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "Aaros_Entity.hpp"

namespace Aaros
{

    class System
    {
    protected /* Members */:
        sf::FloatRect           m_cameraRect = { 0.0f, 0.0f, 0.0f, 0.0f };

    protected /* Methods */:
        virtual bool            isEntityValid (Entity* ap_entity) = 0;

    public /* Methods */:
        virtual void update (const Entity::Container& a_entities,
                             const sf::Time& a_deltaTime,
                             const unsigned int a_entityCount)
        {
            (void) a_entities;
            (void) a_deltaTime;
            (void) a_entityCount;
        }

        virtual void fixedUpdate (const Entity::Container& a_entities,
                                  const sf::Time& a_timestep,
                                  const unsigned int a_entityCount)
        {
            (void) a_entities;
            (void) a_timestep;
            (void) a_entityCount;
        }

        virtual void render (const Entity::Container& a_entities,
                             sf::RenderWindow& a_window,
                             const unsigned int a_entityCount)
        {
            (void) a_entities;
            (void) a_window;
            (void) a_entityCount;
        }

        virtual void updateCameraRect (const sf::View& a_view)
        {
            m_cameraRect = {
                (a_view.getCenter().x - (a_view.getSize().x / 2.0f)) - 64.0f,
                (a_view.getCenter().y - (a_view.getSize().y / 2.0f)) - 64.0f,
                a_view.getSize().x + 128.0f,
                a_view.getSize().y + 128.0f
            };
        }
    };

}
