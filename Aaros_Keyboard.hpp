// File: Aaros_Keyboard.hpp
//
// Handles all events raised by the user via the keyboard.

#pragma once

#include <map>
#include <SFML/Window/Event.hpp>
#include "Aaros_Utils_Singleton.hpp"

namespace Aaros
{

    /**
     * This singleton class handles all events raised by the user
     * through the keyboard.
     */
    class Keyboard : public Utils::Singleton<Keyboard>
    {
    private /* Members */:
        std::map<int, bool>     m_pressed;  /** A map of all keys that have just been pressed. */
        std::map<int, bool>     m_down;     /** A map of all keys that are being held down. */

        char            m_unicode { 0 };

    public /* Methods */:
        /**
         * Checks the SFML event handler given for the following events:
         *
         * - sf::Event::KeyPressed
         * - sf::Event::KeyReleased
         *
         * @param   a_event             The application's SFML event handler.
         */
        void                    pollEvent (sf::Event& a_event);

        /**
         * Checks to see if the given key has just been pressed.
         *
         * @param   a_index             The SFML index of the key.
         *
         * @return  True if the key has just been pressed, false otherwise.
         */
        bool                    isKeyPressed (const int a_index);

        /**
         * Checks to see if the given key is being held down.
         *
         * @param   a_index             The SFML index of the key.
         *
         * @return  True if the key is being held down, false otherwise.
         */
        bool                    isKeyDown (const int a_index);

        char                    getLastChar ();

        void                    resetKeyPress ();

        void                    resetUnicode ();
    };

}
