// File: Aaros_Shader.cpp

#include <iostream>
#include "Aaros_Shader.hpp"

namespace Aaros
{

    void Shader::loadShader (const std::string &a_id)
    {
        if (a_id.empty()) { return; }

        ShaderManager& SM = ShaderManager::getInstance();

        if (m_vertex.empty() == false && m_fragment.empty() == false)
        {
            SM.loadSpecial(m_vertex, m_fragment, a_id);
        }
        else if (m_vertex.empty() == false && m_fragment.empty() == true)
        {
            SM.loadSpecial(m_vertex, sf::Shader::Vertex, a_id);
        }
        else if (m_vertex.empty() == true && m_fragment.empty() == false)
        {
            SM.loadSpecial(m_fragment, sf::Shader::Fragment, a_id);
        }
        else
        {
            std::cout << "[Shader::loadShader] No vertex or fragment shader file provided. Assuming shader already loaded..." << std::endl;
        }

        mp_shader = &SM.get(a_id);
    }

    Shader::Shader (sf::Sprite *ap_sprite) :
        mp_shader       { nullptr },
        mp_sprite       { ap_sprite },
        m_vertex        { "" },
        m_fragment      { "" },
        m_enabled       { true }
    {

    }

    Shader::~Shader ()
    {
        mp_shader = nullptr;
        mp_sprite = nullptr;
        m_vertex.clear();
        m_fragment.clear();
        m_enabled = false;
    }

    void Shader::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        std::string l_id = a_node.attribute("ID").as_string();
        std::string l_vertex = a_node.attribute("Vertex").as_string();
        std::string l_fragment = a_node.attribute("Fragment").as_string();

        if (l_id.empty() == false)
        {
            m_vertex = l_vertex;
            m_fragment = l_fragment;

            loadShader(l_id);
        }
    }

    void Shader::setFloat (const std::string &a_name, const float a_float)
    {
        if (mp_shader != nullptr)
            mp_shader->setParameter(a_name, a_float);
    }

    void Shader::setVec2 (const std::string &a_name, const float a_one, const float a_two)
    {
        if (mp_shader != nullptr)
            mp_shader->setParameter(a_name, a_one, a_two);
    }

    void Shader::setVec3 (const std::string &a_name, const float a_one, const float a_two, const float a_three)
    {
        if (mp_shader != nullptr)
            mp_shader->setParameter(a_name, a_one, a_two, a_three);
    }

    void Shader::setVec4 (const std::string &a_name, const float a_one, const float a_two, const float a_three, const float a_four)
    {
        if (mp_shader != nullptr)
            mp_shader->setParameter(a_name, a_one, a_two, a_three, a_four);
    }

    void Shader::setCurrentTexture (const std::string &a_name)
    {
        if (mp_shader != nullptr && mp_sprite != nullptr)
        {
            if (mp_sprite->getTexture() != nullptr)
            {
                mp_shader->setParameter(a_name, *mp_sprite->getTexture());
            }
        }
    }

}
