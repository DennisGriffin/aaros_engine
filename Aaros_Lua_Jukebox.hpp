// File: Aaros_Lua_Jukebox.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeJukebox (sol::state& a_state);

    }
}
