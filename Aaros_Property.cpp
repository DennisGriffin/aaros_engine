// File: Aaros_Property.cpp

#include "Aaros_Property.hpp"

namespace Aaros
{

    Property::Property (const std::string &a_key, const std::string &a_value, const bool a_save) :
        m_key               { a_key },
        m_value             { a_value },
        m_save              { a_save }
    {

    }

    Property::~Property ()
    {
        m_key.clear();
        m_value.clear();
        m_save = false;
    }

    std::string Property::get (const std::string &a_default) const
    {
        if (m_value.empty())
            return a_default;

        return m_value;
    }

    template <>
    int Property::getAs (const int a_default) const
    {
        int l_return = 0;

        try             { l_return = std::stoi(m_value); }
        catch (...)     { l_return = a_default; }

        return l_return;
    }

    template <>
    unsigned int Property::getAs (const unsigned int a_default) const
    {
        unsigned int l_return = 0;

        try             { l_return = std::stoul(m_value); }
        catch (...)     { l_return = a_default; }

        return l_return;
    }

    template <>
    float Property::getAs (const float a_default) const
    {
        float l_return = 0.0f;

        try             { l_return = std::stof(m_value); }
        catch (...)     { l_return = a_default; }

        return l_return;
    }

    template <>
    bool Property::getAs (const bool a_default) const
    {
        if (m_value == "true")          { return true; }
        else if (m_value == "false")    { return false; }

        return a_default;
    }

    void Property::set (const std::string &a_value)
    {
        m_value = a_value;
    }

    template <>
    void Property::setAs (const int a_value)
    {
        m_value = std::to_string(a_value);
    }

    template <>
    void Property::setAs (const unsigned int a_value)
    {
        m_value = std::to_string(a_value);
    }

    template <>
    void Property::setAs (const float a_value)
    {
        m_value = std::to_string(a_value);
    }

    template <>
    void Property::setAs (const bool a_value)
    {
        if (a_value == true)    { m_value = "true"; }
        else                    { m_value = "false"; }
    }

}
