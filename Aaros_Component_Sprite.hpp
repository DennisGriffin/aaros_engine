// File: Aaros_Component_Sprite.hpp
//
// Allows the owning entity to portray itself by drawing a sprite onscreen.

#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include "Aaros_Shader.hpp"
#include "Aaros_Component.hpp"

namespace Aaros
{

    /**
     * This entity component allows its owning entity to portray itself by drawing
     * a simple sprite on the screen. The sprite can be static, or animated through the use
     * of an animation entity component.
     */
    class SpriteComponent : public Component
    {
    private /* Members */:
        sf::Sprite              m_sprite;               /** The sprite to be drawn onscreen. */
        Shader                  m_shader;               /** The shader that the sprite will be drawn with. */

    public /* Constructor and Destructor */:
        SpriteComponent (Entity* ap_entity,
                         const std::string& a_entityName,
                         const unsigned int a_entityID);
        ~SpriteComponent ();

    public /* Inherited Methods */:
        /**
         * Loads information about the sprite's image and shader.
         */
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Getter-Setters */:
        inline sf::Sprite&      getSprite () { return m_sprite; }
        inline Shader&          getShader () { return m_shader; }

    };

}
