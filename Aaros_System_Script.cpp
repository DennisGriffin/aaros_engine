// File: Aaros_System_Script.cpp

#include "Aaros_System_Script.hpp"

namespace Aaros
{

    bool ScriptSystem::isEntityValid(Entity *ap_entity)
    {
        if (ap_entity->isEnabled() == false) { return false; }

        bool l_hasScript = ap_entity->hasComponent<ScriptComponent>();
        if (l_hasScript == false) { return false; }

        if (ap_entity->getComponent<ScriptComponent>().isEnabled() == false)
            return false;

        return true;
    }

    void ScriptSystem::update(const Entity::Container &a_entities, const sf::Time &a_deltaTime, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            TransformComponent& l_transform = *l_ptr->getTransform();

            if (m_cameraRect.contains(l_transform.getPixelPosition()))
            {
                ScriptComponent& l_script = l_ptr->getComponent<ScriptComponent>();
                l_script.callFunction("onUpdate", a_deltaTime.asSeconds());
            }
        }
    }

    void ScriptSystem::fixedUpdate(const Entity::Container &a_entities, const sf::Time &a_timestep, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            TransformComponent& l_transform = *l_ptr->getTransform();

            if (m_cameraRect.contains(l_transform.getPixelPosition()))
            {
                ScriptComponent& l_script = l_ptr->getComponent<ScriptComponent>();
                l_script.callFunction("onFixedUpdate", a_timestep.asSeconds());
            }
        }
    }

    void ScriptSystem::updateCameraRect (const sf::View &a_view)
    {
        m_cameraRect = {
            (a_view.getCenter().x - (a_view.getSize().x / 2.0f)) - 256.0f,
            (a_view.getCenter().y - (a_view.getSize().y / 2.0f)) - 256.0f,
            a_view.getSize().x + 512.0f,
            a_view.getSize().y + 512.0f
        };
    }

}
