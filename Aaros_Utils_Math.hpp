// File: Aaros_Utils_Math.hpp
//
// Contains some extra, useful math functions.

#pragma once

#include <SFML/System/Vector2.hpp>
#include <cstdlib>
#include <cmath>
#include <ctime>

namespace Aaros
{
    namespace Constants
    {
        const float     G_PIXELS_TO_METERS = 0.03125f;
        const float     G_METERS_TO_PIXELS_F = 32.0f;
        const int       G_METERS_TO_PIXELS_I = 32;
    }

    namespace Math
    {

        inline float randomFloat (const float a_minimum, const float a_maximum)
        {
            static bool s_seeded = false;

            if (s_seeded == false)
            {
                std::srand(static_cast<unsigned int>(std::time(nullptr)));
            }

            const float L_RAND_MAX = static_cast<float>(RAND_MAX);
            float l_random = static_cast<float>(std::rand());
            float l_return = a_minimum + (l_random / (L_RAND_MAX / (a_maximum - a_minimum)));

            return l_return;
        }

        inline float clampNumber (const float a_number, const float a_minimum, const float a_maximum)
        {
            if (a_maximum < a_minimum) { return a_number; }

            if (a_number < a_minimum) { return a_minimum; }
            else if (a_number > a_maximum) { return a_maximum; }

            return a_number;
        }

        inline int clampInteger (const int a_number, const int a_minimum, const int a_maximum)
        {
            if (a_maximum < a_minimum) { return a_number; }

            if (a_number < a_minimum) { return a_minimum; }
            else if (a_number > a_maximum) { return a_maximum; }

            return a_number;
        }

        inline float getVectorLengthSquared (const sf::Vector2f& a_vector)
        {
            return a_vector.x * a_vector.x + a_vector.y * a_vector.y;
        }

        inline float getVectorLength (const sf::Vector2f& a_vector)
        {
            return std::sqrt(getVectorLengthSquared(a_vector));
        }

        inline float getVectorDot (const sf::Vector2f& a_one,
                                   const sf::Vector2f& a_two)
        {
            return a_one.x * a_two.x + a_one.y * a_two.y;
        }

        inline float getVectorCross (const sf::Vector2f& a_one,
                                     const sf::Vector2f& a_two)
        {
            return a_one.x * a_two.y - a_one.y * a_two.x;
        }

        inline sf::Vector2f getVectorAbs (const sf::Vector2f& a_vector)
        {
            return { std::abs(a_vector.x), std::abs(a_vector.y) };
        }

        inline sf::Vector2f normalizeVector (const sf::Vector2f& a_vector)
        {
            float l_length = getVectorLength(a_vector);
            if (l_length == 0.0f) { return { 0.0f, 0.0f }; }

            return { a_vector.x / l_length, a_vector.y / l_length };
        }

    }
}
