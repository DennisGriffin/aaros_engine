// File: Aaros_Utils_GUI.hpp
//
// Some useful utility functions for our GUI system.

#pragma once

#include <string>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include "Aaros_AssetManager.hpp"

namespace Aaros
{
    namespace GUI
    {

        inline float getCharacterWidth (const char a_char,
                                        const unsigned int a_size,
                                        const sf::Font& a_font)
        {
            sf::Glyph l_glyph = a_font.getGlyph(a_char, a_size, false);
            return l_glyph.advance;
        }

        inline float getStringWidth (const std::string& a_string,
                                     const unsigned int a_size,
                                     const sf::Font& a_font)
        {
            float l_width = 0.0f;

            for (const char l_char : a_string)
            {
                l_width += getCharacterWidth(l_char, a_size, a_font);
            }

            return l_width;
        }

        inline std::string wrapText (const std::string& a_textToWrap,
                                     const sf::Vector2f& a_wrapSize,
                                     const sf::Font& a_font,
                                     const unsigned int a_fontSize = 14)
        {
            // Set up some locals.
            std::string l_textWrapped   = "";
            std::string l_currentLine   = "";
            float       l_charWidth     = 0.0f;
            float       l_lineWidth     = 0.0f;
            float       l_areaHeight    = 0.0f;
            float       l_lineHeight    = a_font.getLineSpacing(a_fontSize);

            // Iterate through each character in the string.
            for (char l_char : a_textToWrap)
            {
                // If the character is a tab, then just ignore it.
                if (l_char == '\t') { continue; }

                // First, check for the newline character.
                if (l_char == '\n')
                {
                    // Add the current line, and a newline character to the wrapped text string.
                    l_textWrapped += l_currentLine + "\n";

                    // Check to see if the text's height, plus the height of the current
                    // line exceeds the height of the text area.
                    // If it does, then exit.
                    if (l_areaHeight + l_lineHeight >= a_wrapSize.y - l_lineHeight)
                    {
                        break;
                    }

                    // Start a new line.
                    l_areaHeight += l_lineHeight;
                    l_currentLine.clear();
                    l_lineWidth = 0.0f;

                    // Continue.
                    continue;
                }

                // Get the width of the current character.
                l_charWidth = getCharacterWidth(l_char, a_fontSize, a_font);

                // Check to see if the length of the current line, plus that of the current
                // word, exceeds the width of the text area.
                if (l_lineWidth + l_charWidth >= a_wrapSize.x)
                {
                    // Find the last space in the current line.
                    std::size_t l_lastSpace = l_currentLine.find_last_of(' ');

                    if (l_lastSpace != std::string::npos && l_currentLine.back() != ' ')
                    {
                        // Get the contents of the current line after that space.
                        std::string l_afterLastSpace = l_currentLine.substr(l_lastSpace + 1);

                        // Erase all characters in the current line string, from that space.
                        l_currentLine.erase(l_lastSpace);

                        // Check to see if the text's height, plus the height of the current
                        // line exceeds the height of the text area.
                        // If it does, then exit.
                        if (l_areaHeight + l_lineHeight >= a_wrapSize.y - l_lineHeight)
                        {
                            break;
                        }

                        // Add the current line, and a newline character to the wrapped text string.
                        l_textWrapped += l_currentLine + "\n";

                        // Start a new line and add what we got after that space to it.
                        l_areaHeight += l_lineHeight;
                        l_currentLine = l_afterLastSpace;
                        l_lineWidth = getStringWidth(l_afterLastSpace, a_fontSize, a_font);
                    }
                    else
                    {
                        // Check to see if the text's height, plus the height of the current
                        // line exceeds the height of the text area.
                        // If it does, then exit.
                        if (l_areaHeight + l_lineHeight >= a_wrapSize.y - l_lineHeight)
                        {
                            break;
                        }

                        // Add the current line, and a newline character to the wrapped text string.
                        l_textWrapped += l_currentLine + "\n";

                        // Start a new line.
                        l_areaHeight += l_lineHeight;
                        l_currentLine.clear();
                        l_lineWidth = 0.0f;
                    }
                }

                // Append the character.
                l_currentLine += l_char;
                l_lineWidth += l_charWidth;
            }

            if (!l_currentLine.empty())
            {
                l_textWrapped += l_currentLine;
            }

            return l_textWrapped;
        }

        inline std::string setTextObjectFont (sf::Text& a_text, const std::string& a_fontID = "globalFont")
        {
            FontManager& FM = FontManager::getInstance();
            std::string l_return = "globalFont";

            if (a_fontID.empty() || a_fontID == "globalFont")
            {
                a_text.setFont(FM.get("globalFont"));
            }
            else
            {
                l_return = a_fontID;
                FM.load(a_fontID);
                a_text.setFont(FM.get(l_return));
            }

            return l_return;
        }

    }
}
