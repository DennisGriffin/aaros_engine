// File: Aaros_Component_Emitter.hpp
//
// Emits particles at the given position.

#pragma once

#include <cmath>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Clock.hpp>
#include "Aaros_Component.hpp"
#include "Aaros_Shader.hpp"
#include "Aaros_Utils_Random.hpp"
#include "Aaros_Utils_Misc.hpp"

namespace Aaros
{
    namespace Constants
    {

        const float         G_PARTICLE_RADIUS               = 1.0f;
        const sf::Vector2f  G_PARTICLE_LOWERBOUND_VELOCITY  = { -0.1f, -0.1f };
        const sf::Vector2f  G_PARTICLE_UPPERBOUND_VELOCITY  = { 0.1f, 0.1f };
        const sf::Time      G_PARTICLE_LIFETIME             = sf::seconds(1.0f);
        const sf::Time      G_PARTICLE_SPAWN_TIME           = sf::seconds(0.1f);
        const unsigned int  G_PARTICLE_LIMIT                = 30;

    }

    /**
     * Represents a particle that is emitted by the particle emitter component.
     */
    struct Particle
    {
        using Container     = std::vector<Particle>;

        sf::CircleShape     m_circle;       /** Represents the particle as rendered. */
        sf::Vector2f        m_position;     /** The particle's position. */
        sf::Vector2f        m_velocity;     /** The particle's velocity. Unaffected by the physics engine. */
        sf::Time            m_lifetime;     /** How long has the particle been onscreen. */

        Particle (const float a_x, const float a_y, const sf::Color& a_color) :
            m_position      { 0.0f, 0.0f },
            m_velocity      { a_x, a_y },
            m_lifetime      { sf::Time::Zero }
        {
            m_circle.setRadius(Constants::G_PARTICLE_RADIUS);
            m_circle.setOutlineThickness(0.0f);
            m_circle.setFillColor(a_color);
        }

        ~Particle ()
        {
            m_velocity = { 0.0f, 0.0f };
            m_lifetime = sf::Time::Zero;
        }
    };

    /**
     * This entity component allows the entity to portray itself by emitting
     * particles onscreen.
     */
    class EmitterComponent : public Component
    {
    private /* Particle Members */:
        Particle::Container m_particles;        /** The list of active particles. */
        sf::Vector2f        m_velocityLower;    /** The particles' minimum velocity. */
        sf::Vector2f        m_velocityUpper;    /** The particles' maximum velocity. */
        sf::Time            m_lifetime;         /** The maximum lifetime of the particles. */
        sf::Time            m_spawntime;        /** The time elapsed since the last spawn. */
        sf::Time            m_nextSpawn;        /** When should particles spawn next? */
        sf::Color           m_color;            /** The color of the particles. */
        unsigned int        m_limit;            /** The maximum number of particles onscreen at a time. */

        Shader              m_shader;           /** The shader that the particles will be rendered with, if any. */

    private /* Setter Callbacks */:
        /** Kills all active particles if the component is disabled. */
        virtual void        onEnabledChanged () override;

    private /* Methods */:
        /** Spawns a new particle with a random velocity within bounds. */
        void                spawnParticle ();

    public /* Constructor and Destructor */:
        EmitterComponent (Entity* ap_entity,
                          const std::string& a_entityName,
                          const unsigned int a_entityID);
        ~EmitterComponent ();

    public /* Inherited Methods */:
        /**
         * Loads information about the particles' spawn limit, lifetime, velocity, color,
         * and shader.
         */
        void                loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        /**
         * Called by the render system in order to actually spawn and kill particles.
         *
         * @param   a_transformPosition     The position of the entity's transform. Particles are spawned at this position.
         * @param   a_deltaTime             The amount of time elapsed since the previous frame.
         * @param   a_cameraRect            The camera rect maintained by the render system.
         */
        void                update (const sf::Vector2f& a_transformPosition,
                                    const sf::Time& a_deltaTime,
                                    const sf::FloatRect& a_cameraRect);

        /**
         * Called by the render system to render the particles.
         *
         * @param   a_transformPosition     The position of the entity's transform. Particles are spawned at this position.
         * @param   a_window                The window into which the particles will be drawn.
         * @param   a_cameraRect            The camera rect maintained by the render system.
         */
        void                render (const sf::Vector2f& a_transformPosition,
                                    sf::RenderWindow& a_window,
                                    const sf::FloatRect& a_cameraRect);

    public /* Outline Getter-Setters */:
        void                setLowerboundVelocity (const float a_x, const float a_y);
        void                setUpperboundVelocity (const float a_x, const float a_y);

    public /* Inline Getter-Setters */:
        inline Shader&      getShader () { return m_shader; }
        inline unsigned int getParticleCount () const { return m_particles.size(); }
        inline unsigned int getParticleLimit () const { return m_limit; }
        inline float        getLowerVelocityX () const { return m_velocityLower.x; }
        inline float        getLowerVelocityY () const { return m_velocityLower.y; }
        inline float        getUpperVelocityX () const { return m_velocityUpper.x; }
        inline float        getUpperVelocityY () const { return m_velocityUpper.y; }
        inline float        getParticleLifetime () const { return m_lifetime.asSeconds(); }
        inline float        getParticleSpawntime () const { return m_spawntime.asSeconds(); }

        inline void setParticleLimit (const unsigned int a_limit) { m_limit = a_limit; m_particles.clear(); }
        inline void setParticleLifetime (const float a_lifetime) { m_lifetime = sf::seconds(std::abs(a_lifetime)); m_particles.clear(); }
        inline void setParticleSpawntime (const float a_spawntime) { m_spawntime = sf::seconds(std::abs(a_spawntime)); m_nextSpawn = sf::Time::Zero; m_particles.clear(); }
        inline void setParticleColor (const int a_r, const int a_g, const int a_b) { m_color = Utils::createColor(a_r, a_g, a_b); }

    };
}
