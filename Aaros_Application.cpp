// File: Aaros_Application.cpp

#include "Aaros_Application.hpp"
#include "Aaros_PropertyManager.hpp"

namespace Aaros
{

    void Application::loadInitFile()
    {
        PropertyManager& PM = PropertyManager::getInstance();
        FontManager& FM = FontManager::getInstance();

        // Load the engine's initialization file. Throw if that fails.
        if (PM.loadFromFile(Constants::G_INIT_FILE, "Init") == false)
        {
            throw std::runtime_error {
                "[Application::loadInitFile] Failed to load system initialization file!"
            };
        }

        // Throw an exception if the property manager fails to find some engine-critical
        // properties, such as the global font.
        if (PM.get("globalFont").get() == "")
        {
            throw std::runtime_error {
                "[Application::loadInitFile] Failed to find engine-critical property 'globalFont'!"
            };
        }

        // Load the global font.
        FM.load(PM.get("globalFont").get(), "globalFont");

        // Set up the window properties.
        m_windowSize.x = PM.get("windowWidth").getAs<unsigned int>(Constants::G_WINDOW_WIDTH);
        m_windowSize.y = PM.get("windowHeight").getAs<unsigned int>(Constants::G_WINDOW_HEIGHT);
        m_windowFullscreen = PM.get("windowFullscreen").getAs<bool>(Constants::G_WINDOW_FULLSCREEN);
        m_windowTitle = PM.get("windowTitle").get(Constants::G_WINDOW_TITLE);

        // Let's also set up the FPS indicator.
        m_reportFPS.setFont(FM.get("globalFont"));
        m_reportFPS.setCharacterSize(14);
        m_reportFPS.setPosition(16.0f, 16.0f);
        m_reportFPS.setColor(sf::Color::White);
        m_reportFPS.setString("FPS: --");
    }

    void Application::createWindow()
    {
        PropertyManager& PM = PropertyManager::getInstance();

        // Validate the window's size.
        if (m_windowSize.x < Constants::G_WINDOW_WIDTH) { m_windowSize.x = Constants::G_WINDOW_WIDTH; }
        if (m_windowSize.y < Constants::G_WINDOW_HEIGHT) { m_windowSize.y = Constants::G_WINDOW_HEIGHT; }

        // Create the video mode. Use it to validate the fullscreen flag.
        sf::VideoMode l_videoMode { m_windowSize.x, m_windowSize.y };
        if (m_windowFullscreen == true)
        {
            m_windowFullscreen = l_videoMode.isValid();
            if (m_windowFullscreen == false)
            {
                std::cerr << "[Application::createWindow] "
                          << "Window size '" << m_windowSize.x << " x " << m_windowSize.y << "' "
                          << "is not valid for fullscreen mode. Reverting to windowed mode..." << std::endl;
            }
        }

        // Save the window attribute properties.
        PM.append("windowWidth", false).setAs<unsigned int>(m_windowSize.x);
        PM.append("windowHeight", false).setAs<unsigned int>(m_windowSize.y);
        PM.append("windowFullscreen", false).setAs<bool>(m_windowFullscreen);
        PM.append("windowTitle", false).set(m_windowTitle);

        // Now use the video mode to create the game window.
        if (m_windowFullscreen == true)
        {
            m_window.create(l_videoMode, m_windowTitle, sf::Style::Fullscreen);
        }
        else
        {
            m_window.create(l_videoMode, m_windowTitle, sf::Style::Close | sf::Style::Titlebar);
        }

        // Set up the window's other attributes.
        m_window.setVerticalSyncEnabled(true);
        m_window.setKeyRepeatEnabled(false);
    }

    void Application::loadStartScene()
    {
        std::string l_startScene = PropertyManager::getInstance().get("startScene").get();

        m_scene = std::make_unique<Scene>(this);
        if (!l_startScene.empty())
        {
            m_scene->loadFromFile(l_startScene);
        }
    }

    void Application::processEvents()
    {
        // Create an event handler object.
        sf::Event l_event;

        // Poll the game window.
        while (m_window.pollEvent(l_event))
        {
            // Check to see if the user closed the game window.
            if (l_event.type == sf::Event::Closed)
            {
                m_window.close();
            }

            // Poll the mouse and keyboard for events there.
            Keyboard::getInstance().pollEvent(l_event);
            Mouse::getInstance().pollEvent(l_event);
        }

        // Update the mouse cursor's worldspace position.
        Mouse::getInstance().updateWorldPosition(m_window, m_scene->getCameraView());
    }

    void Application::fixedUpdate(const sf::Time &a_timestep)
    {
        m_scene->fixedUpdate(a_timestep);
    }

    void Application::update(const sf::Time &a_deltaTime)
    {
        m_scene->update(a_deltaTime);

        Mouse::getInstance().resetMouseButtonPresses();
        Keyboard::getInstance().resetKeyPress();
        Keyboard::getInstance().resetUnicode();
    }

    void Application::updateFPS(const sf::Time &a_elapsedTime)
    {
        // Add to the frame time accumulator.
        m_frameTime += a_elapsedTime;

        // Increment the frame counter.
        m_frameCount++;

        // Check to see if it is time to report a new FPS to the user.
        if (m_frameTime >= sf::seconds(1.0f))
        {
            // Report the FPS.
            m_reportFPS.setString("FPS: " + std::to_string(m_frameCount));

            // Reset the frame accumulator and counter.
            m_frameTime -= sf::seconds(1.0f);
            m_frameCount = 0;
        }
    }

    void Application::render()
    {
        m_window.clear();
        m_scene->render(m_window);
        m_window.draw(m_reportFPS);
        m_window.display();
    }

    Application::Application () :
        m_windowSize            { Constants::G_WINDOW_WIDTH, Constants::G_WINDOW_HEIGHT },
        m_windowFullscreen      { Constants::G_WINDOW_FULLSCREEN },
        m_windowTitle           { Constants::G_WINDOW_TITLE },
        m_deltaTime             { sf::Time::Zero },
        m_frameTime             { sf::Time::Zero },
        m_frameCount            { 0 }
    {
        Math::Random::getInstance().initialize();

        loadInitFile();
        createWindow();
        loadStartScene();
    }

    Application::~Application ()
    {
        PropertyManager::getInstance().removeAll();
        TextureManager::getInstance().unloadAll();
        ShaderManager::getInstance().unloadAll();
        FontManager::getInstance().unloadAll();
        SoundManager::getInstance().unloadAll();
    }

    void Application::run ()
    {
        // Keep track of the time that elapses per frame.
        sf::Time l_elapsedTime = sf::Time::Zero;

        // Continue running this loop as long as the game window remains open.
        while (m_window.isOpen())
        {
            // 1. Run event handling.
            processEvents();

            // 2. Get the time elapsed since the previous frame.
            l_elapsedTime = m_appClock.restart();
            m_deltaTime += l_elapsedTime;

            // 3. See if we need to clamp the delta time, in order to avoid the
            // "spiral of death", in which physics calculations end up holding up the entire
            // game loop.
            if (m_deltaTime > Constants::G_DELTA_CAP)
            {
                m_deltaTime = Constants::G_DELTA_CAP;
            }

            // 4. Check to see if we need to request a fixed update.
            //
            // If a fixed update is requested, also run event-handling again.
            while (m_deltaTime >= Constants::G_FIXED_TIMESTEP)
            {
                m_deltaTime -= Constants::G_FIXED_TIMESTEP;

                processEvents();
                fixedUpdate(Constants::G_FIXED_TIMESTEP);
            }

            // 5. Run the standard update and update the FPS, using the elapsed time that
            // we got earlier.
            update(l_elapsedTime);
            updateFPS(l_elapsedTime);

            // 6. Finally, reflect the updates visually by rendering to the game window.
            render();
        }
    }

    void Application::quit()
    {
        m_window.close();
    }

    void Application::setWindowSize (const unsigned int a_width, const unsigned int a_height)
    {
        if (a_width != m_windowSize.x || a_height != m_windowSize.y)
        {
            m_windowSize = { a_width, a_height };
            createWindow();
        }
    }

    void Application::setWindowFullscreen (const bool a_fullscreen)
    {
        if (a_fullscreen != m_windowFullscreen)
        {
            m_windowFullscreen = a_fullscreen;
            createWindow();
        }
    }

}
