// File: Aaros_System_Timer.cpp

#include "Aaros_System_Timer.hpp"

namespace Aaros
{

    bool TimerSystem::isEntityValid (Entity *ap_entity)
    {
        if (ap_entity->isEnabled() == false) { return false; }

        if (ap_entity->hasComponent<TimerComponent>() == false)
            return false;

        if (ap_entity->getComponent<TimerComponent>().isEnabled() == false)
            return false;

        return true;
    }

    void TimerSystem::update (const Entity::Container &a_entities, const sf::Time &a_deltaTime, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            TransformComponent& l_transform = *l_ptr->getTransform();
            if (m_cameraRect.contains(l_transform.getPixelPosition()))
            {
                TimerComponent& l_timer = l_ptr->getComponent<TimerComponent>();
                l_timer.update(a_deltaTime);
            }
        }
    }

    void TimerSystem::updateCameraRect (const sf::View &a_view)
    {
        m_cameraRect = {
            (a_view.getCenter().x - (a_view.getSize().x / 2.0f)) - 256.0f,
            (a_view.getCenter().y - (a_view.getSize().y / 2.0f)) - 256.0f,
            a_view.getSize().x + 512.0f,
            a_view.getSize().y + 512.0f
        };
    }

}
