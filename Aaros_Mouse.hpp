// File: Aaros_Mouse.hpp
//
// Handles all events raised by the user via the mouse.

#pragma once

#include <map>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Window/Event.hpp>
#include "Aaros_Utils_Math.hpp"
#include "Aaros_Utils_Singleton.hpp"

namespace Aaros
{

    /**
     * This helper class handles events raised by the user via the mouse.
     */
    class Mouse : public Utils::Singleton<Mouse>
    {
    private /* Members */:
        friend class Utils::Singleton<Mouse>;

    private /* Members */:
        std::map<int, bool> m_pressed;  /** A list of mouse buttons that have just been pressed. */
        std::map<int, bool> m_down;     /** A list of mouse buttons that are being held down. */
        sf::Vector2i        m_position; /** The position of the mouse cursor, relative to the game window. */
        sf::Vector2f        m_coords;   /** The position of the mouse cursor in world space. */
        int                 m_delta;    /** The change in position of the mouse wheel. */

    public /* Methods */:
        /**
         * Uses the scene's camera to provide the position of the mouse cursor in world space.
         *
         * @param   a_view              A handle to the scene's camera view.
         */
        void                updateWorldPosition (sf::RenderWindow& a_window,
                                                 const sf::View& a_view);

        /**
         * Polls the mouse for the following SFML events:
         *
         * - sf::Event::MouseButtonPressed
         * - sf::Event::MouseButtonReleased
         * - sf::Event::MouseMoved
         * - sf::Event::MouseWheelScrolled
         *
         * @param   a_event             A handle to the application's SFML event handler.
         *
         * @note    sf::Event::MouseWheelScrolled replaces sf::Event::MouseWheelMoved in SFML 2.3.
         */
        void                pollEvent (sf::Event& a_event);

        /**
         * Checks to see if the given mouse button has just been pressed.
         *
         * @param   a_index             The SFML index of the mouse button.
         *
         * @return  True if the mouse button was just pressed, false otherwise.
         */
        bool                isButtonPressed (const int a_index);

        /**
         * Checks to see if the given mouse button is being held down.
         *
         * @param   a_index             The SFML index of the mouse button
         *
         * @return  True if the mouse button is being held down, false otherwise.
         */
        bool                isButtonDown (const int a_index);

        /**
         * Gets the position of the mouse cursor, relative to the top-left position
         * of the game window.
         *
         * @return  The position of the mouse cursor.
         */
        sf::Vector2f        getPosition () const;

        /**
         * Gets the position of the mouse cursor in world space.
         *
         * @return  The position of the mosue cursor in world space.
         */
        sf::Vector2f        getWorldPosition () const;

        /**
         * Retrieves, then resets the current mouse wheel delta.
         *
         * @return  The current mouse wheel delta.
         */
        int                 getWheelDelta ();

        /**
         * Resets the state of all mouse button presses.
         */
        void                resetMouseButtonPresses ();

    public /* Getter-Setters */:
        inline float        getPositionX () const { return m_position.x; }
        inline float        getPositionY () const { return m_position.y; }
        inline float        getCoordX () const { return m_coords.x; }
        inline float        getCoordY () const { return m_coords.y; }
        inline float        getMeterPositionX () const { return m_position.x / Constants::G_METERS_TO_PIXELS_F; }
        inline float        getMeterPositionY () const { return m_position.y / Constants::G_METERS_TO_PIXELS_F; }
        inline float        getMeterCoordX () const { return m_coords.x / Constants::G_METERS_TO_PIXELS_F; }
        inline float        getMeterCoordY () const { return m_coords.y / Constants::G_METERS_TO_PIXELS_F; }

    };

}
