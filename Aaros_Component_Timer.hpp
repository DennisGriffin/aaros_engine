// File: Aaros_Component_Timer.hpp
//
// Provides the entity with events that fire on custom intervals.

#pragma once

#include <vector>
#include <algorithm>
#include <SFML/System/Time.hpp>
#include "Aaros_Component.hpp"

namespace Aaros
{

    namespace Constants
    {

        const sf::Time      G_TIMER_INTERVAL            = sf::seconds(5.0f);

    }

    struct TimeEvent
    {
        using Container     = std::vector<TimeEvent>;
        using Iterator      = Container::iterator;

        sf::Time            m_interval;
        sf::Time            m_elapsed;
        std::string         m_id;
        bool                m_enabled;

        TimeEvent (const std::string& a_id,
                   const float a_interval,
                   const bool a_enabled = true) :
            m_interval      { sf::seconds(a_interval) },
            m_elapsed       { sf::Time::Zero },
            m_id            { a_id },
            m_enabled       { a_enabled }
        {}

        ~TimeEvent ()
        {
            m_interval = sf::Time::Zero;
            m_id.clear();
            m_enabled = false;
        }
    };

    class TimerComponent : public Component
    {
    private /* Members */:
        TimeEvent::Container    m_events;

    private /* Search Methods */:
        TimeEvent::Iterator     findTimeEvent (const std::string& a_id);

    public /* Constructor and Destructor */:
        TimerComponent (Entity* ap_entity,
                        const std::string& a_entityName,
                        const unsigned int a_entityID);
        ~TimerComponent ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        void                    setTimeEventInterval (const std::string& a_id,
                                                      const float a_interval);
        void                    toggleTimeEvent (const std::string& a_id,
                                                 const bool a_enabled);
        void                    resetTimer (const std::string& a_id);
        void                    resetAllTimers ();
        void                    update (const sf::Time& a_timestep);

    };

}
