// File: Aaros_GUI_Scrollbar.hpp
//
// Presents a scrollbar on screen.

#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include "Aaros_GUI_Widget.hpp"

namespace Aaros
{
    namespace Constants
    {

        const sf::Vector2f  G_SCROLLBAR_BUTTON_SIZE     = { 32.0f, 32.0f };
        const float         G_SCROLLBAR_THICKNESS       = 2.0f;
        const float         G_SCROLLBAR_LONG_SIZE       = 256.0f;
        const float         G_SCROLLBAR_SHORT_SIZE      = 16.0f;
        const sf::Color     G_SCROLLBAR_BUTTON_COLOR    = { 154, 154, 154 };
        const sf::Color     G_SCROLLBAR_HOVER_COLOR     = { 33, 33, 33 };
        const sf::Color     G_SCROLLBAR_BACK_COLOR      = { 255, 255, 255 };

    }

    namespace GUI
    {

        /**
         * Enumerates the directions in which the scrollbar can be oriented.
         */
        enum class ScrollbarOrientation
        {
            Vertical,
            Horizontal
        };

        /**
         * Presents a scrollbar onscreen.
         */
        class Scrollbar : public Widget
        {
        private /* Members */:
            sf::RectangleShape      m_minusButton;      /** The scrollbar's "scroll up/left" button. */
            sf::RectangleShape      m_plusButton;       /** The scrollbar's "scroll down/right" button. */
            sf::RectangleShape      m_positionButton;   /** The scrollbar's position button. */
            sf::RectangleShape      m_body;             /** The scrollbar's body. */
            sf::Color               m_backColor;        /** The scrollbar's back color. */
            sf::Color               m_buttonColor;      /** The scrollbar's button color. */
            sf::Color               m_hoverColor;       /** The scrollbar's hover color. */
            ScrollbarOrientation    m_orientation;      /** The scrollbar's orientation. */
            float                   m_longSize;         /** The scrollbar's long size. */
            float                   m_shortSize;        /** The scrollbar's short size. */
            unsigned int            m_value;            /** The scrollbar's current value. */
            unsigned int            m_maximum;          /** The scrollbar's maximum value. */

        private /* Inherited Methods */:
            void                    updateWidgetInternals () override;

        private /* Other Methods */:
            void                    updateButtons (sf::RectangleShape& a_button,
                                                   const int a_id);

        public /* Constructor and Destructor */:
            Scrollbar (Entity* ap_entity, const std::string& a_name);
            ~Scrollbar ();

        public /* Inherited Methods */:
            void                    loadFromXML (const pugi::xml_node &a_node) override;
            void                    update (const sf::Time &a_deltaTime) override;
            void                    render (sf::RenderWindow &a_window) override;

        public /* Outline Getter-Setters */:
            void                setValue (const unsigned int a_value);
            void                setMaximum (const unsigned int a_value);

        public /* Getter-Setters */:
            inline float        getLongSize () const { return m_longSize; }
            inline float        getShortSize () const { return m_shortSize; }
            inline unsigned int getValue () const { return m_value; }
            inline unsigned int getMaximum () const { return m_maximum; }

            inline void setLongSize (const float a_long) { m_longSize = std::abs(a_long); }
            inline void setOrientation (const ScrollbarOrientation a_orientation) { m_orientation = a_orientation; }
            inline void setBodyColor (const int a_r, const int a_g, const int a_b) { m_backColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setButtonColor (const int a_r, const int a_g, const int a_b) { m_buttonColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setHoverColor (const int a_r, const int a_g, const int a_b) { m_hoverColor = Utils::createColor(a_r, a_g, a_b); }
        };

    }
}
