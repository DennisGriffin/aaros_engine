// File: Aaros_GUI_Listbox.hpp
//
// Presents a list of options onscreen.

#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include "Aaros_GUI_Scrollbar.hpp"
#include "Aaros_GUI_Button.hpp"
#include "Aaros_GUI_Widget.hpp"

namespace Aaros
{
    namespace Constants
    {

        const float         G_LISTBOX_BUTTON_HEIGHT         = 24.0f;
        const float         G_LISTBOX_BUTTON_WIDTH          = 160.0f;
        const unsigned int  G_LISTBOX_BUTTON_COUNT          = 3;
        const sf::Color     G_LISTBOX_BUTTON_COLOR          = { 255, 255, 255 };
        const sf::Color     G_LISTBOX_BUTTON_SELECTED_COLOR = { 0, 0, 222 };
        const sf::Color     G_LISTBOX_BUTTON_OUTLINE_COLOR  = { 0, 0, 0 };
        const sf::Color     G_LISTBOX_TEXT_COLOR            = { 0, 0, 0 };
        const sf::Color     G_LISTBOX_TEXT_SELECTED_COLOR   = { 255, 255, 255 };
        const unsigned int  G_LISTBOX_TEXT_FONT_SIZE        = 12;

    }

    namespace GUI
    {

        struct ListboxChoice
        {
            std::string         m_id;                   /** The string ID of the listbox choice. */
            std::string         m_name;                 /** The name shown in the listbox. */

            ListboxChoice (const std::string& a_id, const std::string& a_name) :
                m_id            { a_id },
                m_name          { a_name }
            {

            }

            ~ListboxChoice ()
            {
                m_id.clear();
                m_name.clear();
            }
        };

        class Listbox : public Widget
        {
        private /* Widget Members */:
            /** The list of buttons to be displayed onscreen. */
            std::vector<Button>         m_buttons;

            /** The scrollbar displayed at the right of the widget. */
            Scrollbar                   m_scrollbar { nullptr, "" };

        private /* Button Members */:
            unsigned int                m_buttonCount;          /** The number of buttons to be displayed onscreen. */
            float                       m_buttonWidth;          /** The width of the buttons. */
            sf::Color                   m_buttonFillColor;      /** The buttons' fill color. */
            sf::Color                   m_buttonFillFocus;      /** The buttons' focus fill color. */
            sf::Color                   m_buttonOutlineColor;   /** The buttons' outline color. */
            sf::Color                   m_buttonTextColor;      /** The buttons' text color. */
            sf::Color                   m_buttonTextFocus;      /** The buttons' focus text color. */
            std::string                 m_buttonTextFont;       /** The font used to render the buttons' text. */

        private /* Listbox Choice Members */:
            std::vector<ListboxChoice>  m_choices;              /** The container of choices to be presented. */
            unsigned int                m_firstChoiceIndex;     /** The index number of the first choice displayed. */
            unsigned int                m_selectedChoiceIndex;  /** The index number of the currently selected choice. */
            std::string                 m_selectedChoiceID;     /** The string ID of the currently selected choice. */

        private /* Methods */:
            void                        updateButtons (const sf::Time& a_deltaTime);

        private /* Setter Callbacks */:
            void                        onButtonWidthChanged ();
            void                        onButtonCountChanged ();
            void                        onChoiceCountChanged ();

        private /* Search Methods */:


        public /* Constructor and Destructor */:
            Listbox (Entity* ap_entity, const std::string& a_name);
            ~Listbox ();

        public /* Methods */:
            void                        loadFromXML (const pugi::xml_node &a_node) override;
            void                        update (const sf::Time &a_deltaTime) override;
            void                        render (sf::RenderWindow &a_window) override;

            void                        addChoice (const std::string& a_id, const std::string& a_name);
            void                        removeChoice (const unsigned int a_index);
            void                        popChoice ();
            void                        clearChoices ();


        public /* Getter-Setters */:
            inline unsigned int         getButtonCount () const { return m_buttonCount; }
            inline float                getButtonWidth () const { return m_buttonWidth; }

            inline void setButtonCount (const unsigned int a_count) { m_buttonCount = a_count; onButtonCountChanged(); }
            inline void setButtonWidth (const float a_width) { m_buttonWidth = std::abs(a_width); onButtonWidthChanged(); }
            inline void setButtonFontFile (const std::string& a_font) { m_buttonTextFont = a_font; onButtonCountChanged(); }
            inline void setFillColor (const int a_r, const int a_g, const int a_b) { m_buttonFillColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusFillColor (const int a_r, const int a_g, const int a_b) { m_buttonFillFocus = Utils::createColor(a_r, a_g, a_b); }
            inline void setOutlineColor (const int a_r, const int a_g, const int a_b) { m_buttonOutlineColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setTextColor (const int a_r, const int a_g, const int a_b) { m_buttonTextColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusTextColor (const int a_r, const int a_g, const int a_b) { m_buttonTextFocus = Utils::createColor(a_r, a_g, a_b); }
        };

    }
}
