// File: Aaros_System_Sound.cpp

#include "Aaros_System_Sound.hpp"

namespace Aaros
{

    bool SoundSystem::isEntityValid (Entity *ap_entity)
    {
        if (ap_entity->isEnabled() == false) { return false; }

        bool l_hasSound = ap_entity->hasComponent<SoundComponent>();

        if (l_hasSound == false)
            return false;

        if (ap_entity->getComponent<SoundComponent>().isEnabled() == false)
            return false;

        return true;
    }

    void SoundSystem::update (const Entity::Container &a_entities, const sf::Time &a_deltaTime, const unsigned int a_entityCount)
    {
        (void) a_deltaTime;

        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            SoundComponent& l_sound = l_ptr->getComponent<SoundComponent>();
            l_sound.updatePosition();
        }
    }

}
