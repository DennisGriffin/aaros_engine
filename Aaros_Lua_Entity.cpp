// File: Aaros_Lua_Entity.cpp

#include "Aaros_Lua_Entity.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeEntity (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<Entity> l_userdata
            {
                "Entity", l_ctors,

                "loadFromFile",             &Entity::loadFromFile,
                "getEntityInScene",         &Entity::getEntityInScene,
                "hasChild",                 &Entity::hasChildEntity,
                "getChild",                 &Entity::getChildEntity,
                "detachChild",              &Entity::detachChildEntity,
                "detachAllChildren",        &Entity::detachAllChildren,
                "setParent",                &Entity::setParentEntity,
                "getFullName",              &Entity::getFullName,
                "getTag",                   &Entity::getTag,
                "getScene",                 &Entity::getScene,
                "getSelfName",              &Entity::getSelfName,
                "getID",                    &Entity::getID,
                "getRunOrder",              &Entity::getOrder,
                "isAlive",                  &Entity::isAlive,
                "isEnabled",                &Entity::isEnabled,
                "isPersistent",             &Entity::isPersistent,
                "isRoot",                   &Entity::isRoot,
                "setTag",                   &Entity::setTag,
                "setOrder",                 &Entity::setOrder,
                "setAlive",                 &Entity::setAlive,
                "setEnabled",               &Entity::setEnabled,

                "hasRigidbody",             &Entity::hasComponent<RigidbodyComponent>,
                "hasCollider",              &Entity::hasComponent<ColliderComponent>,
                "hasScript",                &Entity::hasComponent<ScriptComponent>,
                "hasTimer",                 &Entity::hasComponent<TimerComponent>,
                "hasText",                  &Entity::hasComponent<TextComponent>,
                "hasSprite",                &Entity::hasComponent<SpriteComponent>,
                "hasWidgetHost",            &Entity::hasComponent<WidgetComponent>,
                "hasTilemap",               &Entity::hasComponent<TilemapComponent>,
                "hasEmitter",               &Entity::hasComponent<EmitterComponent>,
                "hasSoundHost",             &Entity::hasComponent<SoundComponent>,

                "getTransform",             &Entity::getComponent<TransformComponent>,
                "getRigidbody",             &Entity::getComponent<RigidbodyComponent>,
                "getCollider",              &Entity::getComponent<ColliderComponent>,
                "getScript",                &Entity::getComponent<ScriptComponent>,
                "getTimer",                 &Entity::getComponent<TimerComponent>,
                "getText",                  &Entity::getComponent<TextComponent>,
                "getSprite",                &Entity::getComponent<SpriteComponent>,
                "getWidgetHost",            &Entity::getComponent<WidgetComponent>,
                "getTilemap",               &Entity::getComponent<TilemapComponent>,
                "getEmitter",               &Entity::getComponent<EmitterComponent>,
                "getSoundHost",             &Entity::getComponent<SoundComponent>
            };
            a_state.set_userdata(l_userdata);
        }

    }
}
