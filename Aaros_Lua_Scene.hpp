// File: Aaros_Lua_Scene.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeScene (sol::state& a_state);

    }
}
