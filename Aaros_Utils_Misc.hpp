// File: Aaros_Utils_Misc.hpp

#pragma once

#include <SFML/Graphics/Color.hpp>
#include "Aaros_Utils_Math.hpp"

namespace Aaros
{
    namespace Utils
    {

        inline sf::Color createColor (const int a_red,
                                      const int a_green,
                                      const int a_blue,
                                      const int a_alpha = 255)
        {
            int l_red       = Math::clampInteger(a_red, 0, 255);
            int l_green     = Math::clampInteger(a_green, 0, 255);
            int l_blue      = Math::clampInteger(a_blue, 0, 255);
            int l_alpha     = Math::clampInteger(a_alpha, 0, 255);

            return {
                static_cast<sf::Uint8>(l_red),
                static_cast<sf::Uint8>(l_green),
                static_cast<sf::Uint8>(l_blue),
                static_cast<sf::Uint8>(l_alpha)
            };
        }

    }
}
