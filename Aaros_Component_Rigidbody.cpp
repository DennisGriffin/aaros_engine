// File: Aaros_Component_Rigidbody.cpp

#include "Aaros_Component_Rigidbody.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    void RigidbodyComponent::onMassChanged()
    {
        // Adjust the rigidbody's inverse mass.
        if (m_mass == 0.0f) { m_inverseMass = 0.0f; }
        else                { m_inverseMass = 1.0f / m_mass; }
    }

    RigidbodyComponent::RigidbodyComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_velocity              { 0.0f, 0.0f },
        m_force                 { 0.0f, 0.0f },
        m_mass                  { 0.0f },
        m_inverseMass           { 0.0f },
        m_drag                  { 0.0f },
        m_gravityScale          { 1.0f },
        m_kinematic             { false }
    {

    }

    RigidbodyComponent::~RigidbodyComponent ()
    {
        m_velocity              = { 0.0f, 0.0f };
        m_force                 = { 0.0f, 0.0f };
        m_mass                  = 0.0f;
        m_inverseMass           = 0.0f;
        m_drag                  = 0.0f;
        m_gravityScale          = 1.0f;
        m_kinematic             = false;

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void RigidbodyComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_velocityNode = a_node.child("Velocity");
        xml_node l_physicsNode = a_node.child("Physics");

        float l_vx = l_velocityNode.attribute("X").as_float();
        float l_vy = l_velocityNode.attribute("Y").as_float();
        float l_mass = l_physicsNode.attribute("Mass").as_float();
        float l_drag = l_physicsNode.attribute("Drag").as_float();
        float l_gravityScale = l_physicsNode.attribute("GravityScale").as_float(1.0f);
        float l_kinematic = l_physicsNode.attribute("Kinematic").as_bool();

        m_velocity = { l_vx, l_vy };
        m_mass = std::abs(l_mass);
        m_drag = std::abs(l_drag);
        m_gravityScale = Math::clampNumber(l_gravityScale, 0.0f, 1.0f);
        m_kinematic = l_kinematic;

        onMassChanged();
    }

    void RigidbodyComponent::applyForce (const float a_x, const float a_y)
    {
        m_force.x += a_x;
        m_force.y += a_y;
    }

    void RigidbodyComponent::applyForceVector (const sf::Vector2f &a_force)
    {
        m_force += a_force;
    }

    void RigidbodyComponent::applyImpulseVector (const sf::Vector2f &a_impulse)
    {
        m_velocity += (m_inverseMass * a_impulse);
    }

    void RigidbodyComponent::applyVelocityVector (const sf::Vector2f &a_vector)
    {
        m_velocity += a_vector;
    }

    void RigidbodyComponent::applyDrag (const float a_time)
    {
        m_velocity *= Math::clampNumber(1.0f - m_drag * a_time, 0.0f, 1.0f);

        if (std::abs(m_velocity.x) < 0.03f) { m_velocity.x = 0.0f; }
        if (std::abs(m_velocity.y) < 0.03f) { m_velocity.y = 0.0f; }
    }

}
