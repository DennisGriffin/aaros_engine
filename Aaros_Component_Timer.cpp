// File: Aaros_Component_Timer.cpp

#include "Aaros_Component_Timer.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    TimeEvent::Iterator TimerComponent::findTimeEvent (const std::string &a_id)
    {
        return std::find_if(m_events.begin(), m_events.end(),
                            [&a_id] (const TimeEvent& a_event)
        {
            return a_id == a_event.m_id;
        });
    }

    TimerComponent::TimerComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component           { ap_entity, a_entityName, a_entityID }
    {

    }

    TimerComponent::~TimerComponent ()
    {
        m_events.clear();

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void TimerComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        for (const xml_node& l_node : a_node.children("Timer"))
        {
            std::string l_id = l_node.attribute("ID").as_string();
            float l_interval = l_node.attribute("Interval").as_float(Constants::G_TIMER_INTERVAL.asSeconds());
            bool l_enabled = l_node.attribute("Enabled").as_bool(true);

            if (l_id.empty() == false)
            {
                m_events.emplace_back(l_id, l_interval, l_enabled);
            }
        }
    }

    void TimerComponent::setTimeEventInterval (const std::string &a_id, const float a_interval)
    {
        auto l_find = findTimeEvent(a_id);
        if (l_find != m_events.end())
        {
            l_find->m_interval = sf::seconds(a_interval);
            l_find->m_elapsed = sf::Time::Zero;
            return;
        }

        m_events.emplace_back(a_id, a_interval, true);
    }

    void TimerComponent::toggleTimeEvent (const std::string &a_id, const bool a_enabled)
    {
        auto l_find = findTimeEvent(a_id);
        if (l_find != m_events.end())
        {
            l_find->m_enabled = a_enabled;
            l_find->m_elapsed = sf::Time::Zero;
            return;
        }

        m_events.emplace_back(a_id, Constants::G_TIMER_INTERVAL.asSeconds(), a_enabled);
    }

    void TimerComponent::resetTimer (const std::string &a_id)
    {
        auto l_find = findTimeEvent(a_id);
        if (l_find != m_events.end())
        {
            l_find->m_elapsed = sf::Time::Zero;
        }
    }

    void TimerComponent::resetAllTimers ()
    {
        unsigned int l_eventCount = m_events.size();
        for (unsigned int i = 0; i < l_eventCount; ++i)
        {
            auto& l_event = m_events.at(i);
            l_event.m_elapsed = sf::Time::Zero;
        }
    }

    void TimerComponent::update (const sf::Time &a_timestep)
    {
        unsigned int l_eventCount = m_events.size();
        for (unsigned int i = 0; i < l_eventCount; ++i)
        {
            auto& l_event = m_events.at(i);

            if (l_event.m_enabled == false) { continue; }

            l_event.m_elapsed += a_timestep;
            while (l_event.m_elapsed >= l_event.m_interval)
            {
                l_event.m_elapsed -= l_event.m_interval;

                mp_entity->callLuaFunction("onTimerTick", l_event.m_id);
            }
        }
    }

}
