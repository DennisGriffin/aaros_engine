// File: Aaros_GUI_Textarea.hpp
//
// Presents an area of wrapped text on the screen.

#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include "Aaros_GUI_Widget.hpp"

namespace Aaros
{
    namespace Constants
    {

        const float             G_TEXTAREA_OUTLINE_THICKNESS        = 2.0f;
        const unsigned int      G_TEXTAREA_FONT_SIZE                = 14;
        const sf::Vector2f      G_TEXTAREA_SIZE                     = { 192.0f, 128.0f };
        const sf::Time          G_TEXTAREA_TYPEWRITER_INTERVAL      = sf::seconds(0.1f);
        const sf::Color         G_TEXTAREA_RECT_FILL_COLOR          = { 222, 222, 222 };
        const sf::Color         G_TEXTAREA_RECT_OUTLINE_COLOR       = { 33, 33, 33 };
        const sf::Color         G_TEXTAREA_TEXT_COLOR               = { 222, 222, 222 };

    }

    namespace GUI
    {

        /**
         * This GUI widget presents an area of text on the screen, wrapped
         * to a given size. The user can elect to present this text in a typewriter fashion.
         */
        class Textarea : public Widget
        {
        private /* Text Members */:
            std::string         m_text;                 /** The textarea's base text. */
            std::string         m_textWrapped;          /** The textarea's text, wrapped into its bounds. */
            std::string         m_textFontID;           /** The ID or filename of the font to be used to render the text. */
            unsigned int        m_textFontSize;         /** The font's size. */
            sf::Vector2f        m_textWrapSize;         /** The size of the bounds into which the text will be wrapped. */
            sf::Text            m_textObject;           /** The text object that will be presented on screen. */

        private /* Rectangle Members */:
            bool                m_rectVisible;          /** Should we render the rectangle behind the text? */
            sf::RectangleShape  m_rectObject;           /** The rectangle object that can be rendered behind the text. */
            sf::Vector2f        m_rectSize;             /** The size of the rectangle. */

        private /* Typewriter Effect Members */:
            bool                m_typewriterActive;     /** Should each character of the textarea be "typed" over an interval of time? */
            bool                m_typewriterFinished;   /** Have all characters in the textarea been "typed"? */
            std::string         m_typewriterText;       /** How much of the textarea's text was "typed" onscreen? */
            unsigned int        m_typewriterPosition;   /** Represents the next character to be "typed". */
            sf::Time            m_typewriterTime;       /** Keeps track of when we should "type" the next character. */
            sf::Time            m_typewriterInterval;   /** How often do we "type" the characters in our textarea? */


        private /* Inherited Methods */:
            void                updateWidgetInternals () override;

        private /* Other Methods */:
            /** Updates the typewriter effect for the textarea, if enabled. */
            void                updateTypewriterEffect (const sf::Time& a_deltaTime);

            /** Resets the typewriter effect when its text is changed. */
            void                resetTypewriterEffect ();

        public /* Constructor and Destructor */:
            Textarea (Entity* ap_entity,
                      const std::string& a_name);
            ~Textarea ();

        public /* Inherited Methods */:
            void                loadFromXML (const pugi::xml_node &a_node) override;
            void                update (const sf::Time &a_deltaTime) override;
            void                render (sf::RenderWindow &a_window) override;

        public /* Inline Getter-Setters */:
            inline std::string  getText () const { return m_text; }
            inline std::string  getFont () const { return m_textFontID; }
            inline unsigned int getFontSize () const { return m_textFontSize; }
            inline float        getWidth () const { return m_rectSize.x; }
            inline float        getHeight () const { return m_rectSize.y; }
            inline bool         isRectVisible () const { return m_rectVisible; }
            inline bool         isTypewriterActive () const { return m_typewriterActive; }
            inline bool         isTypewriterFinished () const { return m_typewriterFinished; }
            inline unsigned int getTypewriterPosition () const { return m_typewriterPosition; }
            inline float        getTypewriterInterval () const { return m_typewriterInterval.asSeconds(); }

            inline void setText (const std::string& a_text) { m_text = a_text; resetTypewriterEffect(); }
            inline void setFont (const std::string& a_font) { m_textFontID = setTextObjectFont(m_textObject, a_font); }
            inline void setFontSize (const unsigned int a_size) { m_textFontSize = a_size; }
            inline void setSize (const float a_x, const float a_y) { m_rectSize = { std::abs(a_x), std::abs(a_y) }; }
            inline void setRectVisible (const bool a_visible) { m_rectVisible = a_visible; }
            inline void setTypewriterActive (const bool a_active) { m_typewriterActive = a_active; resetTypewriterEffect(); }
            inline void setTypewriterInterval (const float a_interval) { m_typewriterInterval = sf::seconds(std::abs(a_interval)); }
            inline void setFillColor (const int a_r, const int a_g, const int a_b) { m_rectObject.setFillColor(Utils::createColor(a_r, a_g, a_b)); }
            inline void setOutlineColor (const int a_r, const int a_g, const int a_b) { m_rectObject.setFillColor(Utils::createColor(a_r, a_g, a_b)); }
            inline void setTextColor (const int a_r, const int a_g, const int a_b) { m_textObject.setColor(Utils::createColor(a_r, a_g, a_b)); }

        };

    }
}
