// File: Aaros_Component_Sprite.cpp

#include "Aaros_Component_Sprite.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    SpriteComponent::SpriteComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_shader                { &m_sprite }
    {

    }

    SpriteComponent::~SpriteComponent ()
    {

    }

    void SpriteComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_imageNode = a_node.child("Image");
        std::string l_imageFile = l_imageNode.attribute("Filename").as_string();

        if (!l_imageFile.empty())
        {
            TextureManager& TM = TextureManager::getInstance();

            TM.load(l_imageFile);
            m_sprite.setTexture(TM.get(l_imageFile));
        }

        if (sf::Shader::isAvailable())
        {
            xml_node l_shaderNode = a_node.child("Shader");

            if (l_shaderNode.type() != node_null)
                m_shader.loadFromXML(l_shaderNode);
        }
    }

}
