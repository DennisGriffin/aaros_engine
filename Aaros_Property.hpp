// File: Aaros_Property.hpp
//
// This class consists of a key-value piece of data.

#pragma once

#include <string>
#include <vector>
#include <memory>
#include <algorithm>

namespace Aaros
{

    /**
     * This class represents a key-value piece of data that can be
     * accessed and manipulated.
     */
    class Property
    {
    public /* Typedefs */:
        using Ptr           = std::unique_ptr<Property>;
        using Container     = std::vector<Ptr>;
        using Iterator      = Container::iterator;

    private /* Members */:
        std::string         m_key;          /** The property's identifying key. */
        std::string         m_value;        /** The property's string value. */
        bool                m_save;         /** Should the property manager write this property to a file? */

    public /* Constructor and Destructor */:
        /**
         * The default constructor creates the property with the given
         * string key and value.
         *
         * @param   a_key               The property's string key.
         * @param   a_value             The property's string value.
         * @param   a_save              The property's save flag.
         */
        Property (const std::string& a_key,
                  const std::string& a_value,
                  const bool a_save);
        ~Property ();

    public /* Methods */:
        /**
         * Gets the string value of the property. A default value can be given to return
         * in case of problems, if the user so desires.
         *
         * @param   a_default           A default value to return in case of problems.
         *
         * @return  The string value of the property, or the failsafe value given.
         */
        std::string         get (const std::string& a_default = "") const;

        /**
         * Gets the value of the property, converted into the given numeric or boolean
         * template parameter. A default value can be given to return in case of problems, if
         * the user so desires.
         *
         * @param   a_default           A default value to return in case of problems.
         *
         * @return  The casted value of the property, or the failsafe value given.
         */
        template <typename Type>
        Type                getAs (const Type a_default = 0) const;

        /**
         * Sets the string value of the property.
         *
         * @param   a_value             The property's new string value.
         */
        void                set (const std::string& a_value);

        /**
         * Sets the value of the property to the given numeric or boolean template value,
         * converted to a string.
         *
         * @param   a_value             The property's new value.
         */
        template <typename Type>
        void                setAs (const Type a_value);

    public /* Getter-Setters */:
        inline std::string  getKey ()       const { return m_key; }
        inline bool         isSavable ()    const { return m_save; }

        inline void         setSavable (const bool a_savable) { m_save = a_savable; }
    };

}
