// File: Aaros_Lua_Widgets.cpp

#include "Aaros_Lua_Widgets.hpp"
#include "Aaros_GUI_Textarea.hpp"
#include "Aaros_GUI_Button.hpp"
#include "Aaros_GUI_Scrollbar.hpp"
#include "Aaros_GUI_Progressbar.hpp"
#include "Aaros_GUI_Listbox.hpp"
#include "Aaros_GUI_Checkbox.hpp"

#define EXPOSE_BASE_WIDGET_METHODS(a_widget) \
    "getName",          &a_widget::getName, \
    "getPositionX",     &a_widget::getPositionX, \
    "getPositionY",     &a_widget::getPositionY, \
    "isEnabled",        &a_widget::isEnabled, \
    "isVisible",        &a_widget::isVisible, \
    "isFocusable",      &a_widget::isFocusable, \
    "isFocused",        &a_widget::isFocused, \
    "getFocusIndex",    &a_widget::getFocusIndex, \
    "setPosition",      &a_widget::setPosition, \
    "setEnabled",       &a_widget::setEnabled, \
    "setVisible",       &a_widget::setVisible, \
    "setFocusIndex",    &a_widget::setFocusIndex

namespace Aaros
{
    namespace Lua
    {

        static void exposeTextareaWidget (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<GUI::Textarea> l_userdata
            {
                "Textarea",                 l_ctors,

                "getText",                  &GUI::Textarea::getText,
                "getFont",                  &GUI::Textarea::getFont,
                "getFontSize",              &GUI::Textarea::getFontSize,
                "getWidth",                 &GUI::Textarea::getWidth,
                "getHeight",                &GUI::Textarea::getHeight,
                "isRectVisible",            &GUI::Textarea::isRectVisible,
                "isTypewriterActive",       &GUI::Textarea::isTypewriterActive,
                "isTypewriterFinished",     &GUI::Textarea::isTypewriterFinished,
                "getTypewriterPosition",    &GUI::Textarea::getTypewriterPosition,
                "getTypewriterInterval",    &GUI::Textarea::getTypewriterInterval,

                "setText",                  &GUI::Textarea::setText,
                "setFont",                  &GUI::Textarea::setFont,
                "setFontSize",              &GUI::Textarea::setFontSize,
                "setSize",                  &GUI::Textarea::setSize,
                "setRectVisible",           &GUI::Textarea::setRectVisible,
                "setTypewriterActive",      &GUI::Textarea::setTypewriterActive,
                "setTypewriterInterval",    &GUI::Textarea::setTypewriterInterval,
                "setFillColor",             &GUI::Textarea::setFillColor,
                "setOutlineColor",          &GUI::Textarea::setOutlineColor,
                "setTextColor",             &GUI::Textarea::setTextColor,

                EXPOSE_BASE_WIDGET_METHODS(GUI::Textarea)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeButtonWidget (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<GUI::Button> l_userdata
            {
                "Button",                   l_ctors,

                "getText",                  &GUI::Button::getText,
                "getFont",                  &GUI::Button::getFont,
                "getFontSize",              &GUI::Button::getFontSize,
                "getWidth",                 &GUI::Button::getWidth,
                "getHeight",                &GUI::Button::getHeight,
                "isRectVisible",            &GUI::Button::isRectVisible,

                "setText",                  &GUI::Button::setText,
                "setFont",                  &GUI::Button::setFont,
                "setFontSize",              &GUI::Button::setFontSize,
                "setSize",                  &GUI::Button::setSize,
                "setRectVisible",           &GUI::Button::setRectVisible,
                "setFillColor",             &GUI::Button::setFillColor,
                "setOutlineColor",          &GUI::Button::setOutlineColor,
                "setTextColor",             &GUI::Button::setTextColor,
                "setFocusFillColor",        &GUI::Button::setFocusFillColor,
                "setFocusOutlineColor",     &GUI::Button::setFocusOutlineColor,
                "setFocusTextColor",        &GUI::Button::setFocusTextColor,

                EXPOSE_BASE_WIDGET_METHODS(GUI::Button)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeScrollbarWidget (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<GUI::Scrollbar> l_userdata
            {
                "Scrollbar", l_ctors,

                "setValue",             &GUI::Scrollbar::setValue,
                "setMaximum",           &GUI::Scrollbar::setMaximum,
                "getLongSize",          &GUI::Scrollbar::getLongSize,
                "getShortSize",         &GUI::Scrollbar::getShortSize,
                "getValue",             &GUI::Scrollbar::getValue,
                "getMaximum",           &GUI::Scrollbar::getMaximum,
                "setBodyColor",         &GUI::Scrollbar::setBodyColor,
                "setButtonColor",       &GUI::Scrollbar::setButtonColor,
                "setHoverColor",        &GUI::Scrollbar::setHoverColor,

                EXPOSE_BASE_WIDGET_METHODS(GUI::Scrollbar)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeProgressbarWidget (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<GUI::Progressbar> l_userdata
            {
                "Progressbar", l_ctors,

                "setValue",             &GUI::Progressbar::setValue,
                "setMaximum",           &GUI::Progressbar::setMaximum,
                "getWidth",             &GUI::Progressbar::getWidth,
                "getHeight",            &GUI::Progressbar::getHeight,
                "getPercent",           &GUI::Progressbar::getPercent,
                "isEmpty",              &GUI::Progressbar::isEmpty,
                "isFull",               &GUI::Progressbar::isFull,

                "setSize",              &GUI::Progressbar::setSize,
                "setBackOutlineColor",  &GUI::Progressbar::setBackBorderColor,
                "setBackFillColor",     &GUI::Progressbar::setBackFillColor,
                "setFillColor",         &GUI::Progressbar::setFillColor,

                EXPOSE_BASE_WIDGET_METHODS(GUI::Progressbar)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeListboxWidget (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<GUI::Listbox> l_userdata
            {
                "Listbox", l_ctors,

                "addChoice",            &GUI::Listbox::addChoice,
                "removeChoice",         &GUI::Listbox::removeChoice,
                "popChoice",            &GUI::Listbox::popChoice,
                "clearChoices",         &GUI::Listbox::clearChoices,

                "getButtonCount",       &GUI::Listbox::getButtonCount,
                "getButtonWidth",       &GUI::Listbox::getButtonWidth,
                "setButtonCount",       &GUI::Listbox::setButtonCount,
                "setButtonWidth",       &GUI::Listbox::setButtonWidth,
                "setButtonFontFile",    &GUI::Listbox::setButtonFontFile,
                "setFillColor",         &GUI::Listbox::setFillColor,
                "setFocusFillColor",    &GUI::Listbox::setFocusFillColor,
                "setOutlineColor",      &GUI::Listbox::setOutlineColor,
                "setTextColor",         &GUI::Listbox::setTextColor,
                "setFocusTextColor",    &GUI::Listbox::setFocusTextColor,

                EXPOSE_BASE_WIDGET_METHODS(GUI::Listbox)
            };
            a_state.set_userdata(l_userdata);
        }

        static void exposeCheckboxWidget (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<GUI::Checkbox> l_userdata
            {
                "Checkbox", l_ctors,

                "isSelected",           &GUI::Checkbox::isSelected,
                "setFillColor",         &GUI::Checkbox::setFillColor,
                "setFocusFillColor",    &GUI::Checkbox::setFocusFillColor,
                "setOutlineColor",      &GUI::Checkbox::setOutlineColor,
                "setFocusOutlineColor", &GUI::Checkbox::setFocusOutlineColor,

                EXPOSE_BASE_WIDGET_METHODS(GUI::Checkbox)
            };
            a_state.set_userdata(l_userdata);
        }

        void exposeWidgets (sol::state& a_state)
        {
            exposeTextareaWidget(a_state);
            exposeButtonWidget(a_state);
            exposeScrollbarWidget(a_state);
            exposeProgressbarWidget(a_state);
            exposeListboxWidget(a_state);
            exposeCheckboxWidget(a_state);
        }

    }
}
