// File: Aaros_System_Physics.hpp
//
// Represents the physics world in our ECS.

#pragma once

#include "Aaros_System.hpp"
#include "Aaros_Utils_Math.hpp"

namespace Aaros
{

    /**
     * This is the physics representation of a given entity, containing quick access to
     * the entity's transform, rigidbody, and collider components.
     */
    struct PhysicsObject
    {
        TransformComponent* mp_transform;       /** The entity's transform component. Guranteed valid. */
        RigidbodyComponent* mp_rigidbody;       /** The entity's rigidbody component. */
        ColliderComponent*  mp_collider;        /** The entity's collider component */

        PhysicsObject (Entity* ap_entity) :
            mp_transform    { ap_entity->getTransform() },
            mp_rigidbody    { nullptr },
            mp_collider     { nullptr }
        {

        }

        ~PhysicsObject ()
        {
            mp_transform    = nullptr;
            mp_rigidbody    = nullptr;
            mp_collider     = nullptr;
        }

        inline bool         isValid () const { return (mp_rigidbody != nullptr || mp_collider != nullptr); }
    };

    /**
     * Represents a profile of how two physics objects may be colliding, if they are.
     */
    struct PhysicsContact
    {
        PhysicsObject*      mp_one;             /** The first object in the contact. */
        PhysicsObject*      mp_two;             /** The second object in the contact. */
        sf::Vector2f        m_normal;           /** Which two faces of the colliding AABBs are colliding? */
        float               m_minPenetration;   /** The axis of least penetration. */
        float               m_maxPenetration;   /** The axis of greatest penetration. */
        float               m_minRestitution;   /** The minimum restitution of the colliders. */

        PhysicsContact (PhysicsObject* ap_one, PhysicsObject* ap_two) :
            mp_one              { ap_one },
            mp_two              { ap_two },
            m_normal            { 0.0f, 0.0f },
            m_minPenetration    { 0.0f },
            m_maxPenetration    { 0.0f },
            m_minRestitution    { 0.0f }
        {
            m_minRestitution =
                std::min(mp_one->mp_collider->getRestitution(),
                         mp_two->mp_collider->getRestitution());
        }

        ~PhysicsContact ()
        {
            mp_one = nullptr;
            mp_two = nullptr;
            m_normal = { 0.0f, 0.0f };
            m_minPenetration = 0.0f;
            m_maxPenetration = 0.0f;
            m_minRestitution = 0.0f;
        }
    };

    class PhysicsSystem : public System
    {
    private /* Members */:
        std::vector<PhysicsObject>  m_objects;                  /** The list of validated physics objects. */
        std::vector<PhysicsContact> m_contacts;                 /** The list of registered collisions. */
        sf::Vector2f                m_gravity = { 0.0f, 0.0f }; /** The force of gravity acting upon the physics objects. */
        int                         m_velocityIters = 6;        /** The number of times per frame that impulse resolution is done. */
        int                         m_positionIters = 2;        /** The number of times per frame that position correction is done. */

    private /* Methods */:
        bool                    isEntityValid (Entity *ap_entity) override;
        void                    integrateForce (PhysicsObject& a_object,
                                                const float a_time);
        void                    integrateVelocity (PhysicsObject& a_object,
                                                   const float a_time);
        void                    clearForce (PhysicsObject& a_object);
        bool                    checkCollision (PhysicsObject& a_one,
                                                PhysicsObject& a_two,
                                                const bool a_store = true);
        void                    resolveCollision (PhysicsContact& a_contact);
        void                    correctPosition (PhysicsContact& a_contact);

    public /* Methods */:
        void                    fixedUpdate (const Entity::Container &a_entities,
                                             const sf::Time &a_timestep,
                                             const unsigned int a_entityCount) override;

    public /* Getter-Setters */:
        inline sf::Vector2f     getGravity () const { return m_gravity; }
        inline int              getVelocityIterCount () const { return m_velocityIters; }
        inline int              getPositionIterCount () const { return m_positionIters; }

        inline void setGravity (const float a_x, const float a_y) { m_gravity = { a_x, a_y }; }
        inline void setVelocityIterCount (const int a_iters) { m_velocityIters = std::abs(a_iters); }
        inline void setPositionIterCount (const int a_iters) { m_positionIters = std::abs(a_iters); }
    };

}
