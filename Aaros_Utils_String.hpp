// File: Aaros_Utils_String.hpp
//
// Contains some useful string functions.

#pragma once

#include <string>
#include <vector>

namespace Aaros
{
    namespace Utils
    {

        /**
         * Enumerates the sides of a string. Used by the 'trimString' function.
         */
        enum class StringSide
        {
            Start,
            End,
            Both
        };

        /**
         * This function trims whitespace from one or both sides of the given
         * string. A certain character can also be specified for trimming,
         * if the user so desires.
         *
         * @param   a_string            The string to be trimmed.
         * @param   a_side              Which side of the string will be trimmed?
         * @param   a_character         Do you wish to trim a certain character from the string?
         *
         * @return  The trimmed string.
         */
        inline std::string trimString (const std::string& a_string,
                                       const StringSide a_side,
                                       const char a_character = ' ')
        {
            // Local variable setup.
            std::string l_return = a_string;

            // Trim from one or both sides of the string, depending on which side(s) the
            // user requested.
            if (a_side == StringSide::Start || a_side == StringSide::Both)
            {
                while (l_return.front() == a_character)
                    l_return.erase(0, 1);
            }
            if (a_side == StringSide::End || a_side == StringSide::Both)
            {
                while (l_return.back() == a_character)
                    l_return.erase(l_return.length() - 1);
            }

            return l_return;
        }


        inline std::vector<std::string> splitString (const std::string& a_string,
                                                     const char a_separator,
                                                     const char a_escape = '\\')
        {
            // Local variable setup.
            std::vector<std::string>    l_return;
            std::string                 l_temp = "";
            bool                        l_foundEscape = false;
            char                        l_previous = 0;

            // Iterate through each character in the given string.
            for (const char l_char : a_string)
            {
                // Did we find the separator character?
                if (l_char == a_separator)
                {
                    // Was the previous character the escape character?
                    // If so, then add this character to the string and move on.
                    if (l_foundEscape == true)
                    {
                        l_temp += l_char;
                        l_foundEscape = false;
                    }
                    else
                    {
                        // If the temp string is not empty, trim it and move it into the vector.
                        if (!l_temp.empty())
                        {
                            l_temp = trimString(l_temp, StringSide::Both);
                            l_return.push_back(std::move(l_temp));
                        }
                    }
                }
                else
                {
                    // Was the previous character the escape character?
                    if (l_foundEscape == true)
                    {
                        // If so, then just add that character and flip the switch off.
                        l_temp += l_previous;
                        l_foundEscape = false;
                    }

                    // If this character is the escape character, then flip that switch on
                    // and move on to the next character.
                    if (l_char == a_escape)
                    {
                        l_foundEscape = true;
                    }
                    else
                    {
                        l_temp += l_char;
                    }
                }

                l_previous = l_char;
            }

            // If the temporary string is not empty, trim it and move it into the vector.
            if (!l_temp.empty())
            {
                l_temp = trimString(l_temp, StringSide::Both);
                l_return.push_back(std::move(l_temp));
            }

            return l_return;
        }

    }
}
