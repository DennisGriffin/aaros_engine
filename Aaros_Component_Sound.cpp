// File: Aaros_Component_Sound.cpp

#include "Aaros_Component_Sound.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    SoundComponent::SoundComponent(Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_currentSound          { "" },
        m_minimumPitch          { 1.0f },
        m_maximumPitch          { 1.0f }
    {

    }

    SoundComponent::~SoundComponent()
    {
        m_currentSound.clear();
        m_minimumPitch          = 0.0f;
        m_maximumPitch          = 0.0f;

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void SoundComponent::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_detailsNode = a_node.child("Details");
        xml_node l_soundNode = a_node.child("Sounds");

        float l_volume = l_detailsNode.attribute("Volume").as_float(100.0f);
        float l_minDistance = l_detailsNode.attribute("MinDistance").as_float(1.0f);
        float l_attenuation = l_detailsNode.attribute("Attenuation").as_float(1.0f);
        float l_minPitch = l_detailsNode.attribute("MinPitch").as_float(1.0f);
        float l_maxPitch = l_detailsNode.attribute("MaxPitch").as_float(1.0f);
        bool l_looping = l_detailsNode.attribute("Looping").as_bool();

        setVolume(l_volume);
        setMinimumDistance(l_minDistance);
        setAttenuation(l_attenuation);
        setMinimumPitch(l_minPitch);
        setMaximumPitch(l_maxPitch);
        setLooping(l_looping);

        for (const xml_node& l_node : l_soundNode.children("Sound"))
        {
            std::string l_file = l_node.attribute("File").as_string();

            if (!l_file.empty())
            {
                loadSound(l_file);
            }
        }
    }

    void SoundComponent::loadSound(const std::string &a_soundFile)
    {
        SoundManager& SM = SoundManager::getInstance();

        SM.load(a_soundFile);
    }

    void SoundComponent::playSound(const std::string &a_soundFile)
    {
        if (a_soundFile.empty() == false && a_soundFile != m_currentSound)
        {
            SoundManager& SM = SoundManager::getInstance();

            m_sound.setBuffer(SM.get(a_soundFile));
            m_sound.setPitch(Math::randomFloat(m_minimumPitch, m_maximumPitch));
            m_currentSound = a_soundFile;

            m_sound.play();
        }
    }

    void SoundComponent::pauseSound()
    {
        m_sound.pause();
    }

    void SoundComponent::stopSound()
    {
        m_sound.stop();
    }

    void SoundComponent::updatePosition()
    {
        sf::Vector2f l_center = mp_entity->getTransform()->getPixelCenter();

        m_sound.setPosition(l_center.x, l_center.y, 0.0f);
    }

    void SoundComponent::setMinimumPitch(const float a_pitch)
    {
        m_minimumPitch = Math::clampNumber(a_pitch, 0.1f, m_maximumPitch);
    }

    void SoundComponent::setMaximumPitch(const float a_pitch)
    {
        m_maximumPitch = std::abs(a_pitch);

        if (m_maximumPitch < 0.1f) { m_maximumPitch = 0.1f; }
    }

}
