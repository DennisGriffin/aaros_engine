// File: Aaros_Jukebox.cpp

#include <iostream>
#include "Aaros_Jukebox.hpp"

namespace Aaros
{

    void Jukebox::loadSong (const std::string &a_songFile)
    {
        if (m_musicLoaded == true) { m_currentSong.stop(); }

        m_musicLoaded = m_currentSong.openFromFile(a_songFile);
        m_currentSong.setRelativeToListener(true);
    }

    void Jukebox::playSong()
    {
        if (m_musicLoaded == true)
        {
            m_currentSong.play();
        }
    }

    void Jukebox::pauseSong()
    {
        if (m_musicLoaded == true)
            m_currentSong.pause();
    }

    void Jukebox::stopSong()
    {
        if (m_musicLoaded == true)
        {
            m_currentSong.stop();
        }
    }

}
