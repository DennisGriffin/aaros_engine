// File: Aaros.AssetManager.hpp
//
// A templated singleton class that serves as an interface for loading
// and accessing engine assets.

#pragma once

#include <string>
#include <map>
#include <memory>
#include <typeinfo>
#include <stdexcept>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Shader.hpp>
#include "Aaros_PropertyManager.hpp"
#include "Aaros_Utils_Singleton.hpp"

namespace Aaros
{

    /**
     * This class is responsible for loading and maintaining the engine's
     * SFML-based assets.
     */
    template <typename Asset>
    class AssetManager : public Utils::Singleton<AssetManager<Asset>>
    {
    private /* Friend Classes */:
        friend class Utils::Singleton<AssetManager<Asset>>;

    private /* Members */:
        std::map<std::string, std::unique_ptr<Asset>> m_assets;

    public /* Methods */:
        /**
         * Loads an asset from the file specified.
         *
         * @param   a_filename          The name of the asset file.
         * @param   a_id                A special string ID for engine-critical assets.
         */
        inline void load (const std::string& a_filename,
                          const std::string& a_id = "")
        {
            // Make sure the type given is not an sf::Shader. If so, a compile error is raised.
            static_assert(std::is_same<Asset, sf::Shader>::value == false,
                          "[AssetManager::load] sf::Shader requires two parameters for loading. Use 'loadSpecial' instead.");

            // Get the typename for the class template parameter.
            std::string l_classType = typeid(Asset).name();

            // Check to see if an asset file was provided.
            if (a_filename.empty())
            {
                PropertyManager& PM = PropertyManager::getInstance();
                PM.append("errorAssetType", false).set(l_classType);

                throw std::runtime_error
                {
                    "[AssetManager::load] No asset file specified!"
                };
            }

            // Check to see if the asset file was already loaded, or if an asset was
            // already bound to the given ID, if one was provided.
            if ((a_id.empty() == false && m_assets.find(a_id) != m_assets.end()) ||
                    m_assets.find(a_filename) != m_assets.end())
            {
                return;
            }

            // Now attempt to load the asset.
            std::unique_ptr<Asset> l_asset = std::make_unique<Asset>();
            if (!l_asset->loadFromFile(a_filename))
            {
                PropertyManager& PM = PropertyManager::getInstance();
                PM.append("errorAssetFile", false).set(a_filename);
                PM.append("errorAssetType", false).set(l_classType);

                throw std::runtime_error
                {
                    "[AssetManager::load] Failed to load asset: '" + a_filename + "'!"
                };
            }

            // Map the asset.
            if (a_id.empty())
            {
                m_assets[a_filename] = std::move(l_asset);
                std::cout << "[AssetManager::load] Loaded asset: '" << a_filename << "'." << std::endl;
            }
            else
            {
                m_assets[a_id] = std::move(l_asset);
                std::cout << "[AssetManager::load] Loaded asset: '" << a_filename
                          << "' (ID: " << a_id << ")." << std::endl;
            }
        }

        /**
         * Loads an asset from the file specified. This method is for assets
         * that allow for or require an extra parameter to be considered for loading.
         *
         * If you wish to load shaders, you must use this method because those assets
         * require two parameters of some type to load.
         *
         * @param   a_filename          The name of the asset file.
         * @param   a_extra             The extra parameter allowed/required by the asset.
         * @param   a_id                A special string ID for engine-critical assets.
         */
        template <typename Parameter>
        inline void loadSpecial (const std::string& a_filename,
                                 const Parameter& a_extra,
                                 const std::string& a_id = "")
        {
            // Get the typenames for the class template and method template parameters.
            std::string l_classType = typeid(Asset).name();
            std::string l_funcType = typeid(a_extra).name();

            // Check to see if an asset file was provided.
            if (a_filename.empty())
            {
                PropertyManager& PM = PropertyManager::getInstance();
                PM.append("errorAssetType", false).set(l_classType);
                PM.append("errorExtraParamType", false).set(l_funcType);

                throw std::runtime_error
                {
                    "[AssetManager::loadSpecial] No asset file specified!"
                };
            }

            // Check to see if the asset file was already loaded, or if an asset was
            // already bound to the given ID, if one was provided.
            if ((a_id.empty() == false && m_assets.find(a_id) != m_assets.end()) ||
                    m_assets.find(a_filename) != m_assets.end())
            {
                return;
            }

            // Now attempt to load the asset.
            std::unique_ptr<Asset> l_asset = std::make_unique<Asset>();
            if (!l_asset->loadFromFile(a_filename, a_extra))
            {
                PropertyManager& PM = PropertyManager::getInstance();
                PM.append("errorAssetFile", false).set(a_filename);
                PM.append("errorAssetType", false).set(l_classType);
                PM.append("errorExtraParamType", false).set(l_funcType);

                throw std::runtime_error
                {
                    "[AssetManager::loadSpecial] Failed to load asset: '" + a_filename + "'!"
                };
            }

            // Map the asset.
            if (a_id.empty())
            {
                m_assets[a_filename] = std::move(l_asset);
                std::cout << "[AssetManager::loadSpecial] Loaded asset: '" << a_filename << "'." << std::endl;
            }
            else
            {
                m_assets[a_id] = std::move(l_asset);
                std::cout << "[AssetManager::loadSpecial] Loaded asset: '" << a_filename
                          << "' (ID: " << a_id << ")." << std::endl;
            }
        }

        /**
         * Attempts to fetch a loaded asset bound to the given id (usually, its filename).
         *
         * @param   a_id                The ID bound to the asset.
         *
         * @return  The loaded asset bound to the given ID.
         */
        inline Asset& get (const std::string& a_id)
        {
            // Find the asset. If it does not exist, then throw.
            auto l_find = m_assets.find(a_id);
            if (l_find == m_assets.end())
            {
                PropertyManager& PM = PropertyManager::getInstance();
                PM.append("errorAssetID", false).set(a_id);
                PM.append("errorAssetType", false).set(typeid(Asset).name());

                throw std::runtime_error
                {
                    "[AssetManager::get] Asset ID not found: '" + a_id + "'!"
                };
            }

            return *l_find->second;
        }

        /**
         * Attempts to unload an asset bound to the given ID.
         *
         * @param   a_id                The ID bound to the asset.
         */
        inline void unload (const std::string& a_id)
        {
            // Find the asset. If it exists, then unload it.
            auto l_find = m_assets.find(a_id);
            if (l_find != m_assets.end())
            {
                l_find = m_assets.erase(l_find);
            }
        }

        /**
         * Unloads all loaded assets.
         */
        inline void unloadAll ()
        {
            m_assets.clear();
        }
    };

    // Typedef some common asset managers.
    using TextureManager    = AssetManager<sf::Texture>;
    using FontManager       = AssetManager<sf::Font>;
    using ShaderManager     = AssetManager<sf::Shader>;
    using SoundManager      = AssetManager<sf::SoundBuffer>;

}
