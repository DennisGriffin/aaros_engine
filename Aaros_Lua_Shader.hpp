// File: Aaros_Lua_Shader.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeShader (sol::state& a_state);

    }
}
