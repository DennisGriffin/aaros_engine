// File: Aaros_Component_Tilemap.hpp
//
// Allows the owning entity to portray itself with a grid
// of tiles derived from a single image.

#pragma once

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include "Aaros_Shader.hpp"
#include "Aaros_Component.hpp"

namespace Aaros
{

    namespace Constants
    {

        const sf::Vector2i      G_DEFAULT_TILESIZE      = { 32, 32 };

    }

    struct Tile
    {
        using Container         = std::vector<Tile>;
        using Iterator          = Container::iterator;

        sf::Vector2i            m_position;
        sf::IntRect             m_sourceRect;

        Tile (const sf::Vector2i& a_dstPosition,
              const sf::Vector2i& a_srcPosition) :
            m_position          { a_dstPosition },
            m_sourceRect        { a_srcPosition, Constants::G_DEFAULT_TILESIZE }
        {}

        ~Tile ()
        {
            m_position = { 0, 0 };
            m_sourceRect = { 0, 0, 0, 0 };
        }
    };

    class TilemapComponent : public Component
    {
    private /* Members */:
        Tile::Container         m_tiles;
        sf::RenderTexture       m_texture;
        sf::Sprite              m_sprite;
        sf::Sprite              m_toRender;
        Shader                  m_shader;

    public /* Search Methods */:
        Tile::Iterator          findTile (const int a_x, const int a_y);

    public /* Constructor and Destructor */:
        TilemapComponent (Entity* ap_entity,
                          const std::string& a_entityName,
                          const unsigned int a_entityID);
        ~TilemapComponent ();

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Other Methods */:
        void                    setTile (const int a_x, const int a_y,
                                         const int a_sx, const int a_sy);

        void                    removeTile (const int a_x, const int a_y);

        void                    render (const sf::Vector2f& a_transformPosition,
                                        sf::RenderWindow& a_window,
                                        const sf::FloatRect& a_cameraRect);

    public /* Getter-Setters */:
        inline Shader&          getShader () { return m_shader; }

    };

}
