// File: Aaros_Component_Sound.hpp
//
// Emits sounds from a given position.

#pragma once

#include "Aaros_Utils_Math.hpp"
#include "Aaros_AssetManager.hpp"
#include "Aaros_Component.hpp"

namespace Aaros
{

    /**
     * This component allows the owning entity to emit spatialized sounds in the
     * game world. These sounds can be picked up by the scene's game camera, which
     * also acts as a listener.
     */
    class SoundComponent : public Component
    {
    private /* Members */:
        sf::Sound               m_sound;            /** The sound object. */
        std::string             m_currentSound;     /** The filename of the currently-loaded sound. */
        float                   m_minimumPitch;     /** The minimum pitch of the sound. */
        float                   m_maximumPitch;     /** The maximum pitch of the sound. */

    public /* Constructor and Destructor */:
        SoundComponent (Entity* ap_entity,
                        const std::string& a_entityName,
                        const unsigned int a_entityID);
        ~SoundComponent ();

    public /* Inherited Methods */:
        /**
         * Loads information about the sound, its volume, minimum hearing distance at full volume,
         * attenuation, its pitch, and looping status.
         */
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        /**
         * Uses the Sound Asset Manager to load a new sound file.
         *
         * @param   a_soundFile     The sound file to be loaded.
         */
        void                    loadSound (const std::string& a_soundFile);

        /**
         * Plays the loaded sound file given. The sound file must already be loaded
         * by the Sound Asset Manager, or an exception will be thrown.
         *
         * @param   a_soundFile     The loaded sound file to be played.
         */
        void                    playSound (const std::string& a_soundFile);

        /**
         * Pauses the currently playing sound.
         */
        void                    pauseSound ();

        /**
         * Stops the currently playing sound.
         */
        void                    stopSound ();

    public /* Internal Methods */:
        /**
         * Called by the sound system to update the current position of the sound, based
         * on the center point of the owning entity's transform.
         */
        void                    updatePosition ();

    public /* Outline Getter-Setters */:
        void                    setMinimumPitch (const float a_pitch);
        void                    setMaximumPitch (const float a_pitch);

    public /* Getter-Setters */:
        inline float            getVolume () const { return m_sound.getVolume(); }
        inline float            getMinimumDistance () const { return m_sound.getMinDistance(); }
        inline float            getAttenuation () const { return m_sound.getAttenuation(); }
        inline float            getMinimumPitch () const { return m_minimumPitch; }
        inline float            getMaximumPitch () const { return m_maximumPitch; }
        inline bool             isLooping () const { return m_sound.getLoop(); }

        inline void setVolume (const float a_volume) { m_sound.setVolume(Math::clampNumber(a_volume, 0.0f, 100.0f)); }
        inline void setMinimumDistance (const float a_distance) { m_sound.setMinDistance(std::abs(a_distance) * Constants::G_METERS_TO_PIXELS_F); }
        inline void setAttenuation (const float a_attenuation) { m_sound.setAttenuation(std::abs(a_attenuation)); }
        inline void setLooping (const bool a_looping) { m_sound.setLoop(a_looping); }
    };

}
