TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += C++14

CONFIG (debug, debug | release): LIBS += \
	-lsfml-audio-d -lsfml-graphics-d -lsfml-window-d -lsfml-system-d -lpugixml -llua

SOURCES += \
    Program.cpp \
    Aaros_Application.cpp \
    Aaros_Property.cpp \
    Aaros_PropertyManager.cpp \
    Aaros_Keyboard.cpp \
    Aaros_Mouse.cpp \
    Aaros_Scene.cpp \
    Aaros_Entity.cpp \
    Aaros_Component_Transform.cpp \
    Aaros_Component_Sprite.cpp \
    Aaros_Component_Script.cpp \
    Aaros_System_Render.cpp \
    Aaros_System_Script.cpp \
    Aaros_Component_Rigidbody.cpp \
    Aaros_Component_Collider.cpp \
    Aaros_System_Physics.cpp \
    Aaros_System_Widget.cpp \
    Aaros_Component_Widget.cpp \
    Aaros_Utils_ScriptModule.cpp \
    Aaros_Component_Tilemap.cpp \
    Aaros_Component_Animation.cpp \
    Aaros_System_Animation.cpp \
    Aaros_Jukebox.cpp \
    Aaros_Component_Sound.cpp \
    Aaros_System_Sound.cpp \
    Aaros_GUI_Textarea.cpp \
    Aaros_GUI_Button.cpp \
    Aaros_GUI_Scrollbar.cpp \
    Aaros_GUI_Listbox.cpp \
    Aaros_GUI_Progressbar.cpp \
    Aaros_GUI_Checkbox.cpp \
    Aaros_Component_Emitter.cpp \
    Aaros_Shader.cpp \
    Aaros_Lua_Properties.cpp \
    Aaros_Lua_Jukebox.cpp \
    Aaros_Lua_Keyboard.cpp \
    Aaros_Lua_Mouse.cpp \
    Aaros_Lua_Shader.cpp \
    Aaros_Lua_Scene.cpp \
    Aaros_Lua_Widgets.cpp \
    Aaros_Lua_Components.cpp \
    Aaros_Lua_Entity.cpp \
    Aaros_Component_Timer.cpp \
    Aaros_System_Timer.cpp \
    Aaros_Component_Text.cpp

include(deployment.pri)
qtcAddDeployment()


HEADERS += \
    Aaros_Application.hpp \
    Aaros_Utils_XmlLoadable.hpp \
    Aaros_Utils_Singleton.hpp \
    Aaros_Utils_String.hpp \
    Aaros_Property.hpp \
    Aaros_PropertyManager.hpp \
    Aaros_AssetManager.hpp \
    Aaros_Keyboard.hpp \
    Aaros_Mouse.hpp \
    Aaros_Scene.hpp \
    Aaros_Entity.hpp \
    Aaros_Component.hpp \
    Aaros_Component_Transform.hpp \
    Aaros_Component_Sprite.hpp \
    Aaros_System.hpp \
    Aaros_Utils_Math.hpp \
    Aaros_Component_Script.hpp \
    Aaros_System_Script.hpp \
    Aaros_System_Render.hpp \
    Aaros_Lua_Components.hpp \
    Aaros_Lua_Entity.hpp \
    Aaros_Lua_Scene.hpp \
    Aaros_Lua_Keyboard.hpp \
    Aaros_Lua_Mouse.hpp \
    Aaros_Lua_Properties.hpp \
    Aaros_Component_Rigidbody.hpp \
    Aaros_Component_Collider.hpp \
    Aaros_System_Physics.hpp \
    Aaros_System_Widget.hpp \
    Aaros_Component_Widget.hpp \
    Aaros_GUI_Widget.hpp \
    Aaros_Utils_Misc.hpp \
    Aaros_Utils_ScriptModule.hpp \
    Aaros_Component_Tilemap.hpp \
    Aaros_Component_Animation.hpp \
    Aaros_System_Animation.hpp \
    Aaros_Jukebox.hpp \
    Aaros_Lua_Jukebox.hpp \
    Aaros_Component_Sound.hpp \
    Aaros_System_Sound.hpp \
    Aaros_Utils_GUI.hpp \
    Aaros_GUI_Textarea.hpp \
    Aaros_Lua_Widgets.hpp \
    Aaros_GUI_Button.hpp \
    Aaros_GUI_Scrollbar.hpp \
    Aaros_GUI_Listbox.hpp \
    Aaros_GUI_Progressbar.hpp \
    Aaros_GUI_Checkbox.hpp \
    Aaros_Component_Emitter.hpp \
    Aaros_Utils_Random.hpp \
    Aaros_Shader.hpp \
    Aaros_Lua_Shader.hpp \
    Aaros_Component_Timer.hpp \
    Aaros_System_Timer.hpp \
    Aaros_Component_Text.hpp

