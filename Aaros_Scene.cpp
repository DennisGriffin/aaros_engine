// File: Aaros_Scene.cpp

#include "Aaros_Scene.hpp"
#include "Aaros_Application.hpp"
#include "Aaros_Jukebox.hpp"
#include "Aaros_Lua_Jukebox.hpp"
#include "Aaros_Lua_Keyboard.hpp"
#include "Aaros_Lua_Mouse.hpp"
#include "Aaros_Lua_Properties.hpp"
#include "Aaros_Lua_Shader.hpp"
#include "Aaros_Lua_Widgets.hpp"
#include "Aaros_Lua_Components.hpp"
#include "Aaros_Lua_Entity.hpp"
#include "Aaros_Lua_Scene.hpp"

namespace Aaros
{

    Entity::Iter Scene::findEntityByName(const std::string &a_name)
    {
        return std::find_if(m_entities.begin(), m_entities.end(),
                            [&a_name] (const Entity::Ptr& a_entity)
        {
            return a_entity->getFullName() == a_name;
        });
    }

    void Scene::initializeLua()
    {
        // Set up the Lua context.
        m_lua.open_libraries();

        // Expose our engine's functionality to Lua.
        Lua::exposeJukebox(m_lua);
        Lua::exposeKeyboard(m_lua);
        Lua::exposeMouse(m_lua);
        Lua::exposeProperty(m_lua);
        Lua::exposePropertyManager(m_lua);
        Lua::exposeShader(m_lua);
        Lua::exposeWidgets(m_lua);
        Lua::exposeComponents(m_lua);
        Lua::exposeEntity(m_lua);
        Lua::exposeScene(m_lua);
    }

    void Scene::initializeCamera()
    {
        PropertyManager& PM = PropertyManager::getInstance();

        float l_windowWidth     = PM.get("windowWidth").getAs<float>();
        float l_windowHeight    = PM.get("windowHeight").getAs<float>();

        m_camera = sf::View(sf::FloatRect(0, 0, l_windowWidth, l_windowHeight));

        m_renderSystem.updateCameraRect(m_camera);
        m_animationSystem.updateCameraRect(m_camera);
        m_scriptSystem.updateCameraRect(m_camera);
        m_timerSystem.updateCameraRect(m_camera);
    }

    void Scene::loadRequestedScene ()
    {
        if (m_newSceneRequested == true)
        {
            loadFromFile(m_requestedSceneFile, "Scene");
            m_requestedSceneFile.clear();
            m_newSceneRequested = false;
        }
    }

    void Scene::cullEntities()
    {
        for (auto l_iter = m_entities.begin(); l_iter != m_entities.end(); )
        {
            // If this entity (or an ancestor thereof) is marked dead, then unload it.
            if ((*l_iter)->isAlive() == false)
            {
                if (mp_center != nullptr)
                {
                    if ((*l_iter)->getID() == mp_center->getID())
                    {
                        mp_center = nullptr;
                    }
                }

                (*l_iter)->setParentEntity("");
                (*l_iter)->detachOnKill();
                l_iter = m_entities.erase(l_iter);
            }
            else
            {
                l_iter++;
            }
        }

        sortEntities();
    }

    void Scene::sortEntities()
    {
        return std::sort(m_entities.begin(), m_entities.end(),
                            [] (const Entity::Ptr& a_one, const Entity::Ptr& a_two)
        {
            return a_one->getOrder() < a_two->getOrder();
        });
    }

    void Scene::updateCamera()
    {
        if (mp_center != nullptr)
        {
            sf::Vector2f l_center = mp_center->getTransform()->getPixelCenter();

            m_camera.setCenter(l_center);
            sf::Listener::setPosition(l_center.x, l_center.y, 0.0f);

            m_renderSystem.updateCameraRect(m_camera);
            m_animationSystem.updateCameraRect(m_camera);
            m_scriptSystem.updateCameraRect(m_camera);
            m_timerSystem.updateCameraRect(m_camera);
        }
    }

    Scene::Scene(Application *ap_application) :
        mp_application          { ap_application },
        m_entityCount           { 0 },
        m_paused                { false },
        m_camera                { { 0.0f, 0.0f, 0.0f, 0.0f } },
        mp_center               { nullptr },
        m_newSceneRequested     { false },
        m_requestedSceneFile    { "" }
    {
        initializeLua();

        setVelocityIterCount(6);
        setPositionIterCount(2);

        initializeCamera();
    }

    Scene::~Scene()
    {
        mp_application          = nullptr;
        mp_center               = nullptr;
        m_entities.clear();
        m_paused                = false;
    }

    void Scene::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        // Set some properties for the scene.
        xml_node l_propertiesNode = a_node.child("Properties");
        xml_node l_gravityNode = l_propertiesNode.child("Gravity");
        xml_node l_cameraNode = l_propertiesNode.child("Camera");
        xml_node l_musicNode = l_propertiesNode.child("Music");

        // Set the gravity for the scene.
        float l_gx = l_gravityNode.attribute("X").as_float();
        float l_gy = l_gravityNode.attribute("Y").as_float();
        setGravity(l_gx, l_gy);

        // Iterate through all child nodes named "Entity".
        for (const xml_node& l_node : a_node.children("Entity"))
        {
            // Get the entity's name.
            std::string l_name = l_node.attribute("Name").as_string();

            // If it is not empty, then create and load it.
            if (!l_name.empty())
            {
                addEntity(l_name).loadFromXML(l_node);
            }
        }

        // Set up the game camera for the scene.
        if (l_cameraNode.empty() == false)
        {
            std::string l_entityName = l_cameraNode.attribute("CenterOnEntity").as_string();

            if (l_entityName.empty() == false)
            {
                centerCameraOnEntity(l_entityName);
            }
            else
            {
                float l_cx = l_cameraNode.attribute("X").as_float();
                float l_cy = l_cameraNode.attribute("Y").as_float();

                centerCameraOnPoint(l_cx, l_cy);
            }
        }

        // Set up the music.
        if (l_musicNode.empty() == false)
        {
            std::string l_songFile = l_musicNode.attribute("SongFile").as_string();
            bool l_songLooping = l_musicNode.attribute("Looping").as_bool(false);
            bool l_songPlaying = l_musicNode.attribute("Play").as_bool(true);

            Jukebox& J = Jukebox::getInstance();

            J.loadSong(l_songFile);
            J.setLooping(l_songLooping);

            if (l_songPlaying == true) { J.playSong(); }
        }
    }

    void Scene::loadNew (const std::string &a_filename, const bool a_removePersistents)
    {
        removeAllEntities(a_removePersistents);

        m_requestedSceneFile = a_filename;
        m_newSceneRequested = true;
    }

    void Scene::loadAdditive (const std::string &a_filename)
    {
        m_requestedSceneFile = a_filename;
        m_newSceneRequested = true;
    }

    Entity &Scene::addEntity(const std::string &a_name)
    {
        // Check to see if the name given contains valid characters.
        // If not, then throw.
        if (a_name.find('.') != std::string::npos)
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorEntityName", false).set(a_name);
            PM.append("errorSceneFile", false).set(m_filename);

            throw std::invalid_argument { "[Scene::addEntity] New entity name '" + a_name + "' contains invalid characters!" };
        }

        // Find the entity. If it does exist, then return that instead.
        auto l_find = findEntityByName(a_name);
        if (l_find != m_entities.end())
        {
            return *(*l_find);
        }

        // Create the entity and wrap it in a smart pointer.
        Entity* lp_new = new Entity(this, &m_lua, a_name);
        Entity::Ptr l_smart { lp_new };

        // Move the smart pointer into the container.
        m_entities.push_back(std::move(l_smart));

        // De-reference and return the raw pointer.
        return *lp_new;
    }

    Entity &Scene::getEntity(const std::string &a_name)
    {
        // Find the entity. If it does not exist, then throw an exception.
        auto l_find = findEntityByName(a_name);
        if (l_find == m_entities.end())
        {
            PropertyManager& PM = PropertyManager::getInstance();

            PM.append("errorEntityName", false).set(a_name);
            PM.append("errorSceneFile", false).set(m_filename);

            throw std::runtime_error { "[Entity::getEntity] '" + a_name + "': Entity not found!" };
        }

        // Return the entity.
        return *(*l_find);
    }

    void Scene::removeAllEntities(const bool a_allOfThem)
    {
        for (auto l_iter = m_entities.begin(); l_iter != m_entities.end(); ++l_iter)
        {
            // If the entity is not marked as persistent, then flag it for unloading.
            if ((*l_iter)->isPersistent() == false || a_allOfThem == true)
            {
                (*l_iter)->setAlive(false);
            }
        }
    }

    void Scene::centerCameraOnPoint(const float a_x, const float a_y)
    {
        mp_center = nullptr;
        m_camera.setCenter(a_x * Constants::G_METERS_TO_PIXELS_F, a_y * Constants::G_METERS_TO_PIXELS_F);
        sf::Listener::setPosition(a_x * Constants::G_METERS_TO_PIXELS_F, a_y * Constants::G_METERS_TO_PIXELS_F, 0.0f);

        m_renderSystem.updateCameraRect(m_camera);
        m_animationSystem.updateCameraRect(m_camera);
        m_scriptSystem.updateCameraRect(m_camera);
        m_timerSystem.updateCameraRect(m_camera);
    }

    void Scene::centerCameraOnEntity(const std::string &a_entityName)
    {
        Entity& l_entity = getEntity(a_entityName);

        mp_center = &l_entity;
        updateCamera();
    }

    void Scene::update(const sf::Time &a_deltaTime)
    {
        cullEntities();

        m_entityCount = m_entities.size();

        if (m_paused == false)
        {
            m_scriptSystem.update(m_entities, a_deltaTime, m_entityCount);
            m_timerSystem.update(m_entities, a_deltaTime, m_entityCount);
            m_renderSystem.update(m_entities, a_deltaTime, m_entityCount);
            m_animationSystem.update(m_entities, a_deltaTime, m_entityCount);
            m_soundSystem.update(m_entities, a_deltaTime, m_entityCount);

            updateCamera();
        }

        m_widgetSystem.update(m_entities, a_deltaTime, m_entityCount);

        loadRequestedScene();
    }

    void Scene::fixedUpdate(const sf::Time &a_timestep)
    {
        cullEntities();

        m_entityCount = m_entities.size();

        if (m_paused == false)
        {
            m_scriptSystem.fixedUpdate(m_entities, a_timestep, m_entityCount);
            m_physicsSystem.fixedUpdate(m_entities, a_timestep, m_entityCount);
        }

        loadRequestedScene();
    }

    void Scene::render(sf::RenderWindow &a_window)
    {
        m_entityCount = m_entities.size();

        a_window.setView(m_camera);
        m_renderSystem.render(m_entities, a_window, m_entityCount);
        a_window.setView(a_window.getDefaultView());

        m_widgetSystem.render(m_entities, a_window, m_entityCount);
    }

    void Scene::setWindowSize (const unsigned int a_width, const unsigned int a_height)
    {
        mp_application->setWindowSize(a_width, a_height);
    }

    void Scene::setWindowFullscreen (const bool a_fullscreen)
    {
        mp_application->setWindowFullscreen(a_fullscreen);
    }

    void Scene::quit()
    {
        mp_application->quit();
    }

}
