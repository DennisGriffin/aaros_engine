// File: Aaros_GUI_Button.cpp

#include "Aaros_GUI_Button.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{
    namespace GUI
    {

        void Button::updateWidgetInternals ()
        {
            FontManager& FM = FontManager::getInstance();
            float l_thickness = Constants::G_BUTTON_OUTLINE_THICKNESS;

            m_rectObject.setPosition(m_position);
            m_rectObject.setSize(m_rectSize);

            m_textObject.setPosition(m_position.x + l_thickness, m_position.y + l_thickness);
            m_textObject.setCharacterSize(m_textFontSize);
            m_textWrapSize.x = m_rectSize.x - (l_thickness * 2.0f);
            m_textWrapSize.y = m_rectSize.y - (l_thickness * 2.0f);

            m_textObject.setString(wrapText(m_text, m_textWrapSize, FM.get(m_textFontID), m_textFontSize));
        }

        Button::Button(Entity *ap_entity, const std::string &a_name) :
            Widget              { ap_entity, a_name },
            m_rectSize          { Constants::G_BUTTON_SIZE },
            m_rectFillColor     { Constants::G_BUTTON_FILL_COLOR },
            m_rectFocusFill     { Constants::G_BUTTON_ACTIVE_FILL_COLOR },
            m_rectOutlineColor  { Constants::G_BUTTON_OUTLINE_COLOR },
            m_rectFocusOutline  { Constants::G_BUTTON_ACTIVE_OUTLINE_COLOR },
            m_rectVisible       { true },
            m_selected          { false },
            m_text              { "" },
            m_textFontID        { "globalFont" },
            m_textFontSize      { Constants::G_BUTTON_FONT_SIZE },
            m_textWrapSize      { 0.0f, 0.0f },
            m_textColor         { Constants::G_BUTTON_TEXT_COLOR },
            m_textFocus         { Constants::G_BUTTON_ACTIVE_TEXT_COLOR },
            m_id                { "" }
        {
            m_rectObject.setOutlineThickness(Constants::G_BUTTON_OUTLINE_THICKNESS);
            setFont(m_textFontID);

            m_focusable = true;
            updateWidgetInternals();
        }

        Button::~Button()
        {
            m_rectSize = { 0.0f, 0.0f };
            m_rectVisible = false;
            m_selected = false;
            m_text.clear();
            m_textFontID.clear();
            m_textFontSize = 0;
            m_textWrapSize = { 0.0f, 0.0f };
            m_id.clear();

            m_enabled = false;
            m_visible = false;
            m_focused = false;
            m_focusable = false;
            m_name.clear();
            m_position = { 0.0f, 0.0f };
            mp_entity = nullptr;
        }

        void Button::loadFromXML(const pugi::xml_node &a_node)
        {
            using namespace pugi;

            xml_node l_positionNode = a_node.child("Position");
            xml_node l_sizeNode = a_node.child("Size");
            xml_node l_fontNode = a_node.child("Font");
            xml_node l_textNode = a_node.child("Text");
            xml_node l_colorNode = a_node.child("Color");

            float l_x = l_positionNode.attribute("X").as_float();
            float l_y = l_positionNode.attribute("Y").as_float();

            float l_w = l_sizeNode.attribute("Width").as_float(Constants::G_BUTTON_SIZE.x);
            float l_h = l_sizeNode.attribute("Height").as_float(Constants::G_BUTTON_SIZE.y);

            std::string l_fontID = l_fontNode.attribute("File").as_string("globalFont");
            unsigned int l_fontSize = l_fontNode.attribute("Size").as_uint(Constants::G_BUTTON_FONT_SIZE);

            std::string l_text = l_textNode.attribute("Value").as_string();

            int l_fillRed = l_colorNode.child("Fill").attribute("Red").as_int(Constants::G_BUTTON_FILL_COLOR.r);
            int l_fillGreen = l_colorNode.child("Fill").attribute("Green").as_int(Constants::G_BUTTON_FILL_COLOR.g);
            int l_fillBlue = l_colorNode.child("Fill").attribute("Blue").as_int(Constants::G_BUTTON_FILL_COLOR.b);
            int l_outlineRed = l_colorNode.child("Outline").attribute("Red").as_int(Constants::G_BUTTON_OUTLINE_COLOR.r);
            int l_outlineGreen = l_colorNode.child("Outline").attribute("Green").as_int(Constants::G_BUTTON_OUTLINE_COLOR.g);
            int l_outlineBlue = l_colorNode.child("Outline").attribute("Blue").as_int(Constants::G_BUTTON_OUTLINE_COLOR.b);
            int l_textRed = l_colorNode.child("Text").attribute("Red").as_int(Constants::G_BUTTON_TEXT_COLOR.r);
            int l_textGreen = l_colorNode.child("Text").attribute("Green").as_int(Constants::G_BUTTON_TEXT_COLOR.g);
            int l_textBlue = l_colorNode.child("Text").attribute("Blue").as_int(Constants::G_BUTTON_TEXT_COLOR.b);
            int l_fillRedFocus = l_colorNode.child("FocusFill").attribute("Red").as_int(Constants::G_BUTTON_ACTIVE_FILL_COLOR.r);
            int l_fillGreenFocus = l_colorNode.child("FocusFill").attribute("Green").as_int(Constants::G_BUTTON_ACTIVE_FILL_COLOR.g);
            int l_fillBlueFocus = l_colorNode.child("FocusFill").attribute("Blue").as_int(Constants::G_BUTTON_ACTIVE_FILL_COLOR.b);
            int l_outlineRedFocus = l_colorNode.child("FocusOutline").attribute("Red").as_int(Constants::G_BUTTON_ACTIVE_OUTLINE_COLOR.r);
            int l_outlineGreenFocus = l_colorNode.child("FocusOutline").attribute("Green").as_int(Constants::G_BUTTON_ACTIVE_OUTLINE_COLOR.g);
            int l_outlineBlueFocus = l_colorNode.child("FocusOutline").attribute("Blue").as_int(Constants::G_BUTTON_ACTIVE_OUTLINE_COLOR.b);
            int l_textRedFocus = l_colorNode.child("FocusText").attribute("Red").as_int(Constants::G_BUTTON_ACTIVE_TEXT_COLOR.r);
            int l_textGreenFocus = l_colorNode.child("FocusText").attribute("Green").as_int(Constants::G_BUTTON_ACTIVE_TEXT_COLOR.g);
            int l_textBlueFocus = l_colorNode.child("FocusText").attribute("Blue").as_int(Constants::G_BUTTON_ACTIVE_TEXT_COLOR.b);

            setPosition(l_x, l_y);
            setSize(l_w, l_h);
            setFont(l_fontID);
            setFontSize(l_fontSize);
            setText(l_text);
            setFillColor(l_fillRed, l_fillGreen, l_fillBlue);
            setOutlineColor(l_outlineRed, l_outlineGreen, l_outlineBlue);
            setTextColor(l_textRed, l_textGreen, l_textBlue);
            setFocusFillColor(l_fillRedFocus, l_fillGreenFocus, l_fillBlueFocus);
            setFocusOutlineColor(l_outlineRedFocus, l_outlineGreenFocus, l_outlineBlueFocus);
            setFocusTextColor(l_textRedFocus, l_textGreenFocus, l_textBlueFocus);

            updateWidgetInternals();
        }

        void Button::update(const sf::Time &a_deltaTime)
        {
            (void) a_deltaTime;

            Mouse& M = Mouse::getInstance();
            Keyboard& K = Keyboard::getInstance();
            bool l_hovering = m_rectObject.getGlobalBounds().contains(M.getPosition());

            m_selected = false;

            if (l_hovering || m_focused)
            {
                m_rectObject.setFillColor(m_rectFocusFill);
                m_rectObject.setOutlineColor(m_rectFocusOutline);
                m_textObject.setColor(m_textFocus);

                bool l_clicked = (l_hovering && M.isButtonPressed(sf::Mouse::Left));
                bool l_selected = (m_focused && K.isKeyPressed(sf::Keyboard::Return));

                if (l_clicked || l_selected)
                {
                    if (mp_entity != nullptr)
                        mp_entity->callLuaFunction(m_name + "_onButtonSelected");

                    m_selected = true;
                }
            }
            else
            {
                m_rectObject.setFillColor(m_rectFillColor);
                m_rectObject.setOutlineColor(m_rectOutlineColor);
                m_textObject.setColor(m_textColor);
            }

            updateWidgetInternals();
        }

        void Button::render(sf::RenderWindow &a_window)
        {
            if (m_rectVisible == true) { a_window.draw(m_rectObject); }

            a_window.draw(m_textObject);
        }

    }
}
