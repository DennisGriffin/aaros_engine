// File: Aaros_Component_Collider.cpp

#include "Aaros_Component_Collider.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    std::vector<unsigned int>::iterator ColliderComponent::findWasCollidingID(const unsigned int a_id)
    {
        return std::find_if(m_wasCollidingWith.begin(), m_wasCollidingWith.end(),
                            [&a_id] (const unsigned int a_col)
        {
            return a_id == a_col;
        });
    }

    void ColliderComponent::onPositionChanged()
    {
        m_absolutePosition = m_originPoint + m_relativePosition;
    }

    ColliderComponent::ColliderComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_originPoint           { 0.0f, 0.0f },
        m_relativePosition      { 0.0f, 0.0f },
        m_absolutePosition      { 0.0f, 0.0f },
        m_size                  { 0.0f, 0.0f },
        m_restitution           { 0.0f },
        m_trigger               { false }
    {

    }

    ColliderComponent::~ColliderComponent ()
    {
        m_originPoint           = { 0.0f, 0.0f };
        m_relativePosition      = { 0.0f, 0.0f };
        m_absolutePosition      = { 0.0f, 0.0f };
        m_size                  = { 0.0f, 0.0f };
        m_restitution           = 0.0f;
        m_trigger               = false;

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void ColliderComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_positionNode = a_node.child("Position");
        xml_node l_sizeNode = a_node.child("Size");
        xml_node l_physicsNode = a_node.child("Physics");

        float l_x = l_positionNode.attribute("X").as_float();
        float l_y = l_positionNode.attribute("Y").as_float();
        float l_width = l_sizeNode.attribute("Width").as_float(1.0f);
        float l_height = l_sizeNode.attribute("Height").as_float(1.0f);
        float l_restitution = l_physicsNode.attribute("Restitution").as_float();
        bool l_trigger = l_physicsNode.attribute("Trigger").as_bool(false);

        m_relativePosition = { l_x, l_y };
        m_size = { std::abs(l_width), std::abs(l_height) };
        m_restitution = std::abs(l_restitution);
        m_trigger = l_trigger;

        onPositionChanged();
    }

    void ColliderComponent::updateOrigin ()
    {
        m_originPoint = mp_entity->getTransform()->getPosition();
        onPositionChanged();
    }

    void ColliderComponent::resetCollisionStatus()
    {
        m_wasCollidingWith.clear();

        for (auto l_iter = m_collidingWith.begin(); l_iter != m_collidingWith.end(); )
        {
            m_wasCollidingWith.push_back(*l_iter);
            l_iter = m_collidingWith.erase(l_iter);
        }
    }

    void ColliderComponent::collisionCallback(ColliderComponent *ap_other, const sf::Vector2f &a_normal)
    {
        unsigned int l_otherID = ap_other->getEntityID();
        auto l_find = findWasCollidingID(l_otherID);

        if (l_find != m_wasCollidingWith.end())
        {
            mp_entity->callLuaFunction("onCollisionStay", ap_other->getOwningEntity(), a_normal.x, a_normal.y);
        }
        else
        {
            mp_entity->callLuaFunction("onCollisionEnter", ap_other->getOwningEntity(), a_normal.x, a_normal.y);
        }

        m_collidingWith.push_back(l_otherID);
    }

    void ColliderComponent::noCollisionCallback(ColliderComponent *ap_other)
    {
        unsigned int l_otherID = ap_other->getEntityID();
        auto l_find = findWasCollidingID(l_otherID);

        if (l_find != m_wasCollidingWith.end())
        {
            mp_entity->callLuaFunction("onCollisionExit", ap_other->getOwningEntity());
        }
    }

    void ColliderComponent::checkClicked()
    {
        if (m_trigger == false) { return; }

        Mouse& M = Mouse::getInstance();
        sf::FloatRect l_rect = { m_absolutePosition, m_size };

        if (l_rect.contains(M.getMeterCoordX(), M.getMeterCoordY()))
        {
            int l_button = -1;

            if (M.isButtonPressed(sf::Mouse::Left)) { l_button = sf::Mouse::Left; }
            else if (M.isButtonPressed(sf::Mouse::Right)) { l_button = sf::Mouse::Right; }
            else if (M.isButtonPressed(sf::Mouse::Middle)) { l_button = sf::Mouse::Middle; }

            if (l_button != -1) { mp_entity->callLuaFunction("onTriggerClicked", l_button, M.getPositionX(), M.getPositionY(), M.getMeterCoordX(), M.getMeterCoordY()); }
        }
    }

}
