// File: Aaros_Lua_Properties.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeProperty (sol::state& a_state);
        void exposePropertyManager (sol::state& a_state);

    }
}
