// File: Aaros_Component_Text.cpp

#include "Aaros_Component_Text.hpp"
#include "Aaros_Entity.hpp"
#include "Aaros_Utils_GUI.hpp"

namespace Aaros
{

    TextComponent::TextComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component           { ap_entity, a_entityName, a_entityID },
        m_centered          { false }
    {
        setFont("globalFont");
        m_textObject.setCharacterSize(14);
        m_textObject.setColor(sf::Color::White);
    }

    TextComponent::~TextComponent ()
    {
        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void TextComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_fontNode = a_node.child("Font");
        xml_node l_textNode = a_node.child("Text");
        xml_node l_colorNode = a_node.child("Color");

        std::string l_fontFile = l_fontNode.attribute("File").as_string("globalFont");
        unsigned int l_fontSize = l_fontNode.attribute("Size").as_uint(14);

        bool l_centered = l_textNode.attribute("Centered").as_bool(false);
        std::string l_text = l_textNode.attribute("Value").as_string();

        int l_red = l_colorNode.attribute("Red").as_int(255);
        int l_green = l_colorNode.attribute("Green").as_int(255);
        int l_blue = l_colorNode.attribute("Blue").as_int(255);
        int l_alpha = l_colorNode.attribute("Alpha").as_int(255);

        setFont(l_fontFile);
        setFontSize(l_fontSize);

        setCentered(l_centered);
        setText(l_text);

        setColor(l_red, l_green, l_blue, l_alpha);
    }

    void TextComponent::setFont (const std::string &a_id)
    {
        FontManager& FM = FontManager::getInstance();

        if (a_id.empty() || a_id == "globalFont")
        {
            m_textObject.setFont(FM.get("globalFont"));
        }
        else
        {
            FM.load(a_id);
            m_textObject.setFont(FM.get(a_id));
        }
    }

}
