// File: Aaros_Component_Animation.cpp

#include "Aaros_Component_Animation.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    void AnimationComponent::updateAnimation ()
    {
        Animation& l_animation = m_animations[m_currentAnimation];

        if (!l_animation.empty())
        {
            m_currentFrame++;

            if (m_currentFrame >= l_animation.size())
            {
                m_currentFrame = 0;

                mp_entity->callLuaFunction("onAnimationCompleted", m_currentAnimation);
            }
        }
    }

    AnimationComponent::AnimationComponent (Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        m_currentAnimation      { Constants::G_DEFAULT_ANIMATION_ID },
        m_currentFrame          { 0 },
        m_currentFrameTime      { sf::Time::Zero },
        m_frameInterval         { Constants::G_DEFAULT_FRAME_INTERVAL },
        m_frameSize             { Constants::G_DEFAULT_FRAME_SIZE }
    {
        m_animations[m_currentAnimation] = {};
    }

    AnimationComponent::~AnimationComponent ()
    {
        m_currentAnimation.clear();
        m_currentFrame = 0;
        m_currentFrameTime = sf::Time::Zero;
        m_frameInterval = sf::Time::Zero;
        m_frameSize = { 0, 0 };

        m_enabled               = false;
        m_ancestorDisabled      = false;
        m_entityName.clear();
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void AnimationComponent::loadFromXML (const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_detailNode = a_node.child("Details");
        xml_node l_animationNode = a_node.child("Animations");

        int l_width = l_detailNode.attribute("Width").as_int(Constants::G_DEFAULT_FRAME_SIZE.x);
        int l_height = l_detailNode.attribute("Height").as_int(Constants::G_DEFAULT_FRAME_SIZE.y);
        float l_interval = l_detailNode.attribute("Interval").as_float(Constants::G_DEFAULT_FRAME_INTERVAL.asSeconds());
        std::string l_default = l_animationNode.attribute("Default").as_string(Constants::G_DEFAULT_ANIMATION_ID.c_str());

        setFrameSize(l_width, l_height);
        setFrameInterval(l_interval);

        for (const xml_node& l_node : l_animationNode.children("Frame"))
        {
            std::string l_id = l_node.attribute("ID").as_string(Constants::G_DEFAULT_ANIMATION_ID.c_str());
            int l_x = l_node.attribute("X").as_int();
            int l_y = l_node.attribute("Y").as_int();

            pushFrame(l_id, l_x, l_y);
        }

        setCurrentAnimation(l_default);
    }

    void AnimationComponent::setCurrentAnimation(const std::string &a_id)
    {
        if (a_id.empty() || a_id == m_currentAnimation) { return; }

        auto l_find = m_animations.find(a_id);
        if (l_find != m_animations.end())
        {
            m_currentAnimation = a_id;
            m_currentFrame = 0;
            m_currentFrameTime = sf::Time::Zero;
        }
    }

    void AnimationComponent::pushFrame (const std::string &a_id, const int a_x, const int a_y)
    {
        if (a_id.empty()) { return; }

        if (m_animations.find(a_id) == m_animations.end())
        {
            m_animations[a_id] = {};
        }

        m_animations[a_id].emplace_back(std::abs(a_x), std::abs(a_y));

        if (a_id == m_currentAnimation)
        {
            m_currentFrame = 0;
            m_currentFrameTime = sf::Time::Zero;
        }
    }

    void AnimationComponent::popFrame (const std::string &a_id)
    {
        if (a_id.empty()) { return; }

        auto l_find = m_animations.find(a_id);
        if (l_find != m_animations.end())
        {
            if (l_find->second.empty() == false)
            {
                l_find->second.pop_back();
                m_currentFrame = 0;
                m_currentFrameTime = sf::Time::Zero;
            }
        }
    }

    void AnimationComponent::tick (const sf::Time &a_time)
    {
        m_currentFrameTime += a_time;

        while (m_currentFrameTime >= m_frameInterval)
        {
            m_currentFrameTime -= m_frameInterval;
            updateAnimation();
        }
    }

    sf::Vector2i AnimationComponent::getCurrentFrameVector()
    {
        return m_animations[m_currentAnimation].at(m_currentFrame);
    }

}
