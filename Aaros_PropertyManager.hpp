// File: Aaros_PropertyManager.hpp
//
// A singleton class for creating, accessing, and saving key-value
// pieces of data.

#pragma once

#include <ostream>
#include "Aaros_Property.hpp"
#include "Aaros_Utils_Singleton.hpp"
#include "Aaros_Utils_String.hpp"
#include "Aaros_Utils_XmlLoadable.hpp"

namespace Aaros
{

    /**
     * This singleton class provides the user with an interface for
     * retrieving, manipulating, and saving key-value pieces of data.
     */
    class PropertyManager :
            public Utils::XmlLoadable,
            public Utils::Singleton<PropertyManager>
    {
    private /* Friend Classes */:
        friend class Utils::Singleton<PropertyManager>;

    private /* Members */:
        Property::Container     m_properties;           /** The list of loaded properties. */

        /** A "null" property. See "getProperty" below. */
        Property                m_nullProperty { "", "", false };

    private /* Methods */:
        /**
         * Seeks out a property with the given name.
         *
         * @param   a_name              The name of the property.
         *
         * @return  If found, the property's position in the container.
         */
        Property::Iterator      findProperty (const std::string& a_name);

    public /* Inherited Methods */:
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        /**
         * Saves all loaded properties flagged as savble to the XML node given.
         * The user can also specify to save ALL properties, not just those flagged.
         *
         * @param   a_node              The XML node into which the properties will be written.
         * @param   a_all               All properties, or only those flagged as savable?
         */
        void                    saveToXML (pugi::xml_node& a_node,
                                           const bool a_all = false);

        /**
         * Saves all loaded properties flagged as savble to a file.
         * The user can also specify to save ALL properties, not just those flagged.
         *
         * @param   a_filename          The name of the XML file.
         * @param   a_all               All properties, or only those flagged as savable?
         */
        void                    saveToFile (const std::string& a_filename,
                                            const std::string& a_rootname,
                                            const bool a_all = false);

        /**
         * Saves one specific property to the XML node given. Since these properties are being
         * stated explicitly, their savable flags are not considered here.
         *
         * Multiple properties can be specified with the pipe symbol '|'.
         * Example: "propertyOne | propertyTwo"
         *
         * @param   a_property          The name of the property to be saved.
         * @param   a_node              The XML node into which the property/properties will be written.
         */
        void                    savePropertyToXML (const std::string& a_property,
                                                   pugi::xml_node& a_node);

        /**
         * Saves one or more specific properties directly to the XML file given.
         *
         * @param   a_property          The name of the property(ies) to be saved.
         * @param   a_filename          The name of the XML file to save to.
         * @param   a_rootname          The name to be given to the file's root node.
         */
        void                    savePropertyToFile (const std::string& a_property,
                                                    const std::string& a_filename,
                                                    const std::string& a_rootname);

        /**
         * Saves one or more specific properties to the XML file given, without overwriting the
         * file. If the file specified does not exist, then this method has the same effect as
         * 'savePropertyToFile' above.
         *
         * @param   a_property          The name of the property(ies) to be appended.
         * @param   a_filename          The name of the XML file to save/append to.
         * @param   a_rootname          If the file does not exist, specifies a root node for the newly-created file.
         */
        void                    appendPropertyToFile (const std::string& a_property,
                                                      const std::string& a_filename,
                                                      const std::string& a_rootname = "Properties");

        /**
         * Called in the event of an engine crash, this method dumps the keys
         * and values of all loaded properties into the specified stream.
         *
         * @param   a_out               The output stream through which to dump the properties.
         */
        void                    dumpToStream (std::ostream& a_out);

        /**
         * Creates a new property with the given name and save flag, adds it to the property
         * manager, then returns a handle to it to the user.
         *
         * If a property with the given name already exists in the property manager, that
         * property will be returned instead.
         *
         * @param   a_name              The name of the new property.
         * @param   a_save              The save flag of the property.
         *
         * @return  The newly-created (or already-existing) property.
         */
        Property&               append (const std::string& a_name,
                                        const bool a_save);

        /**
         * Retrieves a property with the given name. In games created with the Aaros Engine, it will
         * not at all be uncommon to request properties that do not exist in the property manager. In
         * these cases, a "null" property with no key, value, or save flag will be returned.
         *
         * @param   a_name              The name of the property.
         *
         * @return  The property with the given name, or the null property if it wasn't found.
         */
        Property&               get (const std::string& a_name);

        /**
         * Removes a property with the given name.
         *
         * @param   a_name              The name of the property.
         */
        void                    remove (const std::string& a_name);

        /**
         * Removes all loaded properties.
         */
        void                    removeAll ();
    };

}
