// File: Aaros_Lua_Keyboard.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeKeyboard (sol::state& a_state);

    }
}
