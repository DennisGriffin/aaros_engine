// File: Aaros_Shader.hpp
//
// Provides an interface for using shaders with renderable entity components.

#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include "Aaros_AssetManager.hpp"

namespace Aaros
{

    class Shader : public Utils::XmlLoadable
    {
    private /* Members */:
        sf::Shader*         mp_shader;
        sf::Sprite*         mp_sprite;
        std::string         m_vertex;
        std::string         m_fragment;
        bool                m_enabled;

    private /* Methods */:
        void                loadShader (const std::string& a_id);

    public /* Constructor and Destructor */:
        Shader (sf::Sprite* ap_sprite = nullptr);
        ~Shader ();

    public /* Inherited Methods */:
        void                loadFromXML (const pugi::xml_node &a_node) override;

    public /* Parameter Methods */:
        void                setFloat (const std::string& a_name,
                                      const float a_float);
        void                setVec2 (const std::string& a_name,
                                     const float a_one,
                                     const float a_two);
        void                setVec3 (const std::string& a_name,
                                     const float a_one,
                                     const float a_two,
                                     const float a_three);
        void                setVec4 (const std::string& a_name,
                                     const float a_one,
                                     const float a_two,
                                     const float a_three,
                                     const float a_four);
        void                setCurrentTexture (const std::string& a_name);

    public /* Getter-Setters */:
        inline std::string  getVertexShaderFile () const { return m_vertex; }
        inline std::string  getFragmentShaderFile () const { return m_fragment; }
        inline bool         isEnabled () const { return m_enabled && mp_shader != nullptr; }

        inline void setEnabled (const bool a_enabled) { m_enabled = a_enabled; }

    public /* Internal Getter-Setters */:
        inline sf::Shader*  getShaderPtr () { return mp_shader; }

    };

}
