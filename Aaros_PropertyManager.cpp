// File: Aaros_PropertyManager.cpp

#include "Aaros_PropertyManager.hpp"

namespace Aaros
{

    Property::Iterator PropertyManager::findProperty(const std::string &a_name)
    {
        return std::find_if(m_properties.begin(), m_properties.end(),
                            [&a_name] (const Property::Ptr& a_property)
        {
            return a_property->getKey() == a_name;
        });
    }

    void PropertyManager::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        // Local variable setup.
        std::string l_key = "";
        std::string l_value = "";
        bool l_save = false;

        // Iterate through the child nodes.
        for (const xml_node& l_node : a_node.children("Property"))
        {
            l_key = l_node.attribute("Key").as_string();
            l_value = l_node.attribute("Value").as_string();
            l_save = l_node.attribute("Save").as_bool();

            if (!l_key.empty())
            {
                append(l_key, l_save).set(l_value);
            }
        }
    }

    void PropertyManager::saveToXML(pugi::xml_node &a_node, const bool a_all)
    {
        using namespace pugi;

        // Local variable setup.
        std::string l_key = "";
        std::string l_value = "";
        bool l_save = false;

        // Iterate through the loaded properties.
        for (auto l_iter = m_properties.begin(); l_iter != m_properties.end(); ++l_iter)
        {
            l_key = (*l_iter)->getKey();
            l_value = (*l_iter)->get();
            l_save = (*l_iter)->isSavable();

            if (l_save == false && a_all == false) { continue; }

            xml_node l_node = a_node.append_child("Property");
            l_node.append_attribute("Key").set_value(l_key.c_str());
            l_node.append_attribute("Value").set_value(l_value.c_str());
            l_node.append_attribute("Save").set_value(l_save);
        }
    }

    void PropertyManager::saveToFile (const std::string &a_filename, const std::string &a_rootname, const bool a_all)
    {
        using namespace pugi;

        xml_document l_document;
        xml_node l_root = l_document.append_child(a_rootname.c_str());

        saveToXML(l_root, a_all);

        l_document.save_file(a_filename.c_str());
    }
    
    void PropertyManager::savePropertyToXML (const std::string &a_property, pugi::xml_node &a_node)
    {
        using namespace pugi;

        std::vector<std::string> l_substrings = Utils::splitString(a_property, '|');
        
        for (const std::string& l_str : l_substrings)
        {
            Property& l_prop = get(l_str);
            if (l_prop.get() == "") { continue; }

            xml_node l_node = a_node.append_child("Property");
            l_node.append_attribute("Key").set_value(l_prop.getKey().c_str());
            l_node.append_attribute("Value").set_value(l_prop.get().c_str());
            l_node.append_attribute("Save").set_value(l_prop.isSavable());
        }
    }

    void PropertyManager::savePropertyToFile (const std::string &a_property, const std::string &a_filename, const std::string &a_rootname)
    {
        using namespace pugi;

        xml_document l_document;
        xml_node l_root = l_document.append_child(a_rootname.c_str());

        savePropertyToXML(a_property, l_root);

        l_document.save_file(a_filename.c_str());
    }

    void PropertyManager::appendPropertyToFile (const std::string &a_property, const std::string &a_filename, const std::string &a_rootname)
    {
        using namespace pugi;

        xml_document l_document;
        xml_parse_result l_result = l_document.load_file(a_filename.c_str());

        if (l_result.status != status_ok)
        {
            savePropertyToFile(a_property, a_filename, a_rootname);
        }
        else
        {
            xml_node l_root = l_document.first_child();
            savePropertyToXML(a_property, l_root);

            l_document.save_file(a_filename.c_str());
        }
    }

    void PropertyManager::dumpToStream (std::ostream &a_out)
    {
        // Local variable setup.
        std::string l_key = "";
        std::string l_value = "";
        bool l_save = false;

        // Iterate through the loaded properties.
        for (auto l_iter = m_properties.begin(); l_iter != m_properties.end(); ++l_iter)
        {
            l_key = (*l_iter)->getKey();
            l_value = (*l_iter)->get();
            l_save = (*l_iter)->isSavable();

            a_out << l_key << " = " << l_value;
            if (l_save == true) { a_out << " (Savable)"; }
            a_out << std::endl;
        }
    }

    Property &PropertyManager::append (const std::string &a_name, const bool a_save)
    {
        // Find the property and see if it doesn't already exist.
        auto l_find = findProperty(a_name);
        if (l_find != m_properties.end())
        {
            // If it does exist, then just return that.
            return *(*l_find);
        }

        // Create the new property object and push it in.
        m_properties.push_back(std::make_unique<Property>(a_name, "", a_save));
        return *m_properties.back();
    }

    Property &PropertyManager::get(const std::string &a_name)
    {
        // Find the property.
        auto l_find = findProperty(a_name);

        // If the property exists, then return it.
        if (l_find != m_properties.end())
        {
            return *(*l_find);
        }

        // Configure the "null" property, then return that.
        m_nullProperty.set("");
        m_nullProperty.setSavable("");
        return m_nullProperty;
    }

    void PropertyManager::remove (const std::string &a_name)
    {
        // Find the property.
        auto l_find = findProperty(a_name);

        // If the property exists, then remove it.
        if (l_find != m_properties.end())
        {
            l_find = m_properties.erase(l_find);
        }
    }

    void PropertyManager::removeAll()
    {
        m_properties.clear();
    }

}
