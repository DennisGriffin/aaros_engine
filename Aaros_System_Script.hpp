// File: Aaros_System_Script.hpp
//
// This entity runs Lua scripts to manipulate entities with script components.

#pragma once

#include "Aaros_Component_Script.hpp"
#include "Aaros_System.hpp"

namespace Aaros
{

    class ScriptSystem : public System
    {
    private /* Methods */:
        bool                    isEntityValid (Entity *ap_entity) override;

    public /* Methods */:
        void                    update (const Entity::Container &a_entities,
                                        const sf::Time &a_deltaTime,
                                        const unsigned int a_entityCount) override;
        void                    fixedUpdate (const Entity::Container &a_entities,
                                             const sf::Time &a_timestep,
                                             const unsigned int a_entityCount) override;
        void                    updateCameraRect (const sf::View &a_view) override;
    };

}
