// File: Aaros_Lua_Components.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeComponents (sol::state& a_state);

    }
}
