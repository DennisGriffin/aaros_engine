// File: Aaros_Component_Text.hpp
//
// Allows the owning entity to present text on screen.

#pragma once

#include <SFML/Graphics/Text.hpp>
#include "Aaros_Utils_Misc.hpp"
#include "Aaros_Component.hpp"

namespace Aaros
{

    /**
     * This component allows the owning entity to portray itself by rendering text
     * onscreen. This component differs from the Textarea GUI widget in that the text
     * rendered here can be affected by the ECS and physics engine.
     */
    class TextComponent : public Component
    {
    private /* Members */:
        sf::Text            m_textObject;           /** The text object. */
        bool                m_centered;             /** Shall we center this text? */

    public /* Constructor and Destructor */:
        TextComponent (Entity* ap_entity,
                       const std::string& a_entityName,
                       const unsigned int a_entityID);
        ~TextComponent ();

    public /* Inherited Methods */:
        void                loadFromXML (const pugi::xml_node &a_node) override;

    public /* Outline Getter-Setters */:
        void                setFont (const std::string& a_id);

    public /* Getter-Setters */:
        inline std::string  getText () const { return m_textObject.getString(); }
        inline unsigned int getFontSize () const { return m_textObject.getCharacterSize(); }
        inline bool         isCentered () const { return m_centered; }

        inline void setText (const std::string& a_text) { m_textObject.setString(a_text); }
        inline void setFontSize (const unsigned int a_size) { m_textObject.setCharacterSize(a_size); }
        inline void setColor (const int a_r, const int a_g, const int a_b, const int a_a = 255) { m_textObject.setColor(Utils::createColor(a_r, a_g, a_b, a_a)); }
        inline void setCentered (const bool a_centered) { m_centered = a_centered; }

    public /* Internal Getter-Setters */:
        inline sf::Text& getTextObject () { return m_textObject; }

    };

}
