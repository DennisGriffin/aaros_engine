// File: Aaros_System_Animation.cpp

#include "Aaros_System_Animation.hpp"

namespace Aaros
{

    bool AnimationSystem::isEntityValid (Entity *ap_entity)
    {
        if (ap_entity->isEnabled() == false) { return false; }

        bool l_hasSprite = ap_entity->hasComponent<SpriteComponent>();
        bool l_hasAnimation = ap_entity->hasComponent<AnimationComponent>();

        if (l_hasSprite == false || l_hasAnimation == false)
            return false;

        if (ap_entity->getComponent<SpriteComponent>().isEnabled() == false ||
            ap_entity->getComponent<AnimationComponent>().isEnabled() == false)
        {
            return false;
        }

        return true;
    }

    void AnimationSystem::update (const Entity::Container &a_entities, const sf::Time &a_deltaTime, const unsigned int a_entityCount)
    {
        for (unsigned int i = 0; i < a_entityCount; ++i)
        {
            auto& l_ptr = a_entities.at(i);

            if (isEntityValid(l_ptr.get()) == false) { continue; }

            TransformComponent& l_transform = *l_ptr->getTransform();

            if (m_cameraRect.contains(l_transform.getPixelPosition()))
            {
                AnimationComponent& l_animation = l_ptr->getComponent<AnimationComponent>();
                SpriteComponent& l_sprite = l_ptr->getComponent<SpriteComponent>();

                l_animation.tick(a_deltaTime);
                l_sprite.getSprite().setTextureRect({ l_animation.getCurrentFrameVector(), l_animation.getFrameSize() });
            }
        }
    }

}
