// File: Aaros_Utils_Random.hpp
//
// Provides an interface for generating random numbers.

#pragma once

#include <cstdlib>
#include <ctime>
#include "Aaros_Utils_Singleton.hpp"

namespace Aaros
{
    namespace Constants
    {

        const int       G_RANDOM_LOWER_I    = 0;
        const int       G_RANDOM_UPPER_I    = 10;
        const float     G_RANDOM_LOWER_F    = 0.0f;
        const float     G_RANDOM_UPPER_F    = 10.0f;

    }

    namespace Math
    {

        class Random : public Utils::Singleton<Random>
        {
        private /* Friend Classes */:
            friend class Utils::Singleton<Random>;

        public /* Methods */:
            inline void initialize ()
            {
                std::srand(static_cast<unsigned int>(std::time(nullptr)));
            }

            inline int getNextInt ()
            {
                return Constants::G_RANDOM_LOWER_I +
                        (std::rand() / (RAND_MAX / (Constants::G_RANDOM_UPPER_I - Constants::G_RANDOM_LOWER_I)));
            }

            inline int getNextInt (const int a_min, const int a_max)
            {
                if (a_min >= a_max) { return getNextInt(); }

                return a_min + (std::rand() / (RAND_MAX / (a_max - a_min)));
            }

            inline float getNextFloat ()
            {
                return Constants::G_RANDOM_LOWER_F +
                        (static_cast<float>(std::rand()) /
                         (static_cast<float>(RAND_MAX / (Constants::G_RANDOM_UPPER_F - Constants::G_RANDOM_LOWER_F))));
            }

            inline float getNextFloat (const float a_min, const float a_max)
            {
                if (a_min >= a_max) { return getNextFloat(); }

                return a_min + (static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / (a_max - a_min))));
            }

        };

    }
}
