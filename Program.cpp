#include <iostream>
#include "Aaros_Application.hpp"
#include "Aaros_Utils_String.hpp"

int main ()
{
    int l_error = 0;

    try
    {
        Aaros::Application l_app;
        l_app.run();
    }
    catch (std::exception& l_ex)
    {
        std::cerr << l_ex.what() << std::endl;
        Aaros::PropertyManager::getInstance().dumpToStream(std::cerr);
        l_error = 1;
    }

    return l_error;
}
