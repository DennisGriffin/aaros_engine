// File: Aaros_GUI_Button.hpp
//
// Presents a selectable button on screen.

#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include "Aaros_GUI_Widget.hpp"

namespace Aaros
{
    namespace Constants
    {

        const sf::Vector2f      G_BUTTON_SIZE                       = { 160.0f, 24.0f };
        const float             G_BUTTON_OUTLINE_THICKNESS          = 2.0f;
        const unsigned int      G_BUTTON_FONT_SIZE                  = 14;
        const sf::Color         G_BUTTON_FILL_COLOR                 = { 222, 222, 222 };
        const sf::Color         G_BUTTON_ACTIVE_FILL_COLOR          = { 33, 33, 33 };
        const sf::Color         G_BUTTON_OUTLINE_COLOR              = { 33, 33, 33 };
        const sf::Color         G_BUTTON_ACTIVE_OUTLINE_COLOR       = { 222, 222, 222 };
        const sf::Color         G_BUTTON_TEXT_COLOR                 = { 33, 33, 33 };
        const sf::Color         G_BUTTON_ACTIVE_TEXT_COLOR          = { 222, 222, 222 };

    }

    namespace GUI
    {

        /**
         * This is a selectable button widget that can be clicked upon with the mouse,
         * or focused upon and selected with the keyboard in order to raise an event which is
         * responded to in Lua.
         */
        class Button : public Widget
        {
        private /* Rectangle Members */:
            sf::RectangleShape  m_rectObject;           /** The rectangle that is drawn onscreen. */
            sf::Vector2f        m_rectSize;             /** The size of the rectangle. */
            sf::Color           m_rectFillColor;        /** The rectangle's fill color, if the button is not in focus. */
            sf::Color           m_rectFocusFill;        /** The rectangle's fill color, if the button is in focus. */
            sf::Color           m_rectOutlineColor;     /** The rectangle's outline color, if not in focus. */
            sf::Color           m_rectFocusOutline;     /** The rectnagle's outline color, if in focus. */
            bool                m_rectVisible;          /** Shall we draw the rectnagle? */
            bool                m_selected;             /** Was the button just clicked upon or selected? */

        private /* Text Members */:
            sf::Text            m_textObject;           /** The text that is drawn onscreen. */
            std::string         m_text;                 /** The text that will be wrapped into the button's bounds. */
            std::string         m_textFontID;           /** The ID or filename of the font asset used for drawing the text. */
            unsigned int        m_textFontSize;         /** The size of that font. */
            sf::Vector2f        m_textWrapSize;         /** The bounds into which the text will be wrapped. */
            sf::Color           m_textColor;            /** The color of the text. */
            sf::Color           m_textFocus;            /** The color of the text when the button is in focus. */

        private /* Listbox Members */:
            std::string         m_id;                   /** If the button is a part of the Listbox widget, this will act as its choice ID. */

        private /* Inherited Methods */:
            void                updateWidgetInternals () override;

        public /* Constructor and Destructor */:
            Button (Entity* ap_entity, const std::string& a_name);
            ~Button ();

        public /* Inherited Methods */:
            void                loadFromXML (const pugi::xml_node &a_node) override;
            void                update (const sf::Time &a_deltaTime) override;
            void                render (sf::RenderWindow &a_window) override;

        public /* Getter-Setters */:
            inline std::string  getText () const { return m_text; }
            inline std::string  getID () const { return m_id; }
            inline std::string  getFont () const { return m_textFontID; }
            inline unsigned int getFontSize () const { return m_textFontSize; }
            inline float        getWidth () const { return m_rectSize.x; }
            inline float        getHeight () const { return m_rectSize.y; }
            inline bool         isRectVisible () const { return m_rectVisible; }
            inline bool         isSelected () const { return m_selected; }

            inline void setText (const std::string& a_text) { m_text = a_text; }
            inline void setID (const std::string& a_id) { m_id = a_id; }
            inline void setFont (const std::string& a_font) { m_textFontID = setTextObjectFont(m_textObject, a_font); }
            inline void setFontSize (const unsigned int a_size) { m_textFontSize = a_size; }
            inline void setSize (const float a_x, const float a_y) { m_rectSize = { std::abs(a_x), std::abs(a_y) }; }
            inline void setRectVisible (const bool a_visible) { m_rectVisible = a_visible; }
            inline void setFillColor (const int a_r, const int a_g, const int a_b) { m_rectFillColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setOutlineColor (const int a_r, const int a_g, const int a_b) { m_rectOutlineColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setTextColor (const int a_r, const int a_g, const int a_b) { m_textColor = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusFillColor (const int a_r, const int a_g, const int a_b) { m_rectFocusFill = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusOutlineColor (const int a_r, const int a_g, const int a_b) { m_rectFocusOutline = Utils::createColor(a_r, a_g, a_b); }
            inline void setFocusTextColor (const int a_r, const int a_g, const int a_b) { m_textFocus = Utils::createColor(a_r, a_g, a_b); }

        };

    }
}
