// File: Aaros_Component_Transform.cpp

#include "Aaros_Component_Transform.hpp"
#include "Aaros_Entity.hpp"

namespace Aaros
{

    TransformComponent::ChildIter TransformComponent::findChildByName(const std::string &a_name)
    {
        return std::find_if(m_children.begin(), m_children.end(),
                            [&a_name] (TransformComponent* ap_transform)
        {
            return ap_transform->getEntityName() == a_name;
        });
    }

    void TransformComponent::onEnabledChanged()
    {
        for (TransformComponent* lp_transform : m_children)
        {
            lp_transform->getOwningEntity()->setAncestorDisabled(!m_enabled);
            lp_transform->setAncestorDisabled(!m_enabled);
        }
    }

    void TransformComponent::onAncestorDisabledChanged()
    {
        if (m_ancestorDisabled == true)
        {
            for (TransformComponent* lp_transform : m_children)
            {
                lp_transform->getOwningEntity()->setAncestorDisabled(true);
                lp_transform->setAncestorDisabled(true);
            }
        }
        else
        {
            for (TransformComponent* lp_transform : m_children)
            {
                lp_transform->getOwningEntity()->setAncestorDisabled(!m_enabled);
                lp_transform->setAncestorDisabled(!m_enabled);
            }
        }
    }

    void TransformComponent::onAliveChanged()
    {
        for (TransformComponent* lp_transform : m_children)
        {
            lp_transform->getOwningEntity()->setAncestorDead(!m_entityAlive);
            lp_transform->setAncestorDead(!m_entityAlive);
        }
    }

    void TransformComponent::onAncestorDeadChanged()
    {
        if (m_ancestorDead == true)
        {
            for (TransformComponent* lp_transform : m_children)
            {
                lp_transform->getOwningEntity()->setAncestorDead(true);
                lp_transform->setAncestorDead(true);
            }
        }
        else
        {
            for (TransformComponent* lp_transform : m_children)
            {
                lp_transform->getOwningEntity()->setAncestorDead(!m_entityAlive);
                lp_transform->setAncestorDead(!m_entityAlive);
            }
        }
    }

    void TransformComponent::onPersistentChanged()
    {
        for (TransformComponent* lp_transform : m_children)
        {
            lp_transform->getOwningEntity()->setAncestorPersistent(m_entityPersistent);
            lp_transform->setAncestorPersistent(m_entityPersistent);
        }
    }

    void TransformComponent::onAncestorPersistentChanged()
    {
        if (m_ancestorPersistent == true)
        {
            for (TransformComponent* lp_transform : m_children)
            {
                lp_transform->getOwningEntity()->setAncestorPersistent(true);
                lp_transform->setAncestorPersistent(true);
            }
        }
        else
        {
            for (TransformComponent* lp_transform : m_children)
            {
                lp_transform->getOwningEntity()->setAncestorPersistent(m_entityPersistent);
                lp_transform->setAncestorPersistent(m_entityPersistent);
            }
        }
    }

    void TransformComponent::onPositionChanged()
    {
        m_absolutePosition = m_originPoint + m_relativePosition;
        m_absoluteCenter = m_relativeCenter + m_absolutePosition;

        for (TransformComponent* lp_transform : m_children)
        {
            lp_transform->setOriginPoint(m_absolutePosition);
        }

        mp_entity->callLuaFunction("onPositionChanged", m_absolutePosition.x, m_absolutePosition.y);
    }

    void TransformComponent::onCenterChanged()
    {
        m_absoluteCenter = m_relativeCenter + m_absolutePosition;
    }

    TransformComponent::TransformComponent(Entity *ap_entity, const std::string &a_entityName, const unsigned int a_entityID) :
        Component               { ap_entity, a_entityName, a_entityID },
        mp_parent               { nullptr },
        m_entityAlive           { true },
        m_entityPersistent      { false },
        m_ancestorDead          { false },
        m_ancestorPersistent    { false },
        m_originPoint           { 0.0f, 0.0f },
        m_relativePosition      { 0.0f, 0.0f },
        m_absolutePosition      { 0.0f, 0.0f },
        m_relativeCenter        { 0.0f, 0.0f },
        m_absoluteCenter        { 0.0f, 0.0f }
    {

    }

    TransformComponent::~TransformComponent()
    {
        m_children.clear();
        m_entityName.clear();

        m_enabled               = false;
        mp_parent               = nullptr;
        m_entityAlive           = false;
        m_entityPersistent      = false;
        m_ancestorDead          = false;
        m_ancestorPersistent    = false;
        m_entityID              = 0;
        mp_entity               = nullptr;
    }

    void TransformComponent::loadFromXML(const pugi::xml_node &a_node)
    {
        using namespace pugi;

        xml_node l_positionNode = a_node.child("Position");
        xml_node l_centerNode = a_node.child("Center");

        float l_x = l_positionNode.attribute("X").as_float();
        float l_y = l_positionNode.attribute("Y").as_float();
        float l_cx = l_centerNode.attribute("X").as_float();
        float l_cy = l_centerNode.attribute("Y").as_float();

        m_relativePosition = { l_x, l_y };
        m_relativeCenter = { l_cx, l_cy };

        onPositionChanged();
    }

    bool TransformComponent::hasChild(const std::string &a_name)
    {
        auto l_find = findChildByName(a_name);
        return (l_find != m_children.end());
    }

    TransformComponent *TransformComponent::getChild(const std::string &a_name)
    {
        auto l_find = findChildByName(a_name);

        if (l_find != m_children.end())
        {
            return (*l_find);
        }

        return nullptr;
    }

    void TransformComponent::detachChild(const std::string &a_name)
    {
        auto l_find = findChildByName(a_name);

        if (l_find != m_children.end())
        {
            (*l_find)->setAncestorDisabled(false);
            (*l_find)->setAncestorDead(false);
            (*l_find)->setAncestorPersistent(false);

            sf::Vector2f l_childPosition = (*l_find)->getPosition();
            (*l_find)->zeroOrigin();
            (*l_find)->setPositionVector(l_childPosition);

            (*l_find)->getOwningEntity()->setHierarchyPath("");
            (*l_find)->adjustHierarchyPath("");
            (*l_find)->mp_parent = nullptr;
            l_find = m_children.erase(l_find);
        }
    }

    void TransformComponent::detachAllChildren(const bool a_killToo)
    {
        for (auto l_iter = m_children.begin(); l_iter != m_children.end(); )
        {
            (*l_iter)->setAncestorDisabled(false);
            (*l_iter)->setAncestorDead(a_killToo);
            (*l_iter)->setAncestorPersistent(false);

            sf::Vector2f l_childPosition = (*l_iter)->getPosition();
            (*l_iter)->zeroOrigin();
            (*l_iter)->setPositionVector(l_childPosition);

            (*l_iter)->getOwningEntity()->setHierarchyPath("");
            (*l_iter)->adjustHierarchyPath("");
            (*l_iter)->mp_parent = nullptr;
            l_iter = m_children.erase(l_iter);
        }
    }

    void TransformComponent::setParent(const std::string &a_path)
    {
        std::string l_parentEntityName = mp_entity->getHierarchyPath();
        if (l_parentEntityName.empty() == false && l_parentEntityName == a_path)
            return;

        if (mp_parent != nullptr)
        {
            mp_parent->detachChild(m_entityName);
        }

        if (a_path.empty() == false)
        {
            Entity& l_entity = mp_entity->getEntityInScene(a_path);
            l_entity.getTransform()->attachChild(this);
        }
    }

    void TransformComponent::translate(const float a_x, const float a_y)
    {
        m_relativePosition.x += a_x;
        m_relativePosition.y += a_y;

        onPositionChanged();
    }

    void TransformComponent::translateByVector (const sf::Vector2f &a_vector)
    {
        m_relativePosition += a_vector;

        onPositionChanged();
    }

    void TransformComponent::attachChild(TransformComponent *ap_transform)
    {
        if (ap_transform == nullptr) { return; }

        auto l_find = findChildByName(ap_transform->getEntityName());
        if (l_find == m_children.end())
        {
            std::string l_fullName = mp_entity->getFullName();

            bool l_ancestorDisabled = (m_enabled == false || m_ancestorDisabled == true);
            bool l_ancestorDead = (m_entityAlive == false || m_ancestorDead == true);
            bool l_ancestorPersistent = (m_entityPersistent == true || m_ancestorPersistent == true);

            ap_transform->setAncestorDisabled(l_ancestorDisabled);
            ap_transform->setAncestorDead(l_ancestorDead);
            ap_transform->setAncestorPersistent(l_ancestorPersistent);

            // Preserving the child's position, set up its origin point.
            sf::Vector2f l_relative = ap_transform->getPosition() - m_absolutePosition;
            ap_transform->setOriginPoint(m_absolutePosition);
            ap_transform->setPositionVector(l_relative);

            ap_transform->getOwningEntity()->setHierarchyPath(l_fullName);
            ap_transform->adjustHierarchyPath(l_fullName);
            ap_transform->mp_parent = this;
            m_children.push_back(ap_transform);
        }
    }

    void TransformComponent::adjustHierarchyPath(const std::string& a_path)
    {
        std::string l_fullPath = "";

        if (a_path.empty() == false) { l_fullPath = a_path + "." + m_entityName; }
        else { l_fullPath = m_entityName; }

        for (TransformComponent* lp_transform : m_children)
        {
            lp_transform->getOwningEntity()->setHierarchyPath(l_fullPath);
            lp_transform->adjustHierarchyPath(l_fullPath);
        }
    }

}
