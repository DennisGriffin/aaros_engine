// File: Aaros_Lua_Widgets.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeWidgets (sol::state& a_state);

    }
}
