// File: Aaros_Component.hpp
//
// The base class for all of our entity components.

#pragma once

#include <map>
#include <memory>
#include <typeinfo>
#include <typeindex>
#include <stdexcept>
#include "Aaros_Utils_XmlLoadable.hpp"

namespace Aaros
{

    /** Forward-declare the Entity class. */
    class Entity;

    /**
     * This is the base class of our entity components. These are the basic
     * building blocks for expanding the functionality and usefulness of our game
     * objects.
     */
    class Component : public Utils::XmlLoadable
    {
    public /* Typedefs */:
        using Ptr               = std::unique_ptr<Component>;
        using Typemap           = std::map<std::type_index, Ptr>;

    protected /* Members */:
        Entity*                 mp_entity;              /** A pointer to the entity that owns this component. */
        std::string             m_entityName;           /** The name of the entity that owns this component. */
        unsigned int            m_entityID;             /** The ID number of the entity that owns this component. */
        bool                    m_enabled;              /** Should the systems operate upon this component? */
        bool                    m_ancestorDisabled;     /** Is a similar component belonging to an ancestor entity disabled? */

    protected /* Setter Callbacks */:
        /** Called when the component's 'enabled' flag is changed. */
        virtual void            onEnabledChanged () {}

        /** Called when the component's 'ancestorDisabled' flag is changed. */
        virtual void            onAncestorDisabledChanged () {}

    protected /* Constructor */:
        /** Constructs the entity component */
        Component (Entity* ap_entity,
                   const std::string& a_entityName,
                   const unsigned int a_entityID) :
            mp_entity           { ap_entity },
            m_entityName        { a_entityName },
            m_entityID          { a_entityID },
            m_enabled           { true },
            m_ancestorDisabled  { false } {}

    public /* Virtual Destructor */:
        virtual ~Component () {}

    public /* Getter-Setters */:
        inline bool             isEnabled () const { return m_enabled && !m_ancestorDisabled; }

        inline void setEnabled (const bool a_enabled) { m_enabled = a_enabled; onEnabledChanged(); }

    public /* Internal Getter-Setters */:
        inline Entity*          getOwningEntity () const { return mp_entity; }
        inline std::string      getEntityName () const { return m_entityName; }
        inline unsigned int     getEntityID () const { return m_entityID; }
        inline bool             isSelfEnabled () const { return m_enabled; }
        inline bool             isAncestorDisabled () const { return m_ancestorDisabled; }

        inline void setAncestorDisabled (const bool a_disabled) { m_ancestorDisabled = a_disabled; onAncestorDisabledChanged(); }

    };

    template <typename T>
    struct TT_Is_Component_Vital
    {
        static const bool Value = false;
    };

}
