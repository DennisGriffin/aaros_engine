// File: Aaros_RenderSystem.hpp
//
// Renders all entities with drawable components.

#pragma once

#include "Aaros_System.hpp"

namespace Aaros
{

    enum class RenderMode
    {
        None,
        Text,
        Sprite,
        Tilemap,
        Emitter
    };

    class RenderSystem : public System
    {
    private /* Members */:
        RenderMode              a_mode;

    private /* Methods */:
        bool                    isEntityValid (Entity *ap_entity) override;

    public /* Methods */:
        void                    update (const Entity::Container &a_entities,
                                        const sf::Time &a_deltaTime,
                                        const unsigned int a_entityCount) override;

        void                    render (const Entity::Container &a_entities,
                                        sf::RenderWindow& a_window,
                                        const unsigned int a_entityCount) override;
    };

}
