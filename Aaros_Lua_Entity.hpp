// File: Aaros_Lua_Entity.hpp

#pragma once

#include <sol.hpp>

namespace Aaros
{
    namespace Lua
    {

        void exposeEntity (sol::state& a_state);

    }
}
