// File: Aaros_Component_Animation.hpp
//
// Allows an owning entity with a sprite component to animate.

#pragma once

#include <vector>
#include <map>
#include <cmath>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>
#include "Aaros_Component.hpp"

namespace Aaros
{

    namespace Constants
    {

        const std::string       G_DEFAULT_ANIMATION_ID          = "default";
        const sf::Time          G_DEFAULT_FRAME_INTERVAL        = sf::seconds(0.5f);
        const sf::Vector2i      G_DEFAULT_FRAME_SIZE            = { 32, 32 };

    }

    /**
     * This component allows a sprite component attached to the owning
     * entity to animate. This component can host various different animation
     * sequences that can be triggered by scripts.
     */
    class AnimationComponent : public Component
    {
    public /* Typedefs */:
        using Animation         = std::vector<sf::Vector2i>;
        using AnimationMap      = std::map<std::string, Animation>;

    private /* Members */:
        AnimationMap            m_animations;           /** The map of registered animations. */
        std::string             m_currentAnimation;     /** The string ID of the currently active animation. */
        unsigned int            m_currentFrame;         /** The index of the current animation frame. */
        sf::Time                m_currentFrameTime;     /** The amount of time, in seconds, that the current frame has been active. */
        sf::Time                m_frameInterval;        /** How long, in seconds, should the current frame be active? */
        sf::Vector2i            m_frameSize;            /** The size of the animation frame, in pixels. */

    private /* Methods */:
        /**
         * Updates the currently active animation.
         */
        void                    updateAnimation ();

    public /* Members */:
        /** A default animation is added automatically upon construction. */
        AnimationComponent (Entity* ap_entity,
                            const std::string& a_entityName,
                            const unsigned int a_entityID);
        ~AnimationComponent ();

    public /* Inherited Methods */:
        /**
         * Loads details on the size of the animated sprite, how quickly the sprite
         * should animate, and each frame in each animation sequence.
         */
        void                    loadFromXML (const pugi::xml_node &a_node) override;

    public /* Methods */:
        /**
         * Use this method to change to a new animation sequence.
         *
         * @param   a_id            The ID of the new animation. Leave blank for the default.
         */
        void                    setCurrentAnimation (const std::string& a_id = Constants::G_DEFAULT_ANIMATION_ID);

        /**
         * Adds a new frame to the given animation sequence.
         *
         * @param   a_id            The ID of the animation. If it doesn't exist, then it is created.
         * @param   a_x             The X coordinate on the source image.
         * @param   a_y             The Y coordinate on the source image.
         */
        void                    pushFrame (const std::string& a_id, const int a_x, const int a_y);

        /**
         * Removes the last frame from the given animation sequence. L.I.F.O basis.
         *
         * @param   a_id            The ID of the animation. If it doesn't exist, then nothing happens.
         */
        void                    popFrame (const std::string& a_id);

    public /* Internal Methods */:
        /**
         * Keeps track of when we need to update the animation next. This is run by
         * the animation system.
         *
         * @param   a_time          The amount of time elapsed since the previous frame.
         */
        void                    tick (const sf::Time& a_time);

        /**
         * Gets the location of the current frame on the source image. This is called
         * by the animation system in order to set the sprite component's texture rect.
         *
         * @return  The location of the current frame on the source image.
         */
        sf::Vector2i            getCurrentFrameVector ();

    public /* Getter-Setters */:
        inline std::string      getCurrentAnimation () const { return m_currentAnimation; }
        inline unsigned int     getCurrentFrame () const { return m_currentFrame; }
        inline float            getFrameInterval () const { return m_frameInterval.asSeconds(); }
        inline int              getFrameWidth () const { return m_frameSize.x; }
        inline int              getFrameHeight () const { return m_frameSize.y; }

        inline void setFrameInterval (const float a_seconds) { m_frameInterval = sf::seconds(std::abs(a_seconds)); }
        inline void setFrameSize (const int a_width, const int a_height) { m_frameSize = { std::abs(a_width), std::abs(a_height) }; }

    public /* Internal Getter-Setters */:
        inline sf::Vector2i     getFrameSize () const { return m_frameSize; }

    };

}
