// File: Aaros_Lua_Scene.cpp

#include "Aaros_Lua_Scene.hpp"
#include "Aaros_Scene.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeScene (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<Scene> l_userdata
            {
                "Scene", l_ctors,

                "loadNew",                  &Scene::loadNew,
                "loadAdditive",             &Scene::loadAdditive,

                "addEntity",                &Scene::addEntity,
                "getEntity",                &Scene::getEntity,
                "removeAllEntities",        &Scene::removeAllEntities,
                "centerCameraOnPoint",      &Scene::centerCameraOnPoint,
                "centerCameraOnEntity",     &Scene::centerCameraOnEntity,

                "getGravityX",              &Scene::getGravityX,
                "getGravityY",              &Scene::getGravityY,
                "getVelocityIterCount",     &Scene::getVelocityIterCount,
                "getPositionIterCount",     &Scene::getPositionIterCount,

                "setWindowSize",            &Scene::setWindowSize,
                "setWindowFullscreen",      &Scene::setWindowFullscreen,
                "quit",                     &Scene::quit,
                "setGravity",               &Scene::setGravity,
                "setVelocityIterCount",     &Scene::setVelocityIterCount,
                "setPositionIterCount",     &Scene::setPositionIterCount

            };
            a_state.set_userdata(l_userdata);
        }

    }
}

