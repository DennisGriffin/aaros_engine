// File: Aaros_Lua_Jukebox.cpp

#include "Aaros_Lua_Jukebox.hpp"
#include "Aaros_Jukebox.hpp"

namespace Aaros
{
    namespace Lua
    {

        void exposeJukebox (sol::state& a_state)
        {
            sol::constructors<> l_ctors;
            sol::userdata<Jukebox> l_userdata
            {
                "Jukebox", l_ctors,

                "loadSong",         &Jukebox::loadSong,
                "playSong",         &Jukebox::playSong,
                "pauseSong",        &Jukebox::pauseSong,
                "stopSong",         &Jukebox::stopSong,
                "isSongLoaded",     &Jukebox::isSongLoaded,
                "isLooping",        &Jukebox::isLooping,
                "setLooping",       &Jukebox::setLooping
            };
            a_state.set_userdata(l_userdata)
                    .get<sol::table>("Jukebox")
                    .set_function("getInstance", Jukebox::getInstance);
        }

    }
}
